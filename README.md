# PackageIO

By [feudalnate](https://github.com/feudalnate)
with help from Mojobojo and DJ Shepherd.

It has no clear license, but is explicitly open source.

[website](http://360haven.com/feudalnate/pio.php)
[360haven forum post](http://www.360haven.com/forums/showthread.php/428-PackageIO-Open-Source-Endian-IO-Class)
[se7ensins forum post](https://www.se7ensins.com/forums/threads/packageio-open-source-io-class.86975/)
