﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("PackageIO")>
<Assembly: AssemblyDescription("An open source multi-purpose Endian IO class")>
<Assembly: AssemblyCompany("")>
<Assembly: AssemblyProduct("PackageIO")>
<Assembly: AssemblyCopyright("")>
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("e0b2d770-967d-4c4e-a296-7c70d430d386")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")>

<Assembly: AssemblyVersion("0.1.9.4")>
<Assembly: AssemblyFileVersion("0.1.9.4")>
