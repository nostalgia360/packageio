﻿'PackageIO and all included classes created by feudalnate
'Special thanks to Mojobojo and DJ Shepherd
'Version: 0.1.9.4
'Visit http://www.360haven.com for updates
'Contact: feudalnate@gmail.com

#Region "Options/Imports"

Option Explicit On
Option Strict Off
Option Compare Binary

Imports System.IO
Imports System.Numeric
Imports System.Net
Imports System.Array
Imports System.Text.Encoding
Imports System.Text.RegularExpressions
Imports System.Globalization
Imports System.Drawing
Imports System.Security
Imports System.Security.Cryptography
Imports System.Runtime.CompilerServices
Imports System.Runtime.InteropServices
Imports Microsoft.VisualBasic.Devices
Imports PackageIO.Conversions
Imports PackageIO.Functions

#End Region

''' <summary>
''' A combination of both the Reader and Writer classes. Supports reading/writing of various data types in either endianness
''' </summary>
Public Class IO
    Implements IDisposable, ICloneable, IEquatable(Of IO)
    Private OpenFile As String = "", OpenStream As Stream = Nothing
    Private Access As FileAccess, Share As FileShare, Type As StreamType
    Private Reader As Reader = Nothing, Writer As Writer = Nothing
    Private Last As Long = 0, Endianness As Endian, CanPause As Boolean = True, StreamPaused As Boolean = False, StreamOwner As Boolean = False

    ''' <summary>
    ''' Creates a new instance of the IO class
    ''' </summary>
    ''' <param name="File">Absolute path to the file to open or create</param>
    ''' <param name="EndianType">Initial byte order in which to read/write data. Optional.</param>
    ''' <param name="Position">Initial offset to set stream position to. Optional.</param>
    ''' <param name="FileMode">A System.IO.FileMode constant that detremines how to open or create the file. Optional.</param>
    ''' <param name="FileAccess">A System.IO.FileAccess contstant that determines how the file can be access by the object. Optional.</param>
    ''' <param name="FileShare">A System.IO.FileAccess contstant that determines how the file will be shared by processes. Optional.</param>
    Sub New(ByVal File As String, Optional ByVal EndianType As Endian = Endian.Little, Optional ByVal Position As Long = 0, Optional ByVal FileMode As FileMode = FileMode.Open, Optional ByVal FileAccess As FileAccess = FileAccess.ReadWrite, Optional ByVal FileShare As FileShare = FileShare.ReadWrite)
        MyBase.New()
        If Not FileMode = FileMode.Create AndAlso Not FileMode = FileMode.CreateNew Then If Not System.IO.File.Exists(File) Then Throw New FileNotFoundException(New String(File & " was not found or could not be accessed"))
        OpenFile = File
        Access = FileAccess
        Share = FileShare
        Type = StreamType.FileStream
        OpenStream = New FileStream(File, FileMode, FileAccess, FileShare)
        If Not OpenStream.CanRead Then Throw New InvalidDataException("Stream does not have read access")
        If Not OpenStream.CanWrite Then Throw New InvalidDataException("Stream does not have write access")
        Reader = New Reader(OpenStream)
        Writer = New Writer(OpenStream)
        StreamOwner = True
        Me.Position = Position
        CurrentEndian = EndianType
    End Sub

    ''' <summary>
    ''' Creates a new instance of the IO class
    ''' </summary>
    ''' <param name="Buffer">An existing memory buffer holding data to do IO operations to</param>
    ''' <param name="EndianType">Initial byte order in which to read/write data. Optional.</param>
    ''' <param name="Position">Initial offset to set stream position to. Optional.</param>
    Sub New(ByVal Buffer As Byte(), Optional ByVal EndianType As Endian = Endian.Little, Optional ByVal Position As Long = 0)
        MyBase.New()
        Type = StreamType.MemoryStream
        OpenStream = New MemoryStream(Buffer, True)
        Reader = New Reader(OpenStream)
        Writer = New Writer(OpenStream)
        StreamOwner = True
        Me.Position = Position
        CurrentEndian = EndianType
        CanPause = False
    End Sub

    ''' <summary>
    ''' Creates a new instance of the IO class
    ''' </summary>
    ''' <param name="Stream">An existing stream to do IO operations to</param>
    ''' <param name="EndianType">Initial byte order in which to read/write data. Optional.</param>
    ''' <param name="Position">Initial offset to set stream position to. Optional.</param>
    Sub New(ByVal Stream As Stream, Optional ByVal EndianType As Endian = Endian.Little, Optional ByVal Position As Long = 0)
        MyBase.New()
        Type = StreamType.GenericStream
        OpenStream = Stream
        If Not OpenStream.CanRead Then Throw New InvalidDataException("Stream does not have read access")
        If Not OpenStream.CanWrite Then Throw New InvalidDataException("Stream does not have write access")
        Reader = New Reader(OpenStream)
        Writer = New Writer(OpenStream)
        Me.Position = Position
        CurrentEndian = EndianType
        CanPause = False
    End Sub

    ''' <summary>
    ''' Returns the current working stream in use by the instance
    ''' </summary>
    ReadOnly Property CurrentStream() As Stream
        Get
            Return OpenStream
        End Get
    End Property

    ''' <summary>
    ''' Returns StreamType of the current stream
    ''' </summary>
    ''' <returns>Returns a StreamType enumerator value indicating which type of stream the underlying stream is</returns>
    ReadOnly Property StreamType() As StreamType
        Get
            Return Type
        End Get
    End Property

    ''' <summary>
    ''' True or false whether the underlying stream was initially created by the instance
    ''' </summary>
    ''' <returns>Returns a boolean value indicating if the underlying stream was created by the instance</returns>
    ReadOnly Property IsOwner() As Boolean
        Get
            Return StreamOwner
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the current position (offset) of the stream
    ''' </summary>
    ''' <value>Offset to set stream position to</value>
    ''' <returns>Current offset stream position is at</returns>
    Property Position() As Long
        Get
            CheckPaused()
            Return OpenStream.Position
        End Get
        Set(ByVal value As Long)
            CheckPaused()
            If Last <> OpenStream.Position Then Last = OpenStream.Position
            OpenStream.Position = value
        End Set
    End Property

    ''' <summary>
    ''' Sets stream postion to the last position before last operation or position set
    ''' </summary>
    Sub JumpBack()
        Position = LastPosition
    End Sub

    ''' <summary>
    ''' Moves stream position to specified offset
    ''' </summary>
    ''' <param name="Offset">Offset to set stream position to</param>
    Sub Seek(ByVal Offset As Long)
        Seek(Offset, SeekOrigin.Begin)
    End Sub

    ''' <summary>
    ''' Moves stream position to specified offset
    ''' </summary>
    ''' <param name="Offset"></param>
    ''' <param name="SeekOrigin">A value of type System.IO.SeekOrigin indicating the reference point used to obtain the new position</param>
    Sub Seek(ByVal Offset As Long, ByVal SeekOrigin As SeekOrigin)
        CheckPaused()
        If Last <> OpenStream.Position Then Last = OpenStream.Position
        OpenStream.Seek(Offset, SeekOrigin)
    End Sub

    ''' <summary>
    ''' Gets the length in bytes of the current stream
    ''' </summary>
    ''' <returns>Current length in bytes of the underlying stream</returns>
    ReadOnly Property Length() As Long
        Get
            Return OpenStream.Length
        End Get
    End Property

    ''' <summary>
    ''' Gets the length in bytes from current position to end of stream
    ''' </summary>
    ''' <returns>Length in bytes from current position to length of stream (Length - Position)</returns>
    ReadOnly Property LengthToEnd() As Long
        Get
            Return CLng(Length - Position)
        End Get
    End Property

    ''' <summary>
    ''' Get or set the endianness currently in use by the Reader/Writer
    ''' </summary>
    Property CurrentEndian() As Endian
        Get
            Return Endianness
        End Get
        Set(ByVal value As Endian)
            If Not [Enum].IsDefined(GetType(Endian), value) Then Return
            Endianness = value
        End Set
    End Property

    ''' <summary>
    ''' Returns the position before last operation or position set
    ''' </summary>
    ReadOnly Property LastPosition() As Long
        Get
            Return Last
        End Get
    End Property

    ''' <summary>
    ''' Returns all bytes in the current stream
    ''' </summary>
    ''' <returns>Buffer containing all bytes in the underlying stream</returns>
    Function GetBuffer() As Byte()
        Dim Buffer() As Byte = New Byte() {}
        GetBuffer(Buffer)
        Return Buffer
    End Function

    ''' <summary>
    ''' Copies all bytes in underlying stream to refrenced byte array
    ''' </summary>
    ''' <param name="OutBuffer">Buffer to store underlying stream bytes</param>
    Sub GetBuffer(<[In](), [Out]()> ByRef OutBuffer As Byte())
        CheckPaused()
        Functions.StreamToBuffer(OpenStream, OutBuffer)
    End Sub

#Region "Reading"

    ''' <summary>
    ''' Reads a single UTF8 encoded character from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a single UTF8 encoded character</returns>
    Function PeekChar() As Char
        CheckPaused()
        Return Reader.PeekChar
    End Function

    ''' <summary>
    ''' Reads specified count of UTF8 encoded characters from stream without advancing current stream position
    ''' </summary>
    ''' <param name="Length">Number of characters to read from stream</param>
    ''' <returns>Returns specified length of UTF8 characters</returns>
    Function PeekChars(ByVal Length As Integer) As Char()
        CheckPaused()
        Return Reader.PeekChars(Length)
    End Function

    ''' <summary>
    ''' Reads a boolean value from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a Boolean type value</returns>
    Function PeekBoolean() As Boolean
        CheckPaused()
        Return Reader.PeekBoolean
    End Function

    ''' <summary>
    ''' Reads a single unsigned byte from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns an unsigned byte</returns>
    Function PeekByte() As Byte
        CheckPaused()
        Return Reader.PeekByte
    End Function

    ''' <summary>
    ''' Reads a single signed byte from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a signed byte</returns>
    Function PeekSByte() As SByte
        CheckPaused()
        Return Reader.PeekSByte
    End Function

    ''' <summary>
    ''' Reads a 8bit signed integer from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a signed 8bit integer value</returns>
    Function PeekInt8() As SByte
        CheckPaused()
        Return Reader.PeekInt8
    End Function

    ''' <summary>
    ''' Reads a 8bit unsigned integer from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a 8bit unsigned integer value</returns>
    Function PeekUInt8() As Byte
        CheckPaused()
        Return Reader.PeekUInt8
    End Function

    ''' <summary>
    ''' Reads a 16bit signed integer from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a 16bit signed integer value in currently set endianness</returns>
    Function PeekInt16() As Int16
        CheckPaused()
        Return Reader.PeekInt16(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 16bit unsigned integer from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a 16bit unsigned integer value in currently set endianness</returns>
    Function PeekUInt16() As UInt16
        CheckPaused()
        Return Reader.PeekUInt16(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 16bit signed integer from stream without advancing current stream position
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a 16bit signed integer value in specifed endianness</returns>
    Function PeekInt16(ByVal EndianType As Endian) As Int16
        CheckPaused()
        Return Reader.PeekInt16(EndianType)
    End Function

    ''' <summary>
    ''' Reads a 16bit unsigned integer from stream without advancing current stream position
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a 16bit unsigned integer value in specifed endianness</returns>
    Function PeekUInt16(ByVal EndianType As Endian) As UInt16
        CheckPaused()
        Return Reader.PeekUInt16(EndianType)
    End Function

    ''' <summary>
    ''' Reads a 24bit signed integer from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a 24bit signed integer value in currently set endianness</returns>
    Function PeekInt24() As Int32
        CheckPaused()
        Return Reader.PeekInt24(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 24bit unsigned integer from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a 24bit unsigned integer value in currently set endianness</returns>
    Function PeekUInt24() As UInt32
        CheckPaused()
        Return Reader.PeekUInt24(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 24bit signed integer from stream without advancing current stream position
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a 24bit signed integer value in specifed endianness</returns>
    Function PeekInt24(ByVal EndianType As Endian) As Int32
        CheckPaused()
        Return Reader.PeekInt24(EndianType)
    End Function

    ''' <summary>
    ''' Reads a 24bit unsigned integer from stream without advancing current stream position
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a 24bit unsigned integer value in specifed endianness</returns>
    Function PeekUInt24(ByVal EndianType As Endian) As UInt32
        CheckPaused()
        Return Reader.PeekUInt24(EndianType)
    End Function

    ''' <summary>
    ''' Reads a 32bit signed integer from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a 32bit signed integer value in currently set endianness</returns>
    Function PeekInt32() As Int32
        CheckPaused()
        Return Reader.PeekInt32(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 32bit unsigned integer from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a 32bit unsigned integer value in currently set endianness</returns>
    Function PeekUInt32() As UInt32
        CheckPaused()
        Return Reader.PeekUInt32(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 32bit signed integer from stream without advancing current stream position
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a 32bit signed integer value in specifed endianness</returns>
    Function PeekInt32(ByVal EndianType As Endian) As Int32
        CheckPaused()
        Return Reader.PeekInt32(EndianType)
    End Function

    ''' <summary>
    ''' Reads a 32bit unsigned integer from stream without advancing current stream position
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a 32bit unsigned integer value in specifed endianness</returns>
    Function PeekUInt32(ByVal EndianType As Endian) As UInt32
        CheckPaused()
        Return Reader.PeekUInt32(EndianType)
    End Function

    ''' <summary>
    ''' Reads a 40bit signed integer from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a 40bit signed integer value in currently set endianness</returns>
    Function PeekInt40() As Long
        CheckPaused()
        Return Reader.PeekInt40(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 40bit unsigned integer from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a 40bit unsigned integer value in currently set endianness</returns>
    Function PeekUInt40() As ULong
        CheckPaused()
        Return Reader.PeekUInt40(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 40bit signed integer from stream without advancing current stream position
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a 40bit signed integer value in specifed endianness</returns>
    Function PeekInt40(ByVal EndianType As Endian) As Long
        CheckPaused()
        Return Reader.PeekInt40(EndianType)
    End Function

    ''' <summary>
    ''' Reads a 40bit unsigned integer from stream without advancing current stream position
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a 40bit unsigned integer value in specifed endianness</returns>
    Function PeekUInt40(ByVal EndianType As Endian) As ULong
        CheckPaused()
        Return Reader.PeekUInt40(EndianType)
    End Function

    ''' <summary>
    ''' Reads a 48bit signed integer from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a 48bit signed integer value in currently set endianness</returns>
    Function PeekInt48() As Long
        CheckPaused()
        Return Reader.PeekInt48(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 48bit unsigned integer from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a 48bit unsigned integer value in currently set endianness</returns>
    Function PeekUInt48() As ULong
        CheckPaused()
        Return Reader.PeekUInt48(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 48bit signed integer from stream without advancing current stream position
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a 48bit signed integer value in specifed endianness</returns>
    Function PeekInt48(ByVal EndianType As Endian) As Long
        CheckPaused()
        Return Reader.PeekInt48(EndianType)
    End Function

    ''' <summary>
    ''' Reads a 48bit unsigned integer from stream without advancing current stream position
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a 48bit unsigned integer value in specifed endianness</returns>
    Function PeekUInt48(ByVal EndianType As Endian) As ULong
        CheckPaused()
        Return Reader.PeekUInt48(EndianType)
    End Function

    ''' <summary>
    ''' Reads a 56bit signed integer from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a 56bit signed integer value in currently set endianness</returns>
    Function PeekInt56() As Long
        CheckPaused()
        Return Reader.PeekInt56(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 56bit unsigned integer from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a56bit unsigned integer value in currently set endianness</returns>
    Function PeekUInt56() As ULong
        CheckPaused()
        Return Reader.PeekUInt56(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 56bit signed integer from stream without advancing current stream position
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a 56bit signed integer value in specifed endianness</returns>
    Function PeekInt56(ByVal EndianType As Endian) As Long
        CheckPaused()
        Return Reader.PeekInt56(EndianType)
    End Function

    ''' <summary>
    ''' Reads a 56bit unsigned integer from stream without advancing current stream position
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a 56bit unsigned integer value in specifed endianness</returns>
    Function PeekUInt56(ByVal EndianType As Endian) As ULong
        CheckPaused()
        Return Reader.PeekUInt56(EndianType)
    End Function

    ''' <summary>
    ''' Reads a 64bit signed integer from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a 64bit signed integer value in currently set endianness</returns>
    Function PeekInt64() As Int64
        CheckPaused()
        Return Reader.PeekInt64(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 64bit unsigned integer from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a 64bit unsigned integer value in currently set endianness</returns>
    Function PeekUInt64() As UInt64
        CheckPaused()
        Return Reader.PeekUInt64(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 64bit signed integer from stream without advancing current stream position
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a 64bit signed integer value in specifed endianness</returns>
    Function PeekInt64(ByVal EndianType As Endian) As Int64
        CheckPaused()
        Return Reader.PeekInt64(EndianType)
    End Function

    ''' <summary>
    ''' Reads a 64bit unsigned integer from stream without advancing current stream position
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a 64bit unsigned integer value in specifed endianness</returns>
    Function PeekUInt64(ByVal EndianType As Endian) As UInt64
        CheckPaused()
        Return Reader.PeekUInt64(EndianType)
    End Function

    ''' <summary>
    ''' Reads a 32bit float from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a 32bit float value in currently set endianness</returns>
    Function PeekSingle() As Single
        CheckPaused()
        Return Reader.PeekSingle(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 64bit double from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a 64bit double value in currently set endianness</returns>
    Function PeekDouble() As Double
        CheckPaused()
        Return Reader.PeekDouble(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 32bit float from stream without advancing current stream position
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a 32bit float value in specifed endianness</returns>
    Function PeekSingle(ByVal EndianType As Endian) As Single
        CheckPaused()
        Return Reader.PeekSingle(EndianType)
    End Function

    ''' <summary>
    ''' Reads a 64bit double from stream without advancing current stream position
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a 64bit double value in specifed endianness</returns>
    Function PeekDouble(ByVal EndianType As Endian) As Double
        CheckPaused()
        Return Reader.PeekDouble(EndianType)
    End Function

    ''' <summary>
    ''' Reads a specified count of unsigned bytes from stream without advancing current stream position
    ''' </summary>
    ''' <param name="Length">Number of bytes to read from stream</param>
    ''' <returns>Returns specified length of unsigned bytes in currently set endianess</returns>
    Function PeekBytes(ByVal Length As Integer) As Byte()
        CheckPaused()
        Return Reader.PeekBytes(Length, CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a specified count of unsigned bytes from stream without advancing current stream position
    ''' </summary>
    ''' <param name="Length">Number of bytes to read from stream</param>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns specified length of unsigned bytes in specified endianess</returns>
    Function PeekBytes(ByVal Length As Integer, ByVal EndianType As Endian) As Byte()
        CheckPaused()
        Return Reader.PeekBytes(Length, EndianType)
    End Function

    ''' <summary>
    ''' Reads a specified count of (Length * 8) bits from stream without advancing current stream position
    ''' </summary>
    ''' <param name="Length">Number of bytes to read from stream and convert to binary string</param>
    ''' <returns>Returns a (Length * 8) long string containing bits in currently set endianness</returns>
    Function PeekBinary(ByVal Length As Integer) As String
        CheckPaused()
        Return Reader.PeekBinary(Length, CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a specified count of (Length * 8) bits from stream without advancing current stream position
    ''' </summary>
    ''' <param name="Length">Number of bytes to read from stream and convert to binary string</param>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a (Length * 8) long string containing bits in specified endianness</returns>
    Function PeekBinary(ByVal Length As Integer, ByVal EndianType As Endian) As String
        CheckPaused()
        Return Reader.PeekBinary(Length, EndianType)
    End Function

    ''' <summary>
    ''' Reads a specified count of ASCII encoded charaters from stream without advancing current stream position
    ''' </summary>
    ''' <param name="Length">Number of charaters to read from the stream</param>
    ''' <returns>Returns a string containing specified length of ASCII encoded characters</returns>
    Function PeekASCII(ByVal Length As Integer) As String
        CheckPaused()
        Return Reader.PeekASCII(Length)
    End Function

    ''' <summary>
    ''' Reads a specified count of Unicode encoded charaters from stream without advancing current stream position
    ''' </summary>
    ''' <param name="Length">Number of charaters to read from the stream</param>
    ''' <returns>Returns a string containing specified length of Unicode encoded characters in currently set endianness</returns>
    Function PeekUnicode(ByVal Length As Integer) As String
        CheckPaused()
        Return Reader.PeekUnicode(Length, CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a specified count of Unicode encoded charaters from stream without advancing current stream position
    ''' </summary>
    ''' <param name="Length">Number of charaters to read from the stream</param>
    ''' <param name="EndianType">Endianness in which to read characters</param>
    ''' <returns>Returns a string containing specified length of Unicode encoded characters in specified endianness</returns>
    Function PeekUnicode(ByVal Length As Integer, ByVal EndianType As Endian) As String
        CheckPaused()
        Return Reader.PeekUnicode(Length, EndianType)
    End Function

    ''' <summary>
    ''' Reads a specified count of Hexadecimal values from stream without advancing current stream position
    ''' </summary>
    ''' <param name="Length">Number of bytes to read from the stream</param>
    ''' <returns>Returns a string containing specified length of Hexadecimal values in currently set endianness</returns>
    Function PeekHex(ByVal Length As Integer) As String
        CheckPaused()
        Return Reader.PeekHex(Length, CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a specified count of Hexadecimal values from stream without advancing current stream position
    ''' </summary>
    ''' <param name="Length">Number of bytes to read from the stream</param>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a string containing specified length of Hexadecimal values in specified endianness</returns>
    Function PeekHex(ByVal Length As Integer, ByVal EndianType As Endian) As String
        CheckPaused()
        Return Reader.PeekHex(Length, EndianType)
    End Function

    ''' <summary>
    ''' Reads a boolean value from stream and advances stream position by 1
    ''' </summary>
    ''' <returns>Returns a Boolean type value</returns>
    Function ReadBoolean() As Boolean
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadBoolean
    End Function

    ''' <summary>
    ''' Reads a single unsigned byte from stream and advances stream position by 1
    ''' </summary>
    ''' <returns>Returns an unsigned byte</returns>
    Function ReadByte() As Byte
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadByte
    End Function

    ''' <summary>
    ''' Reads a single signed byte from stream and advances stream position by 1
    ''' </summary>
    ''' <returns>Returns a signed byte</returns>
    Function ReadSByte() As SByte
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadSByte
    End Function

    ''' <summary>
    ''' Reads a count of unsigned bytes from stream and advances stream position by that count
    ''' </summary>
    ''' <param name="Length">Number of bytes to read from stream</param>
    ''' <returns>Returns a buffer of unsigned bytes</returns>
    Function ReadBytes(ByVal Length As Integer) As Byte()
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadBytes(Length, CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a count of unsigned bytes from stream and advances stream position by that count
    ''' </summary>
    ''' <param name="Length">Number of bytes to read from stream</param>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a buffer of unsigned bytes</returns>
    Function ReadBytes(ByVal Length As Integer, ByVal EndianType As Endian) As Byte()
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadBytes(Length, EndianType)
    End Function

    ''' <summary>
    ''' Reads all unsigned bytes from stream and moves stream position to end of stream
    ''' </summary>
    ''' <returns>Returns a buffer of unsigned bytes containing all data in stream</returns>
    Function ReadAll() As Byte()
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadAll(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads all unsigned bytes from stream and moves stream position to end of stream
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a buffer of unsigned bytes containing all data in stream</returns>
    Function ReadAll(ByVal EndianType As Endian) As Byte()
        CheckPaused()
        Position = 0
        Return Reader.ReadBytes(Length, EndianType)
    End Function

    ''' <summary>
    ''' Reads all unsigned bytes from current stream position to end of stream and moves stream position to end of stream
    ''' </summary>
    ''' <returns>Returns a buffer of unsigned bytes containing all data from current position to end of stream</returns>
    Function ReadToEnd() As Byte()
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadToEnd(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads all unsigned bytes from current stream position to end of stream and moves stream position to end of stream
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a buffer of unsigned bytes containing all data from current position to end of stream</returns>
    Function ReadToEnd(ByVal EndianType As Endian) As Byte()
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadToEnd(EndianType)
    End Function

    ''' <summary>
    ''' Reads a single signed byte from stream and advances stream position by 1
    ''' </summary>
    ''' <returns>Returns a signed byte</returns>
    Function ReadInt8() As SByte
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadInt8
    End Function

    ''' <summary>
    ''' Reads a single unsigned byte from stream and advances stream position by 1
    ''' </summary>
    ''' <returns>Returns an unsigned byte</returns>
    Function ReadUInt8() As Byte
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadUInt8
    End Function

    ''' <summary>
    ''' Reads a 16bit signed integer from stream and advances stream position by 2
    ''' </summary>
    ''' <returns>Returns a 16bit signed integer value in currently set endianness</returns>
    Function ReadInt16() As Int16
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadInt16(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 16bit signed integer from stream and advances stream position by 2
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a 16bit signed integer value in specifed endianness</returns>
    Function ReadInt16(ByVal EndianType As Endian) As Int16
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadInt16(EndianType)
    End Function

    ''' <summary>
    ''' Reads a 16bit unsigned integer from stream and advances stream position by 2
    ''' </summary>
    ''' <returns>Returns a 16bit unsigned integer value in currently set endianness</returns>
    Function ReadUInt16() As UInt16
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadUInt16(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 16bit unsigned integer from stream and advances stream position by 2
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a 16bit unsigned integer value in specifed endianness</returns>
    Function ReadUInt16(ByVal EndianType As Endian) As UInt16
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadUInt16(EndianType)
    End Function

    ''' <summary>
    ''' Reads a 24bit signed integer from stream and advances stream position by 3
    ''' </summary>
    ''' <returns>Returns a 24bit signed integer value in currently set endianness</returns>
    Function ReadInt24() As Integer
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadInt24(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 24bit signed integer from stream and advances stream position by 3
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a 24bit signed integer value in specifed endianness</returns>
    Function ReadInt24(ByVal EndianType As Endian) As Integer
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadInt24(EndianType)
    End Function

    ''' <summary>
    ''' Reads a 24bit unsigned integer from stream and advances stream position by 3
    ''' </summary>
    ''' <returns>Returns a 24bit unsigned integer value in currently set endianness</returns>
    Function ReadUInt24() As UInteger
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadUInt24(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 24bit unsigned integer from stream and advances stream position by 3
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a 24bit unsigned integer value in specifed endianness</returns>
    Function ReadUInt24(ByVal EndianType As Endian) As UInteger
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadUInt24(EndianType)
    End Function

    ''' <summary>
    ''' Reads a 32bit signed integer from stream and advances stream position by 4
    ''' </summary>
    ''' <returns>Returns a 32bit signed integer value in currently set endianness</returns>
    Function ReadInt32() As Int32
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadInt32(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 32bit signed integer from stream and advances stream position by 4
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a 32bit signed integer value in specifed endianness</returns>
    Function ReadInt32(ByVal EndianType As Endian) As Int32
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadInt32(EndianType)
    End Function

    ''' <summary>
    ''' Reads a 32bit unsigned integer from stream and advances stream position by 4
    ''' </summary>
    ''' <returns>Returns a 32bit unsigned integer value in currently set endianness</returns>
    Function ReadUInt32() As UInt32
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadUInt32(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 32bit unsigned integer from stream and advances stream position by 4
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a 32bit unsigned integer value in specifed endianness</returns>
    Function ReadUInt32(ByVal EndianType As Endian) As UInt32
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadUInt32(EndianType)
    End Function

    ''' <summary>
    ''' Reads a 40bit signed integer from stream and advances stream position by 5
    ''' </summary>
    ''' <returns>Returns a 40bit signed integer value in currently set endianness</returns>
    Function ReadInt40() As Long
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadInt40(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 40bit signed integer from stream and advances stream position by 5
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a 40bit signed integer value in specifed endianness</returns>
    Function ReadInt40(ByVal EndianType As Endian) As Long
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadInt40(EndianType)
    End Function

    ''' <summary>
    ''' Reads a 40bit unsigned integer from stream and advances stream position by 5
    ''' </summary>
    ''' <returns>Returns a 40bit unsigned integer value in currently set endianness</returns>
    Function ReadUInt40() As ULong
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadUInt40(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 40bit unsigned integer from stream and advances stream position by 5
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a 40bit unsigned integer value in specifed endianness</returns>
    Function ReadUInt40(ByVal EndianType As Endian) As ULong
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadUInt40(EndianType)
    End Function

    ''' <summary>
    ''' Reads a 48bit signed integer from stream and advances stream position by 6
    ''' </summary>
    ''' <returns>Returns a 48bit signed integer value in currently set endianness</returns>
    Function ReadInt48() As Long
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadInt48(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 48bit signed integer from stream and advances stream position by 6
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a 48bit signed integer value in specifed endianness</returns>
    Function ReadInt48(ByVal EndianType As Endian) As Long
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadInt48(EndianType)
    End Function

    ''' <summary>
    ''' Reads a 48bit signed uninteger from stream and advances stream position by 6
    ''' </summary>
    ''' <returns>Returns a 48bit unsigned integer value in currently set endianness</returns>
    Function ReadUInt48() As ULong
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadUInt48(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 48bit unsigned integer from stream and advances stream position by 6
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a 48bit unsigned integer value in specifed endianness</returns>
    Function ReadUInt48(ByVal EndianType As Endian) As ULong
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadUInt48(EndianType)
    End Function

    ''' <summary>
    ''' Reads a 56bit signed integer from stream and advances stream position by 7
    ''' </summary>
    ''' <returns>Returns a 56bit signed integer value in currently set endianness</returns>
    Function ReadInt56() As Long
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadInt56(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 56bit signed integer from stream and advances stream position by 7
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a 56bit signed integer value in specifed endianness</returns>
    Function ReadInt56(ByVal EndianType As Endian) As Long
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadInt56(EndianType)
    End Function

    ''' <summary>
    ''' Reads a 56bit unsigned integer from stream and advances stream position by 7
    ''' </summary>
    ''' <returns>Returns a 56bit unsigned integer value in currently set endianness</returns>
    Function ReadUInt56() As ULong
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadUInt56(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 56bit unsigned integer from stream and advances stream position by 7
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a 56bit unsigned integer value in specifed endianness</returns>
    Function ReadUInt56(ByVal EndianType As Endian) As ULong
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadUInt56(EndianType)
    End Function

    ''' <summary>
    ''' Reads a 64bit signed integer from stream and advances stream position by 8
    ''' </summary>
    ''' <returns>Returns a 64bit signed integer value in currently set endianness</returns>
    Function ReadInt64() As Int64
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadInt64(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 64bit signed integer from stream and advances stream position by 8
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a 64bit signed integer value in specifed endianness</returns>
    Function ReadInt64(ByVal EndianType As Endian) As Int64
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadInt64(EndianType)
    End Function

    ''' <summary>
    ''' Reads a 64bit unsigned integer from stream and advances stream position by 8
    ''' </summary>
    ''' <returns>Returns a 64bit unsigned integer value in currently set endianness</returns>
    Function ReadUInt64() As UInt64
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadUInt64(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 64bit unsigned integer from stream and advances stream position by 8
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a 64bit unsigned integer value in specifed endianness</returns>
    Function ReadUInt64(ByVal EndianType As Endian) As UInt64
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadUInt64(EndianType)
    End Function

    ''' <summary>
    ''' Reads a 32bit float from stream and advances stream position by 4
    ''' </summary>
    ''' <returns>Returns a 32bit float value in currently set endianness</returns>
    Function ReadSingle() As Single
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadSingle(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 32bit float from stream and advances stream position by 4
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a 32bit float value in specifed endianness</returns>
    Function ReadSingle(ByVal EndianType As Endian) As Single
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadSingle(EndianType)
    End Function

    ''' <summary>
    ''' Reads a 64bit double from stream and advances stream position by 8
    ''' </summary>
    ''' <returns>Returns a 64bit double value in currently set endianness</returns>
    Function ReadDouble() As Double
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadDouble(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 64bit double from stream and advances stream position by 8
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a 64bit double value in specifed endianness</returns>
    Function ReadDouble(ByVal EndianType As Endian) As Double
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadDouble(EndianType)
    End Function

    ''' <summary>
    ''' Reads a single UTF8 encoded character from stream and advances stream position by 1
    ''' </summary>
    ''' <returns>Returns a single UTF8 character</returns>
    Function ReadChar() As Char
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadChar
    End Function

    ''' <summary>
    ''' Reads specified count of UTF8 encoded characters from stream and advances stream position by count
    ''' </summary>
    ''' <param name="Length">Number of characters to read from stream</param>
    ''' <returns>Returns specified length of UTF8 characters</returns>
    Function ReadChars(ByVal Length As Integer) As Char()
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadChars(Length)
    End Function

    ''' <summary>
    ''' Reads a specified count of (Length * 8) bits from stream and advances stream position by count (Length * 8)
    ''' </summary>
    ''' <param name="Length">Number of bytes to read from stream and convert to binary string</param>
    ''' <returns>Returns a (Length * 8) long string containing bits in currently set endianness</returns>
    Function ReadBinary(ByVal Length As Integer) As String
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadBinary(Length, CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a specified count of (Length * 8) bits from stream and advances stream position by count (Length * 8)
    ''' </summary>
    ''' <param name="Length">Number of bytes to read from stream and convert to binary string</param>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a (Length * 8) long string containing bits in currently set endianness</returns>
    Function ReadBinary(ByVal Length As Integer, ByVal EndianType As Endian) As String
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadBinary(Length, EndianType)
    End Function

    ''' <summary>
    ''' Reads a specified count of ASCII encoded charaters from stream and advances stream position by count
    ''' </summary>
    ''' <param name="Length">Number of charaters to read from the stream</param>
    ''' <returns>Returns a string containing specified length of ASCII encoded characters</returns>
    Function ReadASCII(ByVal Length As Integer) As String
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadASCII(Length)
    End Function

    ''' <summary>
    ''' Reads a specified count of Unicode encoded charaters from stream and advances stream position by count
    ''' </summary>
    ''' <param name="Length">Number of charaters to read from the stream</param>
    ''' <returns>Returns a string containing specified length of Unicode encoded characters in currently set endianness</returns>
    Function ReadUnicode(ByVal Length As Integer) As String
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadUnicode(Length, CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a specified count of Unicode encoded charaters from stream and advances stream position by count
    ''' </summary>
    ''' <param name="Length">Number of charaters to read from the stream</param>
    ''' <param name="EndianType">Endianness in which to read characters</param>
    ''' <returns>Returns a string containing specified length of Unicode encoded characters in specified endianness</returns>
    Function ReadUnicode(ByVal Length As Integer, ByVal EndianType As Endian) As String
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadUnicode(Length, EndianType)
    End Function

    ''' <summary>
    ''' Reads a specified count of Hexadecimal values from stream and advances stream position by count
    ''' </summary>
    ''' <param name="Length">Number of bytes to read from the stream</param>
    ''' <returns>Returns a string containing specified length of Hexadecimal values in currently set endianness</returns>
    Function ReadHex(ByVal Length As Integer) As String
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadHex(Length, CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a specified count of Hexadecimal values from stream and advances stream position by count
    ''' </summary>
    ''' <param name="Length">Number of bytes to read from the stream</param>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a string containing specified length of Hexadecimal values in specified endianness</returns>
    Function ReadHex(ByVal Length As Integer, ByVal EndianType As Endian) As String
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadHex(Length, EndianType)
    End Function

    ''' <summary>
    ''' Searches underlying stream for instances of specified criteria
    ''' </summary>
    ''' <param name="Criteria">A sequence of ASCII, Unicode, or Hexadecimal characters to look for during the search</param>
    ''' <param name="StringType">The encoding scheme to use with the criteria</param>
    ''' <param name="ComparisonType">The StringComparison enumeration that represents how the criteria will be compared against data in the underlying stream during the search operation. Optional</param>
    ''' <param name="Position">The offset in the underlying stream to begin the search from. If -1 then current stream position is used. Optional.</param>
    ''' <param name="ReturnOnFirst">Whether to return results after a single instance of the criteria is found. If false, search will continue until EndOffset value is reached. Optional.</param>
    ''' <param name="EndPosition">The offset in the underlying stream to stop the search at. If -1 then end of stream is used. Optional.</param>
    ''' <param name="ThrowErrorIfNotFound">If true an exception will be thrown if no instances of the criteria are found within search parameters, else Nothing (null) will be returned. Optional</param>
    ''' <returns>Returns an array of 64bit signed integers containing all offsets in the underlying stream the criteria was found. Returned offsets represent the position where criteria starts
    ''' If nothing is found then Nothing (null) is returned unless otherwise specified</returns>
    Function SearchString(ByRef Criteria As String, ByVal StringType As StringType, Optional ByVal ComparisonType As StringComparison = 4, Optional ByVal Position As Long = -1, Optional ByVal ReturnOnFirst As Boolean = True, Optional ByVal EndPosition As Long = -1, Optional ByVal ThrowErrorIfNotFound As Boolean = False) As Long()
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.SearchString(Criteria, StringType, ComparisonType, Position, ReturnOnFirst, EndPosition, ThrowErrorIfNotFound)
    End Function

    ''' <summary>
    ''' Searches underlying stream for instances of specified criteria
    ''' </summary>
    ''' <param name="Criteria">A signed or unsigned integer value to look for during the search</param>
    ''' <param name="IntType">The bit width of the criteria value</param>
    ''' <param name="Position">The offset in the underlying stream to begin the search from. If -1 then current stream position is used. Optional.</param>
    ''' <param name="ReturnOnFirst">Whether to return results after a single instance of the criteria is found. If false, search will continue until EndOffset value is reached. Optional.</param>
    ''' <param name="EndPosition">The offset in the underlying stream to stop the search at. If -1 then end of stream is used. Optional.</param>
    ''' <param name="ThrowErrorIfNotFound">If true an exception will be thrown if no instances of the criteria are found within search parameters, else Nothing (null) will be returned. Optional</param>
    ''' <returns>Returns an array of 64bit signed integers containing all offsets in the underlying stream the criteria was found. Returned offsets represent the position where criteria starts
    ''' If nothing is found then Nothing (null) is returned unless otherwise specified</returns>
    Function SearchNumeric(ByRef Criteria As Object, ByVal IntType As IntegerType, Optional ByVal Position As Long = -1, Optional ByVal ReturnOnFirst As Boolean = True, Optional ByVal EndPosition As Long = -1, Optional ByVal ThrowErrorIfNotFound As Boolean = False) As Long()
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.SearchNumeric(Criteria, IntType, Position, ReturnOnFirst, EndPosition, ThrowErrorIfNotFound)
    End Function

    ''' <summary>
    ''' Searches underlying stream for instances of specified criteria
    ''' </summary>
    ''' <param name="Criteria">A sequence of bytes to look for during the search</param>
    ''' <param name="Position">The offset in the underlying stream to begin the search from. If -1 then current stream position is used. Optional.</param>
    ''' <param name="ReturnOnFirst">Whether to return results after a single instance of the criteria is found. If false, search will continue until EndOffset value is reached. Optional.</param>
    ''' <param name="EndPosition">The offset in the underlying stream to stop the search at. If -1 then end of stream is used. Optional.</param>
    ''' <param name="ThrowErrorIfNotFound">If true an exception will be thrown if no instances of the criteria are found within search parameters, else Nothing (null) will be returned. Optional</param>
    ''' <returns>Returns an array of 64bit signed integers containing all offsets in the underlying stream the criteria was found. Returned offsets represent the position where criteria starts
    ''' If nothing is found then Nothing (null) is returned unless otherwise specified</returns>
    Function Search(ByRef Criteria As Byte(), Optional ByVal Position As Long = -1, Optional ByVal ReturnOnFirst As Boolean = True, Optional ByVal EndPosition As Long = -1, Optional ByVal ThrowErrorIfNotFound As Boolean = False) As Long()
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.Search(Criteria, Position, ReturnOnFirst, EndPosition, ThrowErrorIfNotFound)
    End Function

    ''' <summary>
    ''' Asks any current search operations to abort as soon as possible and return any results found
    ''' </summary>
    Sub AbortSearch()
        If Not IsSearching Then Return
        Reader.AbortSearch()
    End Sub

#End Region

#Region "Writing"

    ''' <summary>
    ''' Writes bytes to underlying stream and advances stream position by count of bytes
    ''' </summary>
    ''' <param name="Buffer">Bytes to be written</param>
    Sub Write(ByVal Buffer As Byte())
        CheckPaused()
        Last = OpenStream.Position
        Writer.Write(Buffer, Buffer.Length, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes bytes to underlying stream and advances stream position by count
    ''' </summary>
    ''' <param name="Buffer">Bytes to be written</param>
    ''' <param name="Length">Count of bytes to be written</param>
    Sub Write(ByVal Buffer As Byte(), ByVal Length As Integer)
        CheckPaused()
        Last = OpenStream.Position
        Writer.Write(Buffer, Length, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes bytes to underlying stream and advances stream position by count
    ''' </summary>
    ''' <param name="Buffer">Bytes to be written</param>
    ''' <param name="Length">Count of bytes to be written</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub Write(ByVal Buffer As Byte(), ByVal Length As Integer, ByVal EndianType As Endian)
        CheckPaused()
        Last = OpenStream.Position
        Writer.Write(Buffer, Length, EndianType)
    End Sub

    ''' <summary>
    ''' Writes bytes to underlying stream and advances stream position by count of bytes
    ''' </summary>
    ''' <param name="Buffer">Bytes to be written</param>
    Sub WriteBytes(ByVal Buffer As Byte())
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteBytes(Buffer, Buffer.Length, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes bytes to underlying stream and advances stream position by count
    ''' </summary>
    ''' <param name="Buffer">Bytes to be written</param>
    ''' <param name="Length">Count of bytes to be written</param>
    Sub WriteBytes(ByVal Buffer As Byte(), ByVal Length As Integer)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteBytes(Buffer, Length, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes bytes to underlying stream and advances stream position by count
    ''' </summary>
    ''' <param name="Buffer">Bytes to be written</param>
    ''' <param name="Length">Count of bytes to be written</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteBytes(ByVal Buffer As Byte(), ByVal Length As Integer, ByVal EndianType As Endian)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteBytes(Buffer, Length, EndianType)
    End Sub

    ''' <summary>
    ''' Inserts bytes to underlying stream and advances stream position by count of bytes inserted
    ''' </summary>
    ''' <param name="Buffer">Bytes to be inserted</param>
    Sub Insert(ByVal Buffer As Byte())
        Insert(Buffer, Buffer.Length)
    End Sub

    ''' <summary>
    ''' Inserts bytes to underlying stream and advances stream position by count
    ''' </summary>
    ''' <param name="Buffer">Bytes to be inserted</param>
    ''' <param name="Length">Count of bytes to be inserted</param>
    Sub Insert(ByVal Buffer As Byte(), ByVal Length As Integer)
        Insert(Buffer, Length, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Inserts bytes to underlying stream and advances stream position by count
    ''' </summary>
    ''' <param name="Buffer">Bytes to be inserted</param>
    ''' <param name="Length">Count of bytes to be inserted</param>
    ''' <param name="EndianType">Endianness in which to insert data</param>
    Sub Insert(ByVal Buffer As Byte(), ByVal Length As Integer, ByVal EndianType As Endian)
        CheckPaused()
        Writer.Insert(Buffer, Length, EndianType)
    End Sub

    ''' <summary>
    ''' Inserts bytes to underlying stream and advances stream position by count of bytes inserted
    ''' </summary>
    ''' <param name="Buffer">Bytes to be inserted</param>
    Sub InsertBytes(ByVal Buffer As Byte())
        Insert(Buffer)
    End Sub

    ''' <summary>
    ''' Inserts bytes to underlying stream and advances stream position by count
    ''' </summary>
    ''' <param name="Buffer">Bytes to be inserted</param>
    ''' <param name="Length">Count of bytes to be inserted</param>
    Sub InsertBytes(ByVal Buffer As Byte(), ByVal Length As Integer)
        Insert(Buffer, Length)
    End Sub

    ''' <summary>
    ''' Inserts bytes to underlying stream and advances stream position by count
    ''' </summary>
    ''' <param name="Buffer">Bytes to be inserted</param>
    ''' <param name="Length">Count of bytes to be inserted</param>
    ''' <param name="EndianType">Endianness in which to insert data</param>
    Sub InsertBytes(ByVal Buffer As Byte(), ByVal Length As Integer, ByVal EndianType As Endian)
        Insert(Buffer, Length, EndianType)
    End Sub

    ''' <summary>
    ''' Removes (deletes) bytes from underlying stream and leaves stream position untouched
    ''' </summary>
    ''' <param name="Length">Count of bytes to be removed (deleted)</param>
    Sub Delete(ByVal Length As Integer)
        CheckPaused()
        If Length = 0 OrElse LengthToEnd = 0 Then Return
        If (LengthToEnd - Length) < 0 Then Throw New DataException("Length is longer than the number of bytes left to end of stream. Not enough bytes to delete")
        If Not Functions.AvailableMemory >= (Me.Length + Length) Then Throw New System.OutOfMemoryException("There is not enough memory availible to the system to delete specified length of bytes")
        If Type = StreamType.FileStream AndAlso Not OpenStream.CanRead Then FileAccess = System.IO.FileAccess.ReadWrite
        Dim x As Long = Position
        Position = 0
        Flush()
        Dim Buffer As Byte() = Functions.DeleteBytes(Reader.ReadAll(Endian.Little), x, Length)
        If Type = StreamType.FileStream Then
            File.Delete(OpenFile)
            File.Create(OpenFile).Close()
            OpenStream = New FileStream(OpenFile, FileMode.Open, Access, Share)
        Else
            OpenStream = New MemoryStream(Buffer, True)
        End If
        Reader = New Reader(OpenStream, CurrentEndian)
        Writer = New Writer(OpenStream, CurrentEndian)
        Write(Buffer, Buffer.Length, Endian.Little)
        Flush()
        Position = x
    End Sub

    ''' <summary>
    ''' Removes (deletes) bytes from underlying stream and leaves stream position untouched
    ''' </summary>
    ''' <param name="Length">Count of bytes to be removed (deleted)</param>
    Sub DeleteBytes(ByVal Length As Integer)
        Delete(Length)
    End Sub

    ''' <summary>
    ''' Writes zeroed (00) bytes to underlying stream and advances stream position by count
    ''' </summary>
    ''' <param name="Length">Count of zeroed bytes to write</param>
    Sub WriteNull(ByVal Length As Integer)
        Writer.WriteNull(Length)
    End Sub

    ''' <summary>
    ''' Writes a single unsigned byte to underlying stream and advances stream position by 1
    ''' </summary>
    ''' <param name="Value">Unsigned byte to write to underlying stream</param>
    Sub WriteByte(ByVal Value As Byte)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteByte(Value)
    End Sub

    ''' <summary>
    ''' Writes a single signed byte to underlying stream and advances stream position by 1
    ''' </summary>
    ''' <param name="Value">Signed byte to write to underlying stream</param>
    Sub WriteSByte(ByVal Value As SByte)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteSByte(Value)
    End Sub

    ''' <summary>
    ''' Writes a single signed byte to underlying stream and advances stream position by 1
    ''' </summary>
    ''' <param name="Value">Signed byte to write to underlying stream</param>
    Sub WriteInt8(ByVal Value As SByte)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteInt8(Value)
    End Sub

    ''' <summary>
    ''' Writes a single unsigned byte to underlying stream and advances stream position by 1
    ''' </summary>
    ''' <param name="Value">Unsigned byte to write to underlying stream</param>
    Sub WriteUInt8(ByVal Value As Byte)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteInt8(Value)
    End Sub

    ''' <summary>
    ''' Writes a 16bit signed integer to underlying stream and advances stream position by 2
    ''' </summary>
    ''' <param name="Value">A 16bit signed integer value to write to underlying stream</param>
    Sub WriteInt16(ByVal Value As Short)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteInt16(Value, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a 16bit signed integer to underlying stream and advances stream position by 2
    ''' </summary>
    ''' <param name="Value">A 16bit signed integer value to write to underlying stream</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteInt16(ByVal Value As Short, ByVal EndianType As Endian)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteInt16(Value, EndianType)
    End Sub

    ''' <summary>
    ''' Writes a 16bit unsigned integer to underlying stream and advances stream position by 2
    ''' </summary>
    ''' <param name="Value">A 16bit unsigned integer value to write to underlying stream</param>
    Sub WriteUInt16(ByVal Value As UShort)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteUInt16(Value, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a 16bit unsigned integer to underlying stream and advances stream position by 2
    ''' </summary>
    ''' <param name="Value">A 16bit unsigned integer value to write to underlying stream</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteUInt16(ByVal Value As UShort, ByVal EndianType As Endian)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteUInt16(Value, EndianType)
    End Sub

    ''' <summary>
    ''' Writes a 24bit signed integer to underlying stream and advances stream position by 3
    ''' </summary>
    ''' <param name="Value">A 24bit signed integer value to write to underlying stream</param>
    Sub WriteInt24(ByVal Value As Integer)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteInt24(Value, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a 24bit signed integer to underlying stream and advances stream position by 3
    ''' </summary>
    ''' <param name="Value">A 24bit signed integer value to write to underlying stream</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteInt24(ByVal Value As Integer, ByVal EndianType As Endian)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteInt24(Value, EndianType)
    End Sub

    ''' <summary>
    ''' Writes a 24bit unsigned integer to underlying stream and advances stream position by 3
    ''' </summary>
    ''' <param name="Value">A 24bit unsigned integer value to write to underlying stream</param>
    Sub WriteUInt24(ByVal Value As UInteger)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteUInt24(Value, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a 24bit unsigned integer to underlying stream and advances stream position by 3
    ''' </summary>
    ''' <param name="Value">A 24bit unsigned integer value to write to underlying stream</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteUInt24(ByVal Value As UInteger, ByVal EndianType As Endian)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteUInt24(Value, EndianType)
    End Sub

    ''' <summary>
    ''' Writes a 32bit signed integer to underlying stream and advances stream position by 4
    ''' </summary>
    ''' <param name="Value">A 32bit signed integer value to write to underlying stream</param>
    Sub WriteInt32(ByVal Value As Int32)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteInt32(Value, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a 32bit signed integer to underlying stream and advances stream position by 4
    ''' </summary>
    ''' <param name="Value">A 32bit signed integer value to write to underlying stream</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteInt32(ByVal Value As Int32, ByVal EndianType As Endian)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteInt32(Value, EndianType)
    End Sub

    ''' <summary>
    ''' Writes a 32bit unsigned integer to underlying stream and advances stream position by 4
    ''' </summary>
    ''' <param name="Value">A 32bit unsigned integer value to write to underlying stream</param>
    Sub WriteUInt32(ByVal Value As UInt32)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteUInt32(Value, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a 32bit unsigned integer to underlying stream and advances stream position by 4
    ''' </summary>
    ''' <param name="Value">A 32bit unsigned integer value to write to underlying stream</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteUInt32(ByVal Value As UInt32, ByVal EndianType As Endian)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteUInt32(Value, EndianType)
    End Sub

    ''' <summary>
    ''' Writes a 40bit signed integer to underlying stream and advances stream position by 5
    ''' </summary>
    ''' <param name="Value">A 40bit signed integer value to write to underlying stream</param>
    Sub WriteInt40(ByVal Value As Long)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteInt40(Value, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a 40bit signed integer to underlying stream and advances stream position by 5
    ''' </summary>
    ''' <param name="Value">A 40bit signed integer value to write to underlying stream</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteInt40(ByVal Value As Long, ByVal EndianType As Endian)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteInt40(Value, EndianType)
    End Sub

    ''' <summary>
    ''' Writes a 40bit unsigned integer to underlying stream and advances stream position by 5
    ''' </summary>
    ''' <param name="Value">A 40bit unsigned integer value to write to underlying stream</param>
    Sub WriteUInt40(ByVal Value As ULong)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteUInt40(Value, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a 40bit unsigned integer to underlying stream and advances stream position by 5
    ''' </summary>
    ''' <param name="Value">A 40bit unsigned integer value to write to underlying stream</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteUInt40(ByVal Value As ULong, ByVal EndianType As Endian)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteUInt40(Value, EndianType)
    End Sub

    ''' <summary>
    ''' Writes a 48bit signed integer to underlying stream and advances stream position by 6
    ''' </summary>
    ''' <param name="Value">A 48bit signed integer value to write to underlying stream</param>
    Sub WriteInt48(ByVal Value As Long)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteInt48(Value, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a 48bit signed integer to underlying stream and advances stream position by 6
    ''' </summary>
    ''' <param name="Value">A 48bit signed integer value to write to underlying stream</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteInt48(ByVal Value As Long, ByVal EndianType As Endian)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteInt48(Value, EndianType)
    End Sub

    ''' <summary>
    ''' Writes a 48bit unsigned integer to underlying stream and advances stream position by 6
    ''' </summary>
    ''' <param name="Value">A 48bit unsigned integer value to write to underlying stream</param>
    Sub WriteUInt48(ByVal Value As ULong)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteUInt48(Value, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a 48bit unsigned integer to underlying stream and advances stream position by 6
    ''' </summary>
    ''' <param name="Value">A 48bit unsigned integer value to write to underlying stream</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteUInt48(ByVal Value As ULong, ByVal EndianType As Endian)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteUInt48(Value, EndianType)
    End Sub

    ''' <summary>
    ''' Writes a 56bit signed integer to underlying stream and advances stream position by 7
    ''' </summary>
    ''' <param name="Value">A 56bit signed integer value to write to underlying stream</param>
    Sub WriteInt56(ByVal Value As Long)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteInt56(Value, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a 56bit signed integer to underlying stream and advances stream position by 7
    ''' </summary>
    ''' <param name="Value">A 56bit signed integer value to write to underlying stream</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteInt56(ByVal Value As Long, ByVal EndianType As Endian)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteInt56(Value, EndianType)
    End Sub

    ''' <summary>
    ''' Writes a 56bit unsigned integer to underlying stream and advances stream position by 7
    ''' </summary>
    ''' <param name="Value">A 56bit unsigned integer value to write to underlying stream</param>
    Sub WriteUInt56(ByVal Value As ULong)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteUInt56(Value, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a 56bit unsigned integer to underlying stream and advances stream position by 7
    ''' </summary>
    ''' <param name="Value">A 56bit unsigned integer value to write to underlying stream</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteUInt56(ByVal Value As ULong, ByVal EndianType As Endian)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteUInt56(Value, EndianType)
    End Sub

    ''' <summary>
    ''' Writes a 64bit signed integer to underlying stream and advances stream position by 8
    ''' </summary>
    ''' <param name="Value">A 64bit signed integer value to write to underlying stream</param>
    Sub WriteInt64(ByVal Value As Int64)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteInt64(Value, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a 64bit signed integer to underlying stream and advances stream position by 8
    ''' </summary>
    ''' <param name="Value">A 64bit signed integer value to write to underlying stream</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteInt64(ByVal Value As Int64, ByVal EndianType As Endian)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteInt64(Value, EndianType)
    End Sub

    ''' <summary>
    ''' Writes a 64bit unsigned integer to underlying stream and advances stream position by 8
    ''' </summary>
    ''' <param name="Value">A 64bit unsigned integer value to write to underlying stream</param>
    Sub WriteUInt64(ByVal Value As UInt64)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteUInt64(Value, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a 64bit unsigned integer to underlying stream and advances stream position by 8
    ''' </summary>
    ''' <param name="Value">A 64bit unsigned integer value to write to underlying stream</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteUInt64(ByVal Value As UInt64, ByVal EndianType As Endian)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteUInt64(Value, EndianType)
    End Sub

    ''' <summary>
    ''' Writes a 32bit float to underlying stream and advances stream position by 4
    ''' </summary>
    ''' <param name="Value">A 32bit float value to write to underlying stream</param>
    Sub WriteSingle(ByVal Value As Single)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteSingle(Value, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a 32bit float to underlying stream and advances stream position by 4
    ''' </summary>
    ''' <param name="Value">A 32bit float value to write to underlying stream</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteSingle(ByVal Value As Single, ByVal EndianType As Endian)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteSingle(Value, EndianType)
    End Sub

    ''' <summary>
    ''' Writes a 64bit double to underlying stream and advances stream position by 8
    ''' </summary>
    ''' <param name="Value">A 64bit double float value to write to underlying stream</param>
    Sub WriteDouble(ByVal Value As Double)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteDouble(Value, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a 64bit double to underlying stream and advances stream position by 8
    ''' </summary>
    ''' <param name="Value">A 64bit double value to write to underlying stream</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteDouble(ByVal Value As Double, ByVal EndianType As Endian)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteDouble(Value, EndianType)
    End Sub

    ''' <summary>
    ''' Writes a single UTF8 encoded character to underlying stream and advances stream position by 1
    ''' </summary>
    ''' <param name="Value">A single character to write to underlying stream</param>
    Sub WriteChar(ByVal Value As Char)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteChar(Value)
    End Sub

    ''' <summary>
    ''' Writes a sequence UTF8 encoded characters to underlying stream and advances stream position by count of charaters
    ''' </summary>
    ''' <param name="Value">An array of characters to write to underlying stream</param>
    Sub WriteChars(ByVal Value As Char())
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteChars(Value)
    End Sub

    ''' <summary>
    ''' Writes a string containing binary values (bits) to underlying stream and advances stream position by ((count / 8) + (8 Mod count)) rounded up to nearest multiple of 8
    ''' </summary>
    ''' <param name="Value">A binary (bit) string</param>
    Sub WriteBinary(ByVal Value As String)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteBinary(Value, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a string containing binary values (bits) to underlying stream and advances stream position by ((count / 8) + (8 Mod count)) rounded up to nearest multiple of 8
    ''' </summary>
    ''' <param name="Value">A binary (bit) string</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteBinary(ByVal Value As String, ByVal EndianType As Endian)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteBinary(Value, EndianType)
    End Sub

    ''' <summary>
    ''' Writes a string containing ASCII encoded charaters to underlying stream and advances stream position by count of characters
    ''' </summary>
    ''' <param name="Value">A string containing ASCII encoded characters</param>
    Sub WriteASCII(ByVal Value As String)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteASCII(Value, Value.Length)
    End Sub

    ''' <summary>
    ''' Writes a string containing ASCII encoded charaters to underlying stream for specified length and advances stream position by Length
    ''' </summary>
    ''' <param name="Value">A string containing ASCII encoded characters</param>
    ''' <param name="Length">Maximum number of characters to write</param>
    Sub WriteASCII(ByVal Value As String, ByVal Length As Integer)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteASCII(Value, Length)
    End Sub

    ''' <summary>
    ''' Writes a string containing Unicode encoded charaters to underlying stream in currently set endianness and advances stream position by (count of characters * 2)
    ''' </summary>
    ''' <param name="Value">A string containing Unicode encoded characters</param>
    Sub WriteUnicode(ByVal Value As String)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteUnicode(Value, Value.Length, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a string containing Unicode encoded charaters to underlying stream in currently set endianness and advances stream position by (Length * 2)
    ''' </summary>
    ''' <param name="Value">A string containing Unicode encoded characters</param>
    ''' <param name="Length">Maximum number of characters to write</param>
    Sub WriteUnicode(ByVal Value As String, ByVal Length As Integer)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteUnicode(Value, Length, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a string containing Unicode encoded charaters to underlying stream in specified endianness and advances stream position by (Length * 2)
    ''' </summary>
    ''' <param name="Value">A string containing Unicode encoded characters</param>
    ''' <param name="Length">Maximum number of characters to write</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteUnicode(ByVal Value As String, ByVal Length As Integer, ByVal EndianType As Endian)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteUnicode(Value, Length, EndianType)
    End Sub

    ''' <summary>
    ''' Writes a string containing Hexadecimal values to underlying stream in specified endianness and advances stream position by count of digits
    ''' </summary>
    ''' <param name="Value">A string containing Hexadecimal values</param>
    Sub WriteHex(ByVal Value As String)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteHex(Value, Value.Length, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a string containing Hexadecimal values to underlying stream in specified endianness and advances stream position by Length
    ''' </summary>
    ''' <param name="Value">A string containing Hexadecimal values</param>
    ''' <param name="Length">Maximum number of bytes to write</param>
    Sub WriteHex(ByVal Value As String, ByVal Length As Integer)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteHex(Value, Length, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a string containing Hexadecimal values to underlying stream in specified endianness and advances stream position by Length
    ''' </summary>
    ''' <param name="Value">A string containing Hexadecimal values</param>
    ''' <param name="Length">Maximum number of bytes to write</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteHex(ByVal Value As String, ByVal Length As Integer, ByVal EndianType As Endian)
        CheckPaused()
        Last = OpenStream.Position
        Writer.WriteHex(Value, Length, EndianType)
    End Sub

#End Region

    ''' <summary>
    ''' Gets or sets the current FileAccess flag of the stream (FileStream only)
    ''' </summary>
    ''' <value>FileAccess enum value to attempt to change the underlying stream to</value>
    ''' <returns>Returns current FileAccess enum value the underlying stream is set to</returns>
    Property FileAccess() As FileAccess
        Get
            CheckPaused()
            If Type <> StreamType.FileStream Then Throw New Exception("Current stream type is not FileStream")
            Return Access
        End Get
        Set(ByVal value As FileAccess)
            If value = Access Then Return
            CheckPaused()
            If Type <> StreamType.FileStream Then Throw New Exception("Current stream type is not FileStream")
            Dim x As Long = Position
            OpenStream.Flush()
            OpenStream.Close()
            Access = value
            OpenStream = New FileStream(OpenFile, FileMode.Open, Access, Share)
            Reader = New Reader(OpenStream, CurrentEndian)
            Writer = New Writer(OpenStream, CurrentEndian)
            Position = x
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the current FileShare flag of the stream (FileStream only)
    ''' </summary>
    ''' <value>FileShare enum value to attempt to change the underlying stream to</value>
    ''' <returns>Returns current FileShare enum value the underlying stream is set to</returns>
    Property FileShare() As FileShare
        Get
            CheckPaused()
            If Type <> StreamType.FileStream Then Throw New Exception("Current stream type is not FileStream")
            Return Share
        End Get
        Set(ByVal value As FileShare)
            If value = Share Then Return
            CheckPaused()
            If Type <> StreamType.FileStream Then Throw New Exception("Current stream type is not FileStream")
            Dim x As Long = Position
            OpenStream.Flush()
            OpenStream.Close()
            Share = value
            OpenStream = New FileStream(OpenFile, FileMode.Open, Access, Share)
            Reader = New Reader(OpenStream, CurrentEndian)
            Writer = New Writer(OpenStream, CurrentEndian)
            Position = x
        End Set
    End Property

    ''' <summary>
    ''' Swtiches the current endianness to type not currently in use
    ''' </summary>
    Sub SwapEndian()
        If CurrentEndian = Endian.Little Then
            CurrentEndian = Endian.Big
        Else
            CurrentEndian = Endian.Little
        End If
    End Sub

    ''' <summary>
    ''' Gets the opposite endianness not currently in use
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returns Endian enum value indicating the endianness not currently in use</returns>
    ReadOnly Property OppositeEndian() As Endian
        Get
            If CurrentEndian = Endian.Little Then Return Endian.Big
            Return Endian.Little
        End Get
    End Property

    Private Sub CheckPaused()
        If Not IsOpen Then Throw New Exception("Stream access is currently paused")
    End Sub

    ''' <summary>
    ''' Whether or not access to the underlying stream can be suspended
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returns true or false whether access to underlying stream can be paused</returns>
    ReadOnly Property Pausable() As Boolean
        Get
            Return CanPause
        End Get
    End Property

    ''' <summary>
    ''' Whether the instance is currently accessing the underlying stream
    ''' </summary>
    ''' <returns>True or false whether the instance is currently accessing the underlying stream</returns>
    ReadOnly Property IsOpen() As Boolean
        Get
            If Reader Is Nothing OrElse Writer Is Nothing OrElse OpenStream Is Nothing OrElse IsPaused Then Return False
            Return True
        End Get
    End Property

    ''' <summary>
    ''' Gets whether access to the underlying stream is suspended
    ''' </summary>
    ''' <returns>Returns whether access to the underlying stream is paused</returns>
    ReadOnly Property IsPaused() As Boolean
        Get
            Return StreamPaused
        End Get
    End Property

    ''' <summary>
    ''' True or false whether the Reader is currently performing a search operation on the underlying stream
    ''' </summary>>
    ''' <returns>Returns whether the Reader is currently performing a search</returns>
    ReadOnly Property IsSearching() As Boolean
        Get
            Return Reader.IsSearching
        End Get
    End Property

    ''' <summary>
    ''' True or false whether the Reader has finished the last search operation
    ''' </summary>
    ''' <returns>Returns whether the Reader has completed a search</returns>
    ReadOnly Property IsSearchComplete() As Boolean
        Get
            Return Reader.IsSearchComplete
        End Get
    End Property

    ''' <summary>
    ''' Returns the number of bytes a search operation has looped through since it's initial call
    ''' </summary>
    ''' <returns>Total number of bytes searched. -1 returned if no search is being performed</returns>
    ReadOnly Property BytesSearched() As Long
        Get
            Return Reader.BytesSearched
        End Get
    End Property

    ''' <summary>
    ''' Returns the number of bytes a search has left to loop through before it's finished its operation
    ''' </summary>
    ''' <returns>Total number of bytes left to search. -1 returned if no search is being performed</returns>
    ReadOnly Property BytesToSearch() As Long
        Get
            Return Reader.BytesToSearch
        End Get
    End Property

    ''' <summary>
    ''' Suspends access to current underlying stream (FileStream only)
    ''' </summary>
    Sub Pause()
        If Not CanPause Then Throw New Exception("Stream type cannot be paused or resumed. Can only pause or resume access to files")
        If StreamPaused Then Throw New Exception("Stream access is already paused")
        If IsSearching Then AbortSearch()
        Flush()
        Reader = Nothing
        Writer = Nothing
        OpenStream.Close() : OpenStream = Nothing
        StreamPaused = True
    End Sub

    ''' <summary>
    ''' Resumes access to underlying stream if previously paused
    ''' </summary>
    ''' <param name="Position">Offset to restore stream position to. Optional.</param>
    ''' <param name="EndianType">Byte order in which to read/write data. Optional.</param>
    ''' <param name="FileAccess">A System.IO.FileAccess contstant that determines how the file can be access by the object. Optional.</param>
    ''' <param name="FileShare">A System.IO.FileAccess contstant that determines how the file will be shared by processes. Optional.</param>
    Sub [Resume](Optional ByVal Position As Long = 0, Optional ByVal EndianType As Endian = Endian.Little, Optional ByVal FileAccess As FileAccess = FileAccess.ReadWrite, Optional ByVal FileShare As FileShare = FileShare.ReadWrite)
        If Not CanPause Then Throw New Exception("Stream type cannot be paused or resumed. Can only pause or resume access to files")
        If Not StreamPaused Then Throw New Exception("Stream access is not paused")
        If Not System.IO.File.Exists(OpenFile) Then Throw New FileNotFoundException(New String(OpenFile & " was not found or could not be accessed"))
        Access = FileAccess
        Share = FileShare
        OpenStream = New FileStream(OpenFile, FileMode.Open, FileAccess, FileShare)
        If Not OpenStream.CanRead Then Throw New InvalidDataException("Stream does not have read access")
        If Not OpenStream.CanWrite Then Throw New InvalidDataException("Stream does not have write access")
        Reader = New Reader(OpenStream)
        Writer = New Writer(OpenStream)
        Me.Position = Position
        CurrentEndian = EndianType
        StreamPaused = False
    End Sub

    ''' <summary>
    ''' Ensures all pending writes are written to underlying device and clears any buffered data
    ''' </summary>
    Sub Flush()
        OpenStream.Flush()
    End Sub

    ''' <summary>
    ''' Closes the current IO and optionally the underlying stream. Flushing will occur beforehand
    ''' </summary>
    ''' <param name="CloseUnderlying">Whether to close underlying stream</param>
    ''' <param name="Dispose">Whether to dispose of the class base after closing</param>
    Sub Close(Optional ByVal CloseUnderlying As Boolean = True, Optional ByVal Dispose As Boolean = False)
        If IsSearching Then AbortSearch()
        If IsOpen Then
            Flush()
            Reader = Nothing
            Writer = Nothing
            If CloseUnderlying Then
                OpenStream.Close()
                OpenStream = Nothing
            End If
        End If
        OpenFile = ""
        If Dispose Then Me.Dispose()
    End Sub

    ''' <summary>
    ''' Creates a shallow copy of the current class base
    ''' </summary>
    ''' <returns>Returns a shallow copy reference of the current class base</returns>
    Overloads Function Clone() As Object Implements System.ICloneable.Clone
        Return DirectCast(Me.MemberwiseClone, IO)
    End Function

    ''' <summary>
    ''' Disposes of the class base and forces immediate garbage collection
    ''' </summary>
    Overloads Sub Dispose() Implements System.IDisposable.Dispose
        If Not Reader Is Nothing Then Reader.Dispose()
        If Not Writer Is Nothing Then Writer.Dispose()
        MyBase.Finalize()
        GC.Collect(GC.GetGeneration(Me), GCCollectionMode.Forced)
    End Sub

    ''' <summary>
    ''' Determines whether the specified IO instance is equal to the current instance
    ''' </summary>
    ''' <param name="obj">An existing instance of the IO class to compare to the current instance</param>
    Overloads Function Equals(ByVal obj As IO) As Boolean Implements System.IEquatable(Of IO).Equals
        Return MyBase.Equals(obj)
    End Function

End Class

''' <summary>
''' A multi-endian binary reader capable of reading various data types
''' </summary>
Public Class Reader
    Implements IDisposable, ICloneable, IEquatable(Of Reader)
    Private OpenFile As String = "", OpenStream As Stream = Nothing
    Private Access As FileAccess, Share As FileShare, Type As StreamType
    Private Reader As BinaryReader = Nothing
    Private Searching As Boolean = False, SearchCompleted As Boolean = False, SearchCancellationPending As Boolean = False, TotalSearched, TotalLeftToSearch As Long
    Private Last As Long = 0, Endianness As Endian, CanPause As Boolean = True, StreamPaused As Boolean = False, StreamOwner As Boolean = False

    ''' <summary>
    ''' Creates a new instance of the Reader class
    ''' </summary>
    ''' <param name="File">Absolute path to the file to open or create</param>
    ''' <param name="EndianType">Initial byte order in which to read data. Optional.</param>
    ''' <param name="Position">Initial offset to set stream position to. Optional.</param>
    ''' <param name="FileAccess">A System.IO.FileAccess contstant that determines how the file can be access by the object. Optional.</param>
    ''' <param name="FileShare">A System.IO.FileAccess contstant that determines how the file will be shared by processes. Optional.</param>
    Sub New(ByVal File As String, Optional ByVal EndianType As Endian = Endian.Little, Optional ByVal Position As Long = 0, Optional ByVal FileAccess As FileAccess = FileAccess.Read, Optional ByVal FileShare As FileShare = FileShare.ReadWrite)
        MyBase.New()
        If Not System.IO.File.Exists(File) Then Throw New FileNotFoundException(New String(File & " was not found or could not be accessed"))
        OpenFile = File
        Access = FileAccess
        Share = FileShare.Delete
        Type = StreamType.FileStream
        OpenStream = New FileStream(File, FileMode.Open, FileAccess, FileShare)
        Reader = New BinaryReader(OpenStream)
        StreamOwner = True
        Me.Position = Position
        CurrentEndian = EndianType
    End Sub

    ''' <summary>
    ''' Creates a new instance of the Reader class
    ''' </summary>
    ''' <param name="Buffer">An existing memory buffer holding data to do IO operations to</param>
    ''' <param name="EndianType">Initial byte order in which to read/write data. Optional.</param>
    ''' <param name="Position">Initial offset to set stream position to. Optional.</param>
    Sub New(ByVal Buffer As Byte(), Optional ByVal EndianType As Endian = Endian.Little, Optional ByVal Position As Long = 0)
        MyBase.New()
        Type = StreamType.MemoryStream
        OpenStream = New MemoryStream(Buffer)
        Reader = New BinaryReader(OpenStream)
        StreamOwner = True
        Me.Position = Position
        CurrentEndian = EndianType
        CanPause = False
    End Sub

    ''' <summary>
    ''' Creates a new instance of the Reader class
    ''' </summary>
    ''' <param name="Stream">An existing stream to do IO operations to</param>
    ''' <param name="EndianType">Initial byte order in which to read/write data. Optional.</param>
    ''' <param name="Position">Initial offset to set stream position to. Optional.</param>
    Sub New(ByVal Stream As Stream, Optional ByVal EndianType As Endian = Endian.Little, Optional ByVal Position As Long = 0)
        MyBase.New()
        If Not Stream.CanRead Then Throw New InvalidDataException("Stream does not have read access")
        Type = StreamType.GenericStream
        OpenStream = Stream
        Reader = New BinaryReader(OpenStream)
        Me.Position = Position
        CurrentEndian = EndianType
        CanPause = False
    End Sub

    ''' <summary>
    ''' Returns the current working stream in use by the instance
    ''' </summary>
    ReadOnly Property CurrentStream() As Stream
        Get
            Return OpenStream
        End Get
    End Property

    ''' <summary>
    ''' Returns StreamType of the current stream
    ''' </summary>
    ''' <returns>Returns a StreamType enumerator value indicating which type of stream the underlying stream is</returns>
    ReadOnly Property StreamType() As StreamType
        Get
            Return Type
        End Get
    End Property

    ''' <summary>
    ''' True or false whether the underlying stream was initially created by the instance
    ''' </summary>
    ''' <returns>Returns a boolean value indicating if the underlying stream was created by the instance</returns>
    ReadOnly Property IsOwner() As Boolean
        Get
            Return StreamOwner
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the current position (offset) of the stream
    ''' </summary>
    ''' <value>Offset to set stream position to</value>
    ''' <returns>Current offset stream position is at</returns>
    Property Position() As Long
        Get
            CheckPaused()
            Return OpenStream.Position
        End Get
        Set(ByVal value As Long)
            CheckPaused()
            If Last <> OpenStream.Position Then Last = OpenStream.Position
            OpenStream.Position = value
        End Set
    End Property

    ''' <summary>
    ''' Sets stream postion to the last position before last operation or position set
    ''' </summary>
    Sub JumpBack()
        Position = LastPosition
    End Sub

    ''' <summary>
    ''' Moves stream position to specified offset
    ''' </summary>
    ''' <param name="Offset">Offset to set stream position to</param>
    Sub Seek(ByVal Offset As Long)
        Seek(Offset, SeekOrigin.Begin)
    End Sub

    ''' <summary>
    ''' Moves stream position to specified offset
    ''' </summary>
    ''' <param name="Offset"></param>
    ''' <param name="SeekOrigin">A value of type System.IO.SeekOrigin indicating the reference point used to obtain the new position</param>
    Sub Seek(ByVal Offset As Long, ByVal SeekOrigin As SeekOrigin)
        CheckPaused()
        If Last <> OpenStream.Position Then Last = OpenStream.Position
        Reader.BaseStream.Seek(Offset, SeekOrigin)
    End Sub

    ''' <summary>
    ''' Gets the length in bytes of the current stream
    ''' </summary>
    ''' <returns>Current length in bytes of the underlying stream</returns>
    ReadOnly Property Length() As Long
        Get
            Return OpenStream.Length
        End Get
    End Property

    ''' <summary>
    ''' Gets the length in bytes from current position to end of stream
    ''' </summary>
    ''' <returns>Length in bytes from current position to length of stream (Length - Position)</returns>
    ReadOnly Property LengthToEnd() As Long
        Get
            Return CLng(Length - Position)
        End Get
    End Property

    ''' <summary>
    ''' Get or set the endianness currently in use by the Reader
    ''' </summary>
    Property CurrentEndian() As Endian
        Get
            Return Endianness
        End Get
        Set(ByVal value As Endian)
            If Not [Enum].IsDefined(GetType(Endian), value) Then Return
            Endianness = value
        End Set
    End Property

    ''' <summary>
    ''' Returns the position before last operation or position set
    ''' </summary>
    ReadOnly Property LastPosition() As Long
        Get
            Return Last
        End Get
    End Property

    ''' <summary>
    ''' Returns all bytes in the current stream
    ''' </summary>
    ''' <returns>Buffer containing all bytes in the underlying stream</returns>
    Function GetBuffer() As Byte()
        Dim Buffer() As Byte = New Byte() {}
        GetBuffer(Buffer)
        Return Buffer
    End Function

    ''' <summary>
    ''' Copies all bytes in underlying stream to refrenced byte array
    ''' </summary>
    ''' <param name="OutBuffer">Buffer to store underlying stream bytes</param>
    Sub GetBuffer(<[In](), [Out]()> ByRef OutBuffer As Byte())
        CheckPaused()
        Functions.StreamToBuffer(OpenStream, OutBuffer)
    End Sub

    ''' <summary>
    ''' Reads a single UTF8 encoded character from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a single UTF8 encoded character</returns>
    Function PeekChar() As Char
        Dim Buffer As Char = Me.ReadChar() : Me.Position = LastPosition
        Return Buffer
    End Function

    ''' <summary>
    ''' Reads specified count of UTF8 encoded characters from stream without advancing current stream position
    ''' </summary>
    ''' <param name="Length">Number of characters to read from stream</param>
    ''' <returns>Returns specified length of UTF8 characters</returns>
    Function PeekChars(ByVal Length As Integer) As Char()
        Dim Buffer As Char() = Me.ReadChars(Length) : Me.Position = LastPosition
        Return Buffer
    End Function

    ''' <summary>
    ''' Reads a boolean value from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a Boolean type value</returns>
    Function PeekBoolean() As Boolean
        Dim Buffer As Boolean = Me.ReadBoolean() : Me.Position = LastPosition
        Return Buffer
    End Function

    ''' <summary>
    ''' Reads a single unsigned byte from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns an unsigned byte</returns>
    Function PeekByte() As Byte
        Return PeekBytes(1)(0)
    End Function

    ''' <summary>
    ''' Reads a single signed byte from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a signed byte</returns>
    Function PeekSByte() As SByte
        Return CSByte(PeekBytes(1)(0))
    End Function

    ''' <summary>
    ''' Reads a 8bit signed integer from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a signed 8bit integer value</returns>
    Function PeekInt8() As SByte
        Return PeekSByte()
    End Function

    ''' <summary>
    ''' Reads a 8bit unsigned integer from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a 8bit unsigned integer value</returns>
    Function PeekUInt8() As Byte
        Return PeekByte()
    End Function

    ''' <summary>
    ''' Reads a 16bit signed integer from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a 16bit signed integer value in currently set endianness</returns>
    Function PeekInt16() As Int16
        Return PeekInt16(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 16bit unsigned integer from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a 16bit unsigned integer value in currently set endianness</returns>
    Function PeekUInt16() As UInt16
        Return PeekUInt16(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 16bit signed integer from stream without advancing current stream position
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a 16bit signed integer value in specifed endianness</returns>
    Function PeekInt16(ByVal EndianType As Endian) As Int16
        Return BitConverter.ToInt16(PeekBytes(2, EndianType), 0)
    End Function

    ''' <summary>
    ''' Reads a 16bit unsigned integer from stream without advancing current stream position
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a 16bit unsigned integer value in specifed endianness</returns>
    Function PeekUInt16(ByVal EndianType As Endian) As UInt16
        Return BitConverter.ToUInt16(PeekBytes(2, EndianType), 0)
    End Function

    ''' <summary>
    ''' Reads a 24bit signed integer from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a 24bit signed integer value in currently set endianness</returns>
    Function PeekInt24() As Int32
        Return PeekInt24(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 24bit unsigned integer from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a 24bit unsigned integer value in currently set endianness</returns>
    Function PeekUInt24() As UInt32
        Return PeekUInt24(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 24bit signed integer from stream without advancing current stream position
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a 24bit signed integer value in specifed endianness</returns>
    Function PeekInt24(ByVal EndianType As Endian) As Int32
        Return BytesToInt24(PeekBytes(3, EndianType))
    End Function

    ''' <summary>
    ''' Reads a 24bit unsigned integer from stream without advancing current stream position
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a 24bit unsigned integer value in specifed endianness</returns>
    Function PeekUInt24(ByVal EndianType As Endian) As UInt32
        Return CUInt(BytesToUInt24(PeekBytes(3, EndianType)))
    End Function

    ''' <summary>
    ''' Reads a 32bit signed integer from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a 32bit signed integer value in currently set endianness</returns>
    Function PeekInt32() As Int32
        Return PeekInt32(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 32bit unsigned integer from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a 32bit unsigned integer value in currently set endianness</returns>
    Function PeekUInt32() As UInt32
        Return PeekUInt32(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 32bit signed integer from stream without advancing current stream position
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a 32bit signed integer value in specifed endianness</returns>
    Function PeekInt32(ByVal EndianType As Endian) As Int32
        Return BitConverter.ToInt32(PeekBytes(4, EndianType), 0)
    End Function

    ''' <summary>
    ''' Reads a 32bit unsigned integer from stream without advancing current stream position
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a 32bit unsigned integer value in specifed endianness</returns>
    Function PeekUInt32(ByVal EndianType As Endian) As UInt32
        Return BitConverter.ToUInt32(PeekBytes(4, EndianType), 0)
    End Function

    ''' <summary>
    ''' Reads a 40bit signed integer from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a 40bit signed integer value in currently set endianness</returns>
    Function PeekInt40() As Long
        Return PeekInt40(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 40bit unsigned integer from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a 40bit unsigned integer value in currently set endianness</returns>
    Function PeekUInt40() As ULong
        Return PeekUInt40(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 40bit signed integer from stream without advancing current stream position
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a 40bit signed integer value in specifed endianness</returns>
    Function PeekInt40(ByVal EndianType As Endian) As Long
        Return BytesToInt40(PeekBytes(5, EndianType))
    End Function

    ''' <summary>
    ''' Reads a 40bit unsigned integer from stream without advancing current stream position
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a 40bit unsigned integer value in specifed endianness</returns>
    Function PeekUInt40(ByVal EndianType As Endian) As ULong
        Return CULng(BytesToUInt40(PeekBytes(5, EndianType)))
    End Function

    ''' <summary>
    ''' Reads a 48bit signed integer from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a 48bit signed integer value in currently set endianness</returns>
    Function PeekInt48() As Long
        Return PeekInt48(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 48bit unsigned integer from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a 48bit unsigned integer value in currently set endianness</returns>
    Function PeekUInt48() As ULong
        Return PeekUInt48(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 48bit signed integer from stream without advancing current stream position
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a 48bit signed integer value in specifed endianness</returns>
    Function PeekInt48(ByVal EndianType As Endian) As Long
        Return BytesToInt48(PeekBytes(6, EndianType))
    End Function

    ''' <summary>
    ''' Reads a 48bit unsigned integer from stream without advancing current stream position
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a 48bit unsigned integer value in specifed endianness</returns>
    Function PeekUInt48(ByVal EndianType As Endian) As ULong
        Return CULng(BytesToInt48(PeekBytes(6, EndianType)))
    End Function

    ''' <summary>
    ''' Reads a 56bit signed integer from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a 56bit signed integer value in currently set endianness</returns>
    Function PeekInt56() As Long
        Return PeekInt56(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 56bit unsigned integer from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a56bit unsigned integer value in currently set endianness</returns>
    Function PeekUInt56() As ULong
        Return PeekUInt56(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 56bit signed integer from stream without advancing current stream position
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a 56bit signed integer value in specifed endianness</returns>
    Function PeekInt56(ByVal EndianType As Endian) As Long
        Return BytesToInt56(PeekBytes(7, EndianType))
    End Function

    ''' <summary>
    ''' Reads a 56bit unsigned integer from stream without advancing current stream position
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a 56bit unsigned integer value in specifed endianness</returns>
    Function PeekUInt56(ByVal EndianType As Endian) As ULong
        Return CULng(BytesToInt56(PeekBytes(7, EndianType)))
    End Function

    ''' <summary>
    ''' Reads a 64bit signed integer from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a 64bit signed integer value in currently set endianness</returns>
    Function PeekInt64() As Int64
        Return PeekInt64(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 64bit unsigned integer from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a 64bit unsigned integer value in currently set endianness</returns>
    Function PeekUInt64() As UInt64
        Return PeekUInt64(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 64bit signed integer from stream without advancing current stream position
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a 64bit signed integer value in specifed endianness</returns>
    Function PeekInt64(ByVal EndianType As Endian) As Int64
        Return BitConverter.ToInt64(PeekBytes(8, EndianType), 0)
    End Function

    ''' <summary>
    ''' Reads a 64bit unsigned integer from stream without advancing current stream position
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a 64bit unsigned integer value in specifed endianness</returns>
    Function PeekUInt64(ByVal EndianType As Endian) As UInt64
        Return BitConverter.ToUInt64(PeekBytes(8, EndianType), 0)
    End Function

    ''' <summary>
    ''' Reads a 32bit float from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a 32bit float value in currently set endianness</returns>
    Function PeekSingle() As Single
        Return PeekSingle(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 64bit double from stream without advancing current stream position
    ''' </summary>
    ''' <returns>Returns a 64bit double value in currently set endianness</returns>
    Function PeekDouble() As Double
        Return PeekDouble(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 32bit float from stream without advancing current stream position
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a 32bit float value in specifed endianness</returns>
    Function PeekSingle(ByVal EndianType As Endian) As Single
        Return BitConverter.ToSingle(PeekBytes(4, EndianType), 0)
    End Function

    ''' <summary>
    ''' Reads a 64bit double from stream without advancing current stream position
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a 64bit double value in specifed endianness</returns>
    Function PeekDouble(ByVal EndianType As Endian) As Double
        Return BitConverter.ToDouble(PeekBytes(EndianType), 0)
    End Function

    ''' <summary>
    ''' Reads a specified count of unsigned bytes from stream without advancing current stream position
    ''' </summary>
    ''' <param name="Length">Number of bytes to read from stream</param>
    ''' <returns>Returns specified length of unsigned bytes in currently set endianess</returns>
    Function PeekBytes(ByVal Length As Integer) As Byte()
        Return PeekBytes(Length, CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a specified count of unsigned bytes from stream without advancing current stream position
    ''' </summary>
    ''' <param name="Length">Number of bytes to read from stream</param>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns specified length of unsigned bytes in specified endianess</returns>
    Function PeekBytes(ByVal Length As Integer, ByVal EndianType As Endian) As Byte()
        Dim Buffer As Byte() = Me.ReadBytes(Length, EndianType) : Me.Position = Me.LastPosition
        Return Buffer
    End Function

    ''' <summary>
    ''' Reads a specified count of (Length * 8) bits from stream without advancing current stream position
    ''' </summary>
    ''' <param name="Length">Number of bytes to read from stream and convert to binary string</param>
    ''' <returns>Returns a (Length * 8) long string containing bits in currently set endianness</returns>
    Function PeekBinary(ByVal Length As Integer) As String
        Return PeekBinary(Length, CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a specified count of (Length * 8) bits from stream without advancing current stream position
    ''' </summary>
    ''' <param name="Length">Number of bytes to read from stream and convert to binary string</param>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a (Length * 8) long string containing bits in specified endianness</returns>
    Function PeekBinary(ByVal Length As Integer, ByVal EndianType As Endian) As String
        Dim Buffer As Byte() = Me.ReadBytes(Length, EndianType) : Me.Position = Me.LastPosition
        Return BytesToBitString(Buffer)
    End Function

    ''' <summary>
    ''' Reads a specified count of ASCII encoded charaters from stream without advancing current stream position
    ''' </summary>
    ''' <param name="Length">Number of charaters to read from the stream</param>
    ''' <returns>Returns a string containing specified length of ASCII encoded characters</returns>
    Function PeekASCII(ByVal Length As Integer) As String
        Dim Buffer As String = ReadASCII(Length) : Me.Position = Me.LastPosition
        Return Buffer
    End Function

    ''' <summary>
    ''' Reads a specified count of Unicode encoded charaters from stream without advancing current stream position
    ''' </summary>
    ''' <param name="Length">Number of charaters to read from the stream</param>
    ''' <returns>Returns a string containing specified length of Unicode encoded characters in currently set endianness</returns>
    Function PeekUnicode(ByVal Length As Integer) As String
        Return PeekUnicode(Length, CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a specified count of Unicode encoded charaters from stream without advancing current stream position
    ''' </summary>
    ''' <param name="Length">Number of charaters to read from the stream</param>
    ''' <param name="EndianType">Endianness in which to read characters</param>
    ''' <returns>Returns a string containing specified length of Unicode encoded characters in specified endianness</returns>
    Function PeekUnicode(ByVal Length As Integer, ByVal EndianType As Endian) As String
        Dim Buffer As String = ReadUnicode(Length, EndianType) : Me.Position = Me.LastPosition
        Return Buffer
    End Function

    ''' <summary>
    ''' Reads a specified count of Hexadecimal values from stream without advancing current stream position
    ''' </summary>
    ''' <param name="Length">Number of bytes to read from the stream</param>
    ''' <returns>Returns a string containing specified length of Hexadecimal values in currently set endianness</returns>
    Function PeekHex(ByVal Length As Integer) As String
        Return PeekHex(Length, CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a specified count of Hexadecimal values from stream without advancing current stream position
    ''' </summary>
    ''' <param name="Length">Number of bytes to read from the stream</param>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a string containing specified length of Hexadecimal values in specified endianness</returns>
    Function PeekHex(ByVal Length As Integer, ByVal EndianType As Endian) As String
        Dim Buffer As String = ReadHex(Length, EndianType) : Me.Position = Me.LastPosition
        Return Buffer
    End Function

    ''' <summary>
    ''' Reads a boolean value from stream and advances stream position by 1
    ''' </summary>
    ''' <returns>Returns a Boolean type value</returns>
    Function ReadBoolean() As Boolean
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadBoolean
    End Function

    ''' <summary>
    ''' Reads a single unsigned byte from stream and advances stream position by 1
    ''' </summary>
    ''' <returns>Returns an unsigned byte</returns>
    Function ReadByte() As Byte
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadByte
    End Function

    ''' <summary>
    ''' Reads a single signed byte from stream and advances stream position by 1
    ''' </summary>
    ''' <returns>Returns a signed byte</returns>
    Function ReadSByte() As SByte
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadSByte
    End Function

    ''' <summary>
    ''' Reads a count of unsigned bytes from stream and advances stream position by that count
    ''' </summary>
    ''' <param name="Length">Number of bytes to read from stream</param>
    ''' <returns>Returns a buffer of unsigned bytes</returns>
    Function ReadBytes(ByVal Length As Integer) As Byte()
        Return ReadBytes(Length, CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a count of unsigned bytes from stream and advances stream position by that count
    ''' </summary>
    ''' <param name="Length">Number of bytes to read from stream</param>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a buffer of unsigned bytes</returns>
    Function ReadBytes(ByVal Length As Integer, ByVal EndianType As Endian) As Byte()
        CheckPaused()
        Last = OpenStream.Position
        Dim Buffer As Byte() = Reader.ReadBytes(Length)
        If EndianType = Endian.Big Then SwapSex(Buffer)
        Return Buffer
    End Function

    ''' <summary>
    ''' Reads all unsigned bytes from stream and moves stream position to end of stream
    ''' </summary>
    ''' <returns>Returns a buffer of unsigned bytes containing all data in stream</returns>
    Function ReadAll() As Byte()
        Return ReadAll(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads all unsigned bytes from stream and moves stream position to end of stream
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a buffer of unsigned bytes containing all data in stream</returns>
    Function ReadAll(ByVal EndianType As Endian) As Byte()
        Position = 0
        Return ReadBytes(Length, EndianType)
    End Function

    ''' <summary>
    ''' Reads all unsigned bytes from current stream position to end of stream and moves stream position to end of stream
    ''' </summary>
    ''' <returns>Returns a buffer of unsigned bytes containing all data from current position to end of stream</returns>
    Function ReadToEnd() As Byte()
        Return ReadToEnd(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads all unsigned bytes from current stream position to end of stream and moves stream position to end of stream
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read data</param>
    Function ReadToEnd(ByVal EndianType As Endian) As Byte()
        Return ReadBytes((Length - Position), EndianType)
    End Function

    ''' <summary>
    ''' Reads a single signed byte from stream and advances stream position by 1
    ''' </summary>
    ''' <returns>Returns a signed byte</returns>
    Function ReadInt8() As SByte
        Return CSByte(ReadSByte())
    End Function

    ''' <summary>
    ''' Reads a single unsigned byte from stream and advances stream position by 1
    ''' </summary>
    ''' <returns>Returns an unsigned byte</returns>
    Function ReadUInt8() As Byte
        Return CByte(ReadByte())
    End Function

    ''' <summary>
    ''' Reads a 16bit signed integer from stream and advances stream position by 2
    ''' </summary>
    ''' <returns>Returns a 16bit signed integer value in currently set endianness</returns>
    Function ReadInt16() As Int16
        Return ReadInt16(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 16bit signed integer from stream and advances stream position by 2
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a 16bit signed integer value in specifed endianness</returns>
    Function ReadInt16(ByVal EndianType As Endian) As Int16
        Return BitConverter.ToInt16(ReadBytes(2, EndianType), 0)
    End Function

    ''' <summary>
    ''' Reads a 16bit unsigned integer from stream and advances stream position by 2
    ''' </summary>
    ''' <returns>Returns a 16bit unsigned integer value in currently set endianness</returns>
    Function ReadUInt16() As UInt16
        Return ReadUInt16(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 16bit unsigned integer from stream and advances stream position by 2
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a 16bit unsigned integer value in specifed endianness</returns>
    Function ReadUInt16(ByVal EndianType As Endian) As UInt16
        Return BitConverter.ToUInt16(ReadBytes(2, EndianType), 0)
    End Function

    ''' <summary>
    ''' Reads a 24bit signed integer from stream and advances stream position by 3
    ''' </summary>
    ''' <returns>Returns a 24bit signed integer value in currently set endianness</returns>
    Function ReadInt24() As Integer
        Return ReadInt24(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 24bit signed integer from stream and advances stream position by 3
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a 24bit signed integer value in specifed endianness</returns>
    Function ReadInt24(ByVal EndianType As Endian) As Integer
        Return CInt(BytesToInt24(ReadBytes(3, Endian.Little), IIf(EndianType = Endian.Big, True, False)))
    End Function

    ''' <summary>
    ''' Reads a 24bit unsigned integer from stream and advances stream position by 3
    ''' </summary>
    ''' <returns>Returns a 24bit unsigned integer value in currently set endianness</returns>
    Function ReadUInt24() As UInteger
        Return ReadUInt24(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 24bit unsigned integer from stream and advances stream position by 3
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a 24bit unsigned integer value in specifed endianness</returns>
    Function ReadUInt24(ByVal EndianType As Endian) As UInteger
        Return CUInt(BytesToUInt24(ReadBytes(3, Endian.Little), IIf(EndianType = Endian.Big, True, False)))
    End Function

    ''' <summary>
    ''' Reads a 32bit signed integer from stream and advances stream position by 4
    ''' </summary>
    ''' <returns>Returns a 32bit signed integer value in currently set endianness</returns>
    Function ReadInt32() As Int32
        Return ReadInt32(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 32bit signed integer from stream and advances stream position by 4
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a 32bit signed integer value in specifed endianness</returns>
    Function ReadInt32(ByVal EndianType As Endian) As Int32
        Return BitConverter.ToInt32(ReadBytes(4, EndianType), 0)
    End Function

    ''' <summary>
    ''' Reads a 32bit unsigned integer from stream and advances stream position by 4
    ''' </summary>
    ''' <returns>Returns a 32bit unsigned integer value in currently set endianness</returns>
    Function ReadUInt32() As UInt32
        Return ReadUInt32(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 32bit unsigned integer from stream and advances stream position by 4
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a 32bit unsigned integer value in specifed endianness</returns>
    Function ReadUInt32(ByVal EndianType As Endian) As UInt32
        Return BitConverter.ToUInt32(ReadBytes(4, EndianType), 0)
    End Function

    ''' <summary>
    ''' Reads a 40bit signed integer from stream and advances stream position by 5
    ''' </summary>
    ''' <returns>Returns a 40bit signed integer value in currently set endianness</returns>
    Function ReadInt40() As Long
        Return ReadInt40(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 40bit signed integer from stream and advances stream position by 5
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a 40bit signed integer value in specifed endianness</returns>
    Function ReadInt40(ByVal EndianType As Endian) As Long
        Return CLng(BytesToInt40(ReadBytes(5, EndianType)))
    End Function

    ''' <summary>
    ''' Reads a 40bit unsigned integer from stream and advances stream position by 5
    ''' </summary>
    ''' <returns>Returns a 40bit unsigned integer value in currently set endianness</returns>
    Function ReadUInt40() As ULong
        Return ReadUInt40(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 40bit unsigned integer from stream and advances stream position by 5
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a 40bit unsigned integer value in specifed endianness</returns>
    Function ReadUInt40(ByVal EndianType As Endian) As ULong
        Return CULng(BytesToUInt40(ReadBytes(5, EndianType)))
    End Function

    ''' <summary>
    ''' Reads a 48bit signed integer from stream and advances stream position by 6
    ''' </summary>
    ''' <returns>Returns a 48bit signed integer value in currently set endianness</returns>
    Function ReadInt48() As Long
        Return ReadInt48(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 48bit signed integer from stream and advances stream position by 6
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a 48bit signed integer value in specifed endianness</returns>
    Function ReadInt48(ByVal EndianType As Endian) As Long
        Return CLng(BytesToInt48(ReadBytes(6, EndianType)))
    End Function

    ''' <summary>
    ''' Reads a 48bit signed uninteger from stream and advances stream position by 6
    ''' </summary>
    ''' <returns>Returns a 48bit unsigned integer value in currently set endianness</returns>
    Function ReadUInt48() As ULong
        Return ReadUInt48(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 48bit unsigned integer from stream and advances stream position by 6
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a 48bit unsigned integer value in specifed endianness</returns>
    Function ReadUInt48(ByVal EndianType As Endian) As ULong
        Return CULng(BytesToUInt48(ReadBytes(6, EndianType)))
    End Function

    ''' <summary>
    ''' Reads a 56bit signed integer from stream and advances stream position by 7
    ''' </summary>
    ''' <returns>Returns a 56bit signed integer value in currently set endianness</returns>
    Function ReadInt56() As Long
        Return ReadInt56(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 56bit signed integer from stream and advances stream position by 7
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a 56bit signed integer value in specifed endianness</returns>
    Function ReadInt56(ByVal EndianType As Endian) As Long
        Return CLng(BytesToInt56(ReadBytes(7, EndianType)))
    End Function

    ''' <summary>
    ''' Reads a 56bit unsigned integer from stream and advances stream position by 7
    ''' </summary>
    ''' <returns>Returns a 56bit unsigned integer value in currently set endianness</returns>
    Function ReadUInt56() As ULong
        Return ReadUInt56(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 56bit unsigned integer from stream and advances stream position by 7
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a 56bit unsigned integer value in specifed endianness</returns>
    Function ReadUInt56(ByVal EndianType As Endian) As ULong
        Return CULng(BytesToUInt56(ReadBytes(7, EndianType)))
    End Function

    ''' <summary>
    ''' Reads a 64bit signed integer from stream and advances stream position by 8
    ''' </summary>
    ''' <returns>Returns a 64bit signed integer value in currently set endianness</returns>
    Function ReadInt64() As Int64
        Return ReadInt64(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 64bit signed integer from stream and advances stream position by 8
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a 64bit signed integer value in specifed endianness</returns>
    Function ReadInt64(ByVal EndianType As Endian) As Int64
        Return BitConverter.ToInt64(ReadBytes(8, EndianType), 0)
    End Function

    ''' <summary>
    ''' Reads a 64bit unsigned integer from stream and advances stream position by 8
    ''' </summary>
    ''' <returns>Returns a 64bit unsigned integer value in currently set endianness</returns>
    Function ReadUInt64() As UInt64
        Return ReadUInt64(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 64bit unsigned integer from stream and advances stream position by 8
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a 64bit unsigned integer value in specifed endianness</returns>
    Function ReadUInt64(ByVal EndianType As Endian) As UInt64
        Return BitConverter.ToUInt64(ReadBytes(8, EndianType), 0)
    End Function

    ''' <summary>
    ''' Reads a 32bit float from stream and advances stream position by 4
    ''' </summary>
    ''' <returns>Returns a 32bit float value in currently set endianness</returns>
    Function ReadSingle() As Single
        Return ReadSingle(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 32bit float from stream and advances stream position by 4
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a 32bit float value in specifed endianness</returns>
    Function ReadSingle(ByVal EndianType As Endian) As Single
        Return BitConverter.ToSingle(ReadBytes(4, EndianType), 0)
    End Function

    ''' <summary>
    ''' Reads a 64bit double from stream and advances stream position by 8
    ''' </summary>
    ''' <returns>Returns a 64bit double value in currently set endianness</returns>
    Function ReadDouble() As Double
        Return ReadDouble(CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a 64bit double from stream and advances stream position by 8
    ''' </summary>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a 64bit double value in specifed endianness</returns>
    Function ReadDouble(ByVal EndianType As Endian) As Double
        Return BitConverter.ToDouble(ReadBytes(8, EndianType), 0)
    End Function

    ''' <summary>
    ''' Reads a single UTF8 encoded character from stream and advances stream position by 1
    ''' </summary>
    ''' <returns>Returns a single UTF8 character</returns>
    Function ReadChar() As Char
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadChar
    End Function

    ''' <summary>
    ''' Reads specified count of UTF8 encoded characters from stream and advances stream position by count
    ''' </summary>
    ''' <param name="Length">Number of characters to read from stream</param>
    ''' <returns>Returns specified length of UTF8 characters</returns>
    Function ReadChars(ByVal Length As Integer) As Char()
        CheckPaused()
        Last = OpenStream.Position
        Return Reader.ReadChars(Length)
    End Function

    ''' <summary>
    ''' Reads a specified count of (Length * 8) bits from stream and advances stream position by count (Length * 8)
    ''' </summary>
    ''' <param name="Length">Number of bytes to read from stream and convert to binary string</param>
    ''' <returns>Returns a (Length * 8) long string containing bits in currently set endianness</returns>
    Function ReadBinary(ByVal Length As Integer) As String
        Return ReadBinary(Length, CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a specified count of (Length * 8) bits from stream and advances stream position by count (Length * 8)
    ''' </summary>
    ''' <param name="Length">Number of bytes to read from stream and convert to binary string</param>
    ''' <param name="EndianType">Endianness in which to read value</param>
    ''' <returns>Returns a (Length * 8) long string containing bits in currently set endianness</returns>
    Function ReadBinary(ByVal Length As Integer, ByVal EndianType As Endian) As String
        Dim Buffer As Byte() = Me.ReadBytes(Length, CurrentEndian)
        Return BytesToBitString(Buffer)
    End Function

    ''' <summary>
    ''' Reads a specified count of ASCII encoded charaters from stream and advances stream position by count
    ''' </summary>
    ''' <param name="Length">Number of charaters to read from the stream</param>
    ''' <returns>Returns a string containing specified length of ASCII encoded characters</returns>
    Function ReadASCII(ByVal Length As Integer) As String
        Return ASCII.GetString(Me.ReadBytes(Length, Endian.Little))
    End Function

    ''' <summary>
    ''' Reads a specified count of Unicode encoded charaters from stream and advances stream position by count
    ''' </summary>
    ''' <param name="Length">Number of charaters to read from the stream</param>
    ''' <returns>Returns a string containing specified length of Unicode encoded characters in currently set endianness</returns>
    Function ReadUnicode(ByVal Length As Integer) As String
        Return ReadUnicode(Length, CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a specified count of Unicode encoded charaters from stream and advances stream position by count
    ''' </summary>
    ''' <param name="Length">Number of charaters to read from the stream</param>
    ''' <param name="EndianType">Endianness in which to read characters</param>
    ''' <returns>Returns a string containing specified length of Unicode encoded characters in specified endianness</returns>
    Function ReadUnicode(ByVal Length As Integer, ByVal EndianType As Endian) As String
        Dim Buffer As Byte() = Me.ReadBytes(Length, Endian.Little)
        If EndianType = Endian.Big Then Return BigEndianUnicode.GetString(Buffer)
        Return Unicode.GetString(Buffer)
    End Function

    ''' <summary>
    ''' Reads a specified count of Hexadecimal values from stream and advances stream position by count
    ''' </summary>
    ''' <param name="Length">Number of bytes to read from the stream</param>
    ''' <returns>Returns a string containing specified length of Hexadecimal values in currently set endianness</returns>
    Function ReadHex(ByVal Length As Integer) As String
        Return ReadHex(Length, CurrentEndian)
    End Function

    ''' <summary>
    ''' Reads a specified count of Hexadecimal values from stream and advances stream position by count
    ''' </summary>
    ''' <param name="Length">Number of bytes to read from the stream</param>
    ''' <param name="EndianType">Endianness in which to read data</param>
    ''' <returns>Returns a string containing specified length of Hexadecimal values in specified endianness</returns>
    Function ReadHex(ByVal Length As Integer, ByVal EndianType As Endian) As String
        Return ObjectToHex(Me.ReadBytes(Length, EndianType))
    End Function

    ''' <summary>
    ''' Searches underlying stream for instances of specified criteria
    ''' </summary>
    ''' <param name="Criteria">A sequence of ASCII, Unicode, or Hexadecimal characters to look for during the search</param>
    ''' <param name="StringType">The encoding scheme to use with the criteria</param>
    ''' <param name="ComparisonType">The StringComparison enumeration that represents how the criteria will be compared against data in the underlying stream during the search operation. Optional</param>
    ''' <param name="Position">The offset in the underlying stream to begin the search from. If -1 then current stream position is used. Optional.</param>
    ''' <param name="ReturnOnFirst">Whether to return results after a single instance of the criteria is found. If false, search will continue until EndOffset value is reached. Optional.</param>
    ''' <param name="EndPosition">The offset in the underlying stream to stop the search at. If -1 then end of stream is used. Optional.</param>
    ''' <param name="ThrowErrorIfNotFound">If true an exception will be thrown if no instances of the criteria are found within search parameters, else Nothing (null) will be returned. Optional</param>
    ''' <returns>Returns an array of 64bit signed integers containing all offsets in the underlying stream the criteria was found. Returned offsets represent the position where criteria starts
    ''' If nothing is found then Nothing (null) is returned unless otherwise specified</returns>
    Function SearchString(ByRef Criteria As String, ByVal StringType As StringType, Optional ByVal ComparisonType As StringComparison = 4, Optional ByVal Position As Long = -1, Optional ByVal ReturnOnFirst As Boolean = True, Optional ByVal EndPosition As Long = -1, Optional ByVal ThrowErrorIfNotFound As Boolean = False) As Long()
        Dim Results As Long() = Nothing, Encoder As System.Text.Encoding = Nothing
        If StringType = 0 Then
            If Not IsValidASCII(Criteria) Then Throw New Exception("Criteria contains invalid ASCII characters")
            Encoder = System.Text.Encoding.ASCII
            Results = SearchString(Criteria, Encoder, ComparisonType, Position, ReturnOnFirst, EndPosition, ThrowErrorIfNotFound)
        ElseIf StringType = 1 Then
            If Not IsValidUnicode(Criteria) Then Throw New Exception("Criteria contains invalid Unicode characters")
            Encoder = System.Text.Encoding.Unicode
            Results = SearchString(Criteria, Encoder, ComparisonType, Position, ReturnOnFirst, EndPosition, ThrowErrorIfNotFound)
        ElseIf StringType = 2 Then
            If Not IsValidUnicode(Criteria) Then Throw New Exception("Criteria contains invalid Unicode characters")
            Encoder = System.Text.Encoding.BigEndianUnicode
            Results = SearchString(Criteria, Encoder, ComparisonType, Position, ReturnOnFirst, EndPosition, ThrowErrorIfNotFound)
        ElseIf StringType = 3 Then
            If Not IsValidHex(Criteria) Then Throw New Exception("Criteria contains invalid Hexadecimal characters")
            Dim Buffer As Byte() = HexToBytes(Criteria)
            Results = Search(Buffer, Position, ReturnOnFirst, EndPosition, ThrowErrorIfNotFound)
            Erase Buffer
        End If
        If Encoder IsNot Nothing Then Encoder = Nothing
        Return Results
    End Function

    ''' <summary>
    ''' Searches underlying stream for instances of specified criteria
    ''' </summary>
    ''' <param name="Criteria">A sequence of ASCII, Unicode, or Hexadecimal characters to look for during the search</param>
    ''' <param name="Encoder">The encoding class to use on data in the stream to make a comparison</param>
    ''' <param name="ComparisonType">How the critera should be compared against data in the stream</param>
    ''' <param name="Position">The offset in the underlying stream to begin the search from. If -1 then current stream position is used. Optional.</param>
    ''' <param name="ReturnOnFirst">Whether to return results after a single instance of the criteria is found. If false, search will continue until EndOffset value is reached. Optional.</param>
    ''' <param name="EndPosition">The offset in the underlying stream to stop the search at. If -1 then end of stream is used. Optional.</param>
    ''' <param name="ThrowErrorIfNotFound">If true an exception will be thrown if no instances of the criteria are found within search parameters, else Nothing (null) will be returned. Optional</param>
    ''' <returns>Returns an array of 64bit signed integers containing all offsets in the underlying stream the criteria was found. Returned offsets represent the position where criteria starts
    ''' If nothing is found then Nothing (null) is returned unless otherwise specified</returns>
    Private Function SearchString(ByRef Criteria As String, ByRef Encoder As System.Text.Encoding, Optional ByVal ComparisonType As StringComparison = 4, Optional ByVal Position As Long = -1, Optional ByVal ReturnOnFirst As Boolean = True, Optional ByVal EndPosition As Long = -1, Optional ByVal ThrowErrorIfNotFound As Boolean = False) As Long()
        If Equals(Encoder, Nothing) Then Throw New Exception("Encoder cannot be null")
        CheckPaused()
        If Position < -1 OrElse EndPosition < -1 Then Throw New Exception("Begin and end offsets cannot be negitive values")
        If Position = -1 Then Position = Me.Position
        If EndPosition = -1 Then EndPosition = Me.Length
        If Position > Me.Length OrElse EndPosition > Me.Length Then Throw New ArgumentOutOfRangeException("Begin offset or end offset is outside the bounds of the stream")
        Dim Results As New List(Of Long), BeginPosition As Long = Position, CharSize As Byte = CByte(IIf(Equals(Encoder, System.Text.Encoding.ASCII), 1, 2))
        RaiseEvent SearchingEvent()
        For i As Long = Position To Me.Length - 1
            If (i + (Criteria.Length * CharSize)) > Me.Length OrElse SearchCancellationPending Then Exit For
            If Not EndPosition = 0 AndAlso (i + (Criteria.Length * CharSize)) > EndPosition Then Exit For
            Me.Position = i
            Dim Buffer As Byte() = Me.ReadBytes((Criteria.Length * CharSize))
            RaiseEvent SearchAdvancedEvent(BeginPosition, Me.Position, EndPosition)
            Dim StringBuffer As String = Encoder.GetString(Buffer, 0, Buffer.Length)
            If CompareStrings(Criteria.Replace(vbNullChar, ""), StringBuffer.Replace(vbNullChar, ""), ComparisonType) Then Results.Add(i)
            Array.Reverse(Buffer)
            StringBuffer = Encoder.GetString(Buffer, 0, Buffer.Length)
            If CompareStrings(Criteria.Replace(vbNullChar, ""), StringBuffer.Replace(vbNullChar, ""), ComparisonType) Then Results.Add(i)
            Erase Buffer
            StringBuffer = Nothing
            If Results IsNot Nothing AndAlso Results.Count > 0 AndAlso ReturnOnFirst Then Exit For
        Next
        RaiseEvent SearchCompletedEvent()
        SearchCancellationPending = False
        If Results Is Nothing OrElse Results.Count = 0 Then
            If ThrowErrorIfNotFound Then Throw New Exception("Specified search criteria was not found")
            Return Nothing
        End If
        Return Results.ToArray
    End Function

    ''' <summary>
    ''' Searches underlying stream for instances of specified criteria
    ''' </summary>
    ''' <param name="Criteria">A signed or unsigned integer value to look for during the search</param>
    ''' <param name="IntType">The bit width of the criteria value</param>
    ''' <param name="Position">The offset in the underlying stream to begin the search from. If -1 then current stream position is used. Optional.</param>
    ''' <param name="ReturnOnFirst">Whether to return results after a single instance of the criteria is found. If false, search will continue until EndOffset value is reached. Optional.</param>
    ''' <param name="EndPosition">The offset in the underlying stream to stop the search at. If -1 then end of stream is used. Optional.</param>
    ''' <param name="ThrowErrorIfNotFound">If true an exception will be thrown if no instances of the criteria are found within search parameters, else Nothing (null) will be returned. Optional</param>
    ''' <returns>Returns an array of 64bit signed integers containing all offsets in the underlying stream the criteria was found. Returned offsets represent the position where criteria starts
    ''' If nothing is found then Nothing (null) is returned unless otherwise specified</returns>
    Function SearchNumeric(ByRef Criteria As Object, ByVal IntType As IntegerType, Optional ByVal Position As Long = -1, Optional ByVal ReturnOnFirst As Boolean = True, Optional ByVal EndPosition As Long = -1, Optional ByVal ThrowErrorIfNotFound As Boolean = False) As Long()
        Dim Buffer As Byte()
        Select Case IntType
            Case 0 Or 1
                Buffer = New Byte() {IIf(IntType = 0, CSByte(Criteria), CByte(Criteria))}
            Case 2 Or 3
                Buffer = IIf(IntType = 2, Int16ToBytes(CShort(Criteria)), UInt16ToBytes(CUShort(Criteria)))
            Case 4 Or 5
                Buffer = IIf(IntType = 4, Int24ToBytes(CInt(Criteria)), UInt24ToBytes(CUInt(Criteria)))
            Case 6 Or 7
                Buffer = IIf(IntType = 6, Int32ToBytes(CInt(Criteria)), UInt32ToBytes(CUInt(Criteria)))
            Case 8 Or 9
                Buffer = IIf(IntType = 8, Int40ToBytes(CLng(Criteria)), UInt40ToBytes(CULng(Criteria)))
            Case 10 Or 11
                Buffer = IIf(IntType = 10, Int48ToBytes(CLng(Criteria)), UInt48ToBytes(CULng(Criteria)))
            Case 12 Or 13
                Buffer = IIf(IntType = 12, Int56ToBytes(CLng(Criteria)), UInt56ToBytes(CULng(Criteria)))
            Case 14 Or 15
                Buffer = IIf(IntType = 14, Int64ToBytes(CLng(Criteria)), UInt64ToBytes(CULng(Criteria)))
            Case 16 Or 17
                Buffer = IIf(IntType = 16, SingleToBytes(CSng(Criteria)), DoubleToBytes(CDbl(Criteria)))
            Case Else
                Return Nothing
        End Select
        Dim Results As Long() = Search(Buffer, Position, ReturnOnFirst, EndPosition, ThrowErrorIfNotFound)
        Erase Buffer
        Return Results
    End Function

    ''' <summary>
    ''' Searches underlying stream for instances of specified criteria
    ''' </summary>
    ''' <param name="Criteria">A sequence of bytes to look for during the search</param>
    ''' <param name="Position">The offset in the underlying stream to begin the search from. If -1 then current stream position is used. Optional.</param>
    ''' <param name="ReturnOnFirst">Whether to return results after a single instance of the criteria is found. If false, search will continue until EndOffset value is reached. Optional.</param>
    ''' <param name="EndPosition">The offset in the underlying stream to stop the search at. If -1 then end of stream is used. Optional.</param>
    ''' <param name="ThrowErrorIfNotFound">If true an exception will be thrown if no instances of the criteria are found within search parameters, else Nothing (null) will be returned. Optional</param>
    ''' <returns>Returns an array of 64bit signed integers containing all offsets in the underlying stream the criteria was found. Returned offsets represent the position where criteria starts
    ''' If nothing is found then Nothing (null) is returned unless otherwise specified</returns>
    Function Search(ByRef Criteria As Byte(), Optional ByVal Position As Long = -1, Optional ByVal ReturnOnFirst As Boolean = True, Optional ByVal EndPosition As Long = -1, Optional ByVal ThrowErrorIfNotFound As Boolean = False) As Long()
        CheckPaused()
        If Position < -1 OrElse EndPosition < -1 Then Throw New Exception("Begin and end offsets cannot be negitive values")
        If Position = -1 Then Position = Me.Position
        If EndPosition = -1 Then EndPosition = Me.Length
        If Position > Me.Length OrElse EndPosition > Me.Length Then Throw New ArgumentOutOfRangeException("Begin offset or end offset is outside the bounds of the stream")
        Dim Results As New List(Of Long), BeginPosition As Long = Position
        RaiseEvent SearchingEvent()
        For i As Long = Position To Me.Length - 1
            If (i + Criteria.Length) > Me.Length OrElse SearchCancellationPending Then Exit For
            If Not EndPosition = 0 AndAlso (i + Criteria.Length) > EndPosition Then Exit For
            Me.Position = i
            Dim Buffer As Byte() = Me.ReadBytes(Criteria.Length)
            RaiseEvent SearchAdvancedEvent(BeginPosition, Me.Position, EndPosition)
            If CompareBytes(Criteria, Buffer) OrElse CompareBytes(Criteria, SwapSex(Buffer)) Then Results.Add(i)
            Erase Buffer
            If Results IsNot Nothing AndAlso Results.Count > 0 AndAlso ReturnOnFirst Then Exit For
        Next
        RaiseEvent SearchCompletedEvent()
        SearchCancellationPending = False
        If Results Is Nothing OrElse Results.Count = 0 Then
            If ThrowErrorIfNotFound Then Throw New Exception("Specified search criteria was not found")
            Return Nothing
        End If
        Return Results.ToArray
    End Function

    ''' <summary>
    ''' Asks any current search operations to abort as soon as possible and return any results found
    ''' </summary>
    Sub AbortSearch()
        If Not IsSearching Then Return
        SearchCancellationPending = True
    End Sub

    ''' <summary>
    ''' Gets or sets the current FileAccess flag of the stream (FileStream only)
    ''' </summary>
    ''' <value>FileAccess enum value to attempt to change the underlying stream to</value>
    ''' <returns>Returns current FileAccess enum value the underlying stream is set to</returns>
    Property FileAccess() As FileAccess
        Get
            CheckPaused()
            If Type <> StreamType.FileStream Then Throw New Exception("Current stream type is not FileStream")
            Return Access
        End Get
        Set(ByVal value As FileAccess)
            If value = Access Then Return
            CheckPaused()
            If Type <> StreamType.FileStream Then Throw New Exception("Current stream type is not FileStream")
            Dim x As Long = Position
            OpenStream.Flush()
            OpenStream.Close()
            Access = value
            OpenStream = New FileStream(OpenFile, FileMode.Open, Access, Share)
            Position = x
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the current FileShare flag of the stream (FileStream only)
    ''' </summary>
    ''' <value>FileShare enum value to attempt to change the underlying stream to</value>
    ''' <returns>Returns current FileShare enum value the underlying stream is set to</returns>
    Property FileShare() As FileShare
        Get
            CheckPaused()
            If Type <> StreamType.FileStream Then Throw New Exception("Current stream type is not FileStream")
            Return Share
        End Get
        Set(ByVal value As FileShare)
            If value = Share Then Return
            CheckPaused()
            If Type <> StreamType.FileStream Then Throw New Exception("Current stream type is not FileStream")
            Dim x As Long = Position
            OpenStream.Flush()
            OpenStream.Close()
            Share = value
            OpenStream = New FileStream(OpenFile, FileMode.Open, Access, Share)
            Position = x
        End Set
    End Property

    ''' <summary>
    ''' Swtiches the current endianness to type not currently in use
    ''' </summary>
    Sub SwapEndian()
        If CurrentEndian = Endian.Little Then
            CurrentEndian = Endian.Big
        Else
            CurrentEndian = Endian.Little
        End If
    End Sub

    ''' <summary>
    ''' Gets the opposite endianness not currently in use
    ''' </summary>
    ''' <returns>Returns Endian enum value indicating the endianness not currently in use</returns>
    ReadOnly Property OppositeEndian() As Endian
        Get
            If CurrentEndian = Endian.Little Then Return Endian.Big
            Return Endian.Little
        End Get
    End Property

    Private Sub CheckPaused()
        If Not IsOpen Then Throw New Exception("Stream access is currently paused")
    End Sub

    ''' <summary>
    ''' Whether or not access to the underlying stream can be suspended
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returns true or false whether access to underlying stream can be paused</returns>
    ReadOnly Property Pausable() As Boolean
        Get
            Return CanPause
        End Get
    End Property

    ''' <summary>
    ''' Whether the instance is currently accessing the underlying stream
    ''' </summary>
    ''' <returns>True or false whether the instance is currently accessing the underlying stream</returns>
    ReadOnly Property IsOpen() As Boolean
        Get
            If Reader Is Nothing OrElse OpenStream Is Nothing OrElse IsPaused Then Return False
            Return True
        End Get
    End Property

    ''' <summary>
    ''' Gets whether access to the underlying stream is suspended
    ''' </summary>
    ''' <returns>Returns whether access to the underlying stream is paused</returns>
    ReadOnly Property IsPaused() As Boolean
        Get
            Return StreamPaused
        End Get
    End Property

    ''' <summary>
    ''' True or false whether the Reader is currently performing a search operation on the underlying stream
    ''' </summary>>
    ''' <returns>Returns whether the Reader is currently performing a search</returns>
    ReadOnly Property IsSearching() As Boolean
        Get
            Return Searching
        End Get
    End Property

    ''' <summary>
    ''' True or false whether the Reader has finished the last search operation
    ''' </summary>
    ''' <returns>Returns whether the Reader has completed a search</returns>
    ReadOnly Property IsSearchComplete() As Boolean
        Get
            Return SearchCompleted
        End Get
    End Property

    ''' <summary>
    ''' Returns the number of bytes a search operation has looped through since it's initial call
    ''' </summary>
    ''' <returns>Total number of bytes searched. -1 returned if no search is being performed</returns>
    ReadOnly Property BytesSearched() As Long
        Get
            If Not IsSearching Then Return -1
            Return TotalSearched
        End Get
    End Property

    ''' <summary>
    ''' Returns the number of bytes a search has left to loop through before it's finished its operation
    ''' </summary>
    ''' <returns>Total number of bytes left to search. -1 returned if no search is being performed</returns>
    ReadOnly Property BytesToSearch() As Long
        Get
            If Not IsSearching Then Return -1
            Return TotalLeftToSearch
        End Get
    End Property

    ''' <summary>
    ''' Suspends access to current underlying stream (FileStream only)
    ''' </summary>
    Sub Pause()
        If Not CanPause Then Throw New Exception("Stream type cannot be paused or resumed. Can only pause or resume access to files")
        If StreamPaused Then Throw New Exception("Stream access is already paused")
        If IsSearching Then AbortSearch()
        Reader.Close() : Reader = Nothing
        OpenStream.Close() : OpenStream = Nothing
        StreamPaused = True
    End Sub

    ''' <summary>
    ''' Resumes access to underlying stream if previously paused
    ''' </summary>
    ''' <param name="Position">Offset to restore stream position to. Optional.</param>
    ''' <param name="EndianType">Byte order in which to read/write data. Optional.</param>
    ''' <param name="FileAccess">A System.IO.FileAccess contstant that determines how the file can be access by the object. Optional.</param>
    ''' <param name="FileShare">A System.IO.FileAccess contstant that determines how the file will be shared by processes. Optional.</param>
    Sub [Resume](Optional ByVal Position As Long = 0, Optional ByVal EndianType As Endian = Endian.Little, Optional ByVal FileAccess As FileAccess = FileAccess.Read, Optional ByVal FileShare As FileShare = FileShare.ReadWrite)
        If Not CanPause Then Throw New Exception("Stream type cannot be paused or resumed. Can only pause or resume access to files")
        If Not StreamPaused Then Throw New Exception("Stream access is not paused")
        If Not System.IO.File.Exists(OpenFile) Then Throw New FileNotFoundException(New String(OpenFile & " was not found or could not be accessed"))
        Access = FileAccess
        Share = FileShare
        OpenStream = New FileStream(OpenFile, FileMode.Open, FileAccess, FileShare)
        Reader = New BinaryReader(OpenStream)
        Me.Position = Position
        CurrentEndian = EndianType
        StreamPaused = False
    End Sub

    ''' <summary>
    ''' Closes the current reader and optionally the underlying stream
    ''' </summary>
    ''' <param name="CloseUnderlying">Whether to close underlying stream</param>
    ''' <param name="Dispose">Whether to dispose of the class base after closing</param>
    Sub Close(Optional ByVal CloseUnderlying As Boolean = True, Optional ByVal Dispose As Boolean = False)
        If IsSearching Then AbortSearch()
        If IsOpen Then
            Reader.Close() : Reader = Nothing
            If CloseUnderlying Then
                OpenStream.Close()
                OpenStream = Nothing
            End If
        End If
        OpenFile = ""
        If Dispose Then Me.Dispose()
    End Sub

    ''' <summary>
    ''' Creates a shallow copy of the current class base
    ''' </summary>
    ''' <returns>Returns a shallow copy reference of the current class base</returns>
    Overloads Function Clone() As Object Implements System.ICloneable.Clone
        Return DirectCast(Me.MemberwiseClone, Reader)
    End Function

    ''' <summary>
    ''' Disposes of the class base and forces immediate garbage collection
    ''' </summary>
    Overloads Sub Dispose() Implements System.IDisposable.Dispose
        MyBase.Finalize()
        GC.Collect(GC.GetGeneration(Me), GCCollectionMode.Forced)
    End Sub

    ''' <summary>
    ''' Determines whether the specified Reader instance is equal to the current instance
    ''' </summary>
    ''' <param name="obj">An existing instance of the Reader class to compare to the current instance</param>
    Overloads Function Equals(ByVal obj As Reader) As Boolean Implements System.IEquatable(Of Reader).Equals
        Return MyBase.Equals(obj)
    End Function

#Region "Events/Handlers"

    Private Sub OnSearch() Handles Me.SearchingEvent
        Searching = True
        SearchCompleted = False
        TotalSearched = 0
        TotalLeftToSearch = 0
    End Sub

    Private Sub OnSearchComplete() Handles Me.SearchCompletedEvent
        Searching = False
        SearchCompleted = True
    End Sub

    Private Sub OnSearchAdvance(ByRef BeginOffset As Long, ByRef CurrentOffset As Long, ByRef EndOffset As Long) Handles Me.SearchAdvancedEvent
        TotalSearched = (CurrentOffset - BeginOffset)
        TotalLeftToSearch = (EndOffset - CurrentOffset)
    End Sub

    Private Event SearchingEvent()
    Private Event SearchCompletedEvent()
    Private Event SearchAdvancedEvent(ByRef BeginOffset As Long, ByRef CurrentOffset As Long, ByRef EndOffset As Long)

#End Region

End Class

''' <summary>
''' A multi-endian binary writer capable of writing various data types
''' </summary>
Public Class Writer
    Implements IDisposable, ICloneable, IEquatable(Of Writer)
    Private OpenFile As String = "", OpenStream As Stream = Nothing
    Private Access As FileAccess, Share As FileShare, Type As StreamType
    Private Writer As BinaryWriter = Nothing
    Private Last As Long = 0, Endianness As Endian, CanPause As Boolean = True, StreamPaused As Boolean = False, StreamOwner As Boolean = False

    ''' <summary>
    ''' Creates a new instance of the Writer class
    ''' </summary>
    ''' <param name="File">Absolute path to the file to open or create</param>
    ''' <param name="EndianType">Initial byte order in which to write data. Optional.</param>
    ''' <param name="Position">Initial offset to set stream position to. Optional.</param>
    ''' <param name="FileAccess">A System.IO.FileAccess contstant that determines how the file can be access by the object. Optional.</param>
    ''' <param name="FileShare">A System.IO.FileAccess contstant that determines how the file will be shared by processes. Optional.</param>
    Sub New(ByVal File As String, Optional ByVal EndianType As Endian = Endian.Little, Optional ByVal Position As Long = 0, Optional ByVal FileMode As FileMode = FileMode.Open, Optional ByVal FileAccess As FileAccess = FileAccess.Write, Optional ByVal FileShare As FileShare = FileShare.ReadWrite)
        MyBase.New()
        If Not FileMode = FileMode.Create AndAlso Not FileMode = FileMode.CreateNew Then If Not System.IO.File.Exists(File) Then Throw New FileNotFoundException(New String(File & " was not found or could not be accessed"))
        OpenFile = File
        Access = FileAccess
        Share = FileShare
        Type = StreamType.FileStream
        OpenStream = New FileStream(File, FileMode, FileAccess, FileShare)
        Writer = New BinaryWriter(OpenStream)
        StreamOwner = True
        Me.Position = Position
        CurrentEndian = EndianType
    End Sub

    ''' <summary>
    ''' Creates a new instance of the Writer class
    ''' </summary>
    ''' <param name="Buffer">An existing memory buffer holding data to do IO operations to</param>
    ''' <param name="EndianType">Initial byte order in which to read/write data. Optional.</param>
    ''' <param name="Position">Initial offset to set stream position to. Optional.</param>
    Sub New(ByVal Buffer As Byte(), Optional ByVal EndianType As Endian = Endian.Little, Optional ByVal Position As Long = 0)
        MyBase.New()
        Type = StreamType.MemoryStream
        OpenStream = New MemoryStream(Buffer, True)
        Writer = New BinaryWriter(OpenStream)
        StreamOwner = True
        Me.Position = Position
        CurrentEndian = EndianType
        CanPause = False
    End Sub

    ''' <summary>
    ''' Creates a new instance of the Writer class
    ''' </summary>
    ''' <param name="Stream">An existing stream to do IO operations to</param>
    ''' <param name="EndianType">Initial byte order in which to read/write data. Optional.</param>
    ''' <param name="Position">Initial offset to set stream position to. Optional.</param>
    Sub New(ByVal Stream As Stream, Optional ByVal EndianType As Endian = Endian.Little, Optional ByVal Position As Long = 0)
        MyBase.New()
        If Not Stream.CanWrite Then Throw New InvalidDataException("Stream does not have write access")
        Type = StreamType.GenericStream
        OpenStream = Stream
        Writer = New BinaryWriter(OpenStream)
        Me.Position = Position
        CurrentEndian = EndianType
        CanPause = False
    End Sub

    ''' <summary>
    ''' Returns the current working stream in use by the instance
    ''' </summary>
    ReadOnly Property CurrentStream() As Stream
        Get
            Return OpenStream
        End Get
    End Property

    ''' <summary>
    ''' Returns StreamType of the current stream
    ''' </summary>
    ''' <returns>Returns a StreamType enumerator value indicating which type of stream the underlying stream is</returns>
    ReadOnly Property StreamType() As StreamType
        Get
            Return Type
        End Get
    End Property

    ''' <summary>
    ''' True or false whether the underlying stream was initially created by the instance
    ''' </summary>
    ''' <returns>Returns a boolean value indicating if the underlying stream was created by the instance</returns>
    ReadOnly Property IsOwner() As Boolean
        Get
            Return StreamOwner
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the current position (offset) of the stream
    ''' </summary>
    ''' <value>Offset to set stream position to</value>
    ''' <returns>Current offset stream position is at</returns>
    Property Position() As Long
        Get
            CheckPaused()
            Return OpenStream.Position
        End Get
        Set(ByVal value As Long)
            CheckPaused()
            If Last <> OpenStream.Position Then Last = OpenStream.Position
            OpenStream.Position = value
        End Set
    End Property

    ''' <summary>
    ''' Sets stream postion to the last position before last operation or position set
    ''' </summary>
    Sub JumpBack()
        Position = LastPosition
    End Sub

    ''' <summary>
    ''' Moves stream position to specified offset
    ''' </summary>
    ''' <param name="Offset">Offset to set stream position to</param>
    Sub Seek(ByVal Offset As Long)
        Seek(Offset, SeekOrigin.Begin)
    End Sub

    ''' <summary>
    ''' Moves stream position to specified offset
    ''' </summary>
    ''' <param name="Offset"></param>
    ''' <param name="SeekOrigin">A value of type System.IO.SeekOrigin indicating the reference point used to obtain the new position</param>
    Sub Seek(ByVal Offset As Long, ByVal SeekOrigin As SeekOrigin)
        CheckPaused()
        If Last <> OpenStream.Position Then Last = OpenStream.Position
        Writer.BaseStream.Seek(Offset, SeekOrigin)
    End Sub

    ''' <summary>
    ''' Gets the length in bytes of the current stream
    ''' </summary>
    ''' <returns>Current length in bytes of the underlying stream</returns>
    ReadOnly Property Length() As Long
        Get
            Return OpenStream.Length
        End Get
    End Property

    ''' <summary>
    ''' Gets the length in bytes from current position to end of stream
    ''' </summary>
    ''' <returns>Length in bytes from current position to length of stream (Length - Position)</returns>
    ReadOnly Property LengthToEnd() As Long
        Get
            Return CLng(Length - Position)
        End Get
    End Property

    ''' <summary>
    ''' Get or set the endianness currently in use by the Writer
    ''' </summary>
    Property CurrentEndian() As Endian
        Get
            Return Endianness
        End Get
        Set(ByVal value As Endian)
            If Not [Enum].IsDefined(GetType(Endian), value) Then Return
            Endianness = value
        End Set
    End Property

    ''' <summary>
    ''' Returns the position before last operation or position set
    ''' </summary>
    ReadOnly Property LastPosition() As Long
        Get
            Return Last
        End Get
    End Property

    ''' <summary>
    ''' Returns all bytes in the current stream
    ''' </summary>
    ''' <returns>Buffer containing all bytes in the underlying stream</returns>
    Function GetBuffer() As Byte()
        Dim Buffer() As Byte = New Byte() {}
        GetBuffer(Buffer)
        Return Buffer
    End Function

    ''' <summary>
    ''' Copies all bytes in underlying stream to refrenced byte array
    ''' </summary>
    ''' <param name="OutBuffer">Buffer to store underlying stream bytes</param>
    Sub GetBuffer(<[In](), [Out]()> ByRef OutBuffer As Byte())
        CheckPaused()
        Functions.StreamToBuffer(OpenStream, OutBuffer)
    End Sub

    ''' <summary>
    ''' Writes bytes to underlying stream and advances stream position by count of bytes
    ''' </summary>
    ''' <param name="Buffer">Bytes to be written</param>
    Sub Write(ByVal Buffer As Byte())
        Write(Buffer, Buffer.Length, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes bytes to underlying stream and advances stream position by count
    ''' </summary>
    ''' <param name="Buffer">Bytes to be written</param>
    ''' <param name="Length">Count of bytes to be written</param>
    Sub Write(ByVal Buffer As Byte(), ByVal Length As Integer)
        Write(Buffer, Length, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes bytes to underlying stream and advances stream position by count
    ''' </summary>
    ''' <param name="Buffer">Bytes to be written</param>
    ''' <param name="Length">Count of bytes to be written</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub Write(ByVal Buffer As Byte(), ByVal Length As Integer, ByVal EndianType As Endian)
        CheckPaused()
        Last = OpenStream.Position
        If EndianType = Endian.Big Then SwapSex(Buffer)
        Writer.Write(Buffer, 0, Length)
    End Sub

    ''' <summary>
    ''' Writes bytes to underlying stream and advances stream position by count of bytes
    ''' </summary>
    ''' <param name="Buffer">Bytes to be written</param>
    Sub WriteBytes(ByVal Buffer As Byte())
        Write(Buffer)
    End Sub

    ''' <summary>
    ''' Writes bytes to underlying stream and advances stream position by count
    ''' </summary>
    ''' <param name="Buffer">Bytes to be written</param>
    ''' <param name="Length">Count of bytes to be written</param>
    Sub WriteBytes(ByVal Buffer As Byte(), ByVal Length As Integer)
        Write(Buffer, Length)
    End Sub

    ''' <summary>
    ''' Writes bytes to underlying stream and advances stream position by count
    ''' </summary>
    ''' <param name="Buffer">Bytes to be written</param>
    ''' <param name="Length">Count of bytes to be written</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteBytes(ByVal Buffer As Byte(), ByVal Length As Integer, ByVal EndianType As Endian)
        Write(Buffer, Length, EndianType)
    End Sub

    ''' <summary>
    ''' Inserts bytes to underlying stream and advances stream position by count of bytes inserted
    ''' </summary>
    ''' <param name="Buffer">Bytes to be inserted</param>
    Sub Insert(ByVal Buffer As Byte())
        Insert(Buffer, Buffer.Length)
    End Sub

    ''' <summary>
    ''' Inserts bytes to underlying stream and advances stream position by count
    ''' </summary>
    ''' <param name="Buffer">Bytes to be inserted</param>
    ''' <param name="Length">Count of bytes to be inserted</param>
    Sub Insert(ByVal Buffer As Byte(), ByVal Length As Integer)
        Insert(Buffer, Length, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Inserts bytes to underlying stream and advances stream position by count
    ''' </summary>
    ''' <param name="Buffer">Bytes to be inserted</param>
    ''' <param name="Length">Count of bytes to be inserted</param>
    ''' <param name="EndianType">Endianness in which to insert data</param>
    Sub Insert(ByVal Buffer As Byte(), ByVal Length As Integer, ByVal EndianType As Endian)
        CheckPaused()
        If Buffer Is Nothing OrElse Buffer.Length = 0 OrElse Length = 0 Then Return
        If Not Functions.AvailableMemory() >= LengthToEnd OrElse Not Functions.AvailableMemory() >= (Length + Me.Length) Then Throw New System.OutOfMemoryException("There is not enough memory availible to the system to insert specified length of bytes")
        If Type = StreamType.FileStream AndAlso Not OpenStream.CanRead OrElse Not OpenStream.CanWrite Then FileAccess = System.IO.FileAccess.ReadWrite
        If LengthToEnd <> 0 Then
            Dim x As Long = Position
            Dim Chunk(LengthToEnd - 1) As Byte
            OpenStream.Read(Chunk, 0, LengthToEnd)
            Position = x
            Write(Buffer, Length, EndianType)
            Write(Chunk, Chunk.Length, Endian.Little)
            Position = CLng(x + Length)
            Array.Clear(Chunk, 0, Chunk.Length)
        Else
            Write(Buffer, Length, EndianType)
        End If
    End Sub

    ''' <summary>
    ''' Inserts bytes to underlying stream and advances stream position by count of bytes inserted
    ''' </summary>
    ''' <param name="Buffer">Bytes to be inserted</param>
    Sub InsertBytes(ByVal Buffer As Byte())
        Insert(Buffer)
    End Sub

    ''' <summary>
    ''' Inserts bytes to underlying stream and advances stream position by count
    ''' </summary>
    ''' <param name="Buffer">Bytes to be inserted</param>
    ''' <param name="Length">Count of bytes to be inserted</param>
    Sub InsertBytes(ByVal Buffer As Byte(), ByVal Length As Integer)
        Insert(Buffer, Length)
    End Sub

    ''' <summary>
    ''' Inserts bytes to underlying stream and advances stream position by count
    ''' </summary>
    ''' <param name="Buffer">Bytes to be inserted</param>
    ''' <param name="Length">Count of bytes to be inserted</param>
    ''' <param name="EndianType">Endianness in which to insert data</param>
    Sub InsertBytes(ByVal Buffer As Byte(), ByVal Length As Integer, ByVal EndianType As Endian)
        Insert(Buffer, Length, EndianType)
    End Sub

    ''' <summary>
    ''' Removes (deletes) bytes from underlying stream and leaves stream position untouched
    ''' </summary>
    ''' <param name="Length">Count of bytes to be removed (deleted)</param>
    Sub Delete(ByVal Length As Integer)
        CheckPaused()
        If Length = 0 OrElse LengthToEnd = 0 Then Return
        If (LengthToEnd - Length) < 0 Then Throw New DataException("Length is longer than the number of bytes left to end of stream. Not enough bytes to delete")
        If Not Functions.AvailableMemory >= (Me.Length + Length) Then Throw New System.OutOfMemoryException("There is not enough memory availible to the system to delete specified length of bytes")
        If Type = StreamType.FileStream AndAlso Not OpenStream.CanRead OrElse Not OpenStream.CanWrite Then FileAccess = System.IO.FileAccess.ReadWrite
        Dim Buffer(Me.Length - 1) As Byte
        Dim x As Long = Position
        Position = 0
        OpenStream.Flush()
        OpenStream.Read(Buffer, 0, Me.Length)
        OpenStream.Close()
        Buffer = Functions.DeleteBytes(Buffer, x, Length)
        If Type = StreamType.FileStream Then
            File.Delete(OpenFile)
            File.Create(OpenFile).Close()
            OpenStream = New FileStream(OpenFile, FileMode.Open, Access, Share)
        Else
            OpenStream = New MemoryStream(Buffer, True)
        End If
        Writer = New BinaryWriter(OpenStream)
        Writer.Write(Buffer, 0, Buffer.Length)
        Writer.Flush()
        Position = x
    End Sub

    ''' <summary>
    ''' Removes (deletes) bytes from underlying stream and leaves stream position untouched
    ''' </summary>
    ''' <param name="Length">Count of bytes to be removed (deleted)</param>
    Sub DeleteBytes(ByVal Length As Integer)
        Delete(Length)
    End Sub

    ''' <summary>
    ''' Writes zeroed (00) bytes to underlying stream and advances stream position by count
    ''' </summary>
    ''' <param name="Length">Count of zeroed bytes to write</param>
    Sub WriteNull(ByVal Length As Integer)
        Dim Buffer(Length - 1) As Byte
        Write(Buffer)
    End Sub

    ''' <summary>
    ''' Writes a single unsigned byte to underlying stream and advances stream position by 1
    ''' </summary>
    ''' <param name="Value">Unsigned byte to write to underlying stream</param>
    Sub WriteByte(ByVal Value As Byte)
        Write(New Byte() {CByte(Value)})
    End Sub

    ''' <summary>
    ''' Writes a single signed byte to underlying stream and advances stream position by 1
    ''' </summary>
    ''' <param name="Value">Signed byte to write to underlying stream</param>
    Sub WriteSByte(ByVal Value As SByte)
        Write(New Byte() {CSByte(Value)})
    End Sub

    ''' <summary>
    ''' Writes a single signed byte to underlying stream and advances stream position by 1
    ''' </summary>
    ''' <param name="Value">Signed byte to write to underlying stream</param>
    Sub WriteInt8(ByVal Value As SByte)
        WriteSByte(Value)
    End Sub

    ''' <summary>
    ''' Writes a single unsigned byte to underlying stream and advances stream position by 1
    ''' </summary>
    ''' <param name="Value">Unsigned byte to write to underlying stream</param>
    Sub WriteUInt8(ByVal Value As Byte)
        WriteByte(Value)
    End Sub

    ''' <summary>
    ''' Writes a 16bit signed integer to underlying stream and advances stream position by 2
    ''' </summary>
    ''' <param name="Value">A 16bit signed integer value to write to underlying stream</param>
    Sub WriteInt16(ByVal Value As Short)
        WriteInt16(Value, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a 16bit signed integer to underlying stream and advances stream position by 2
    ''' </summary>
    ''' <param name="Value">A 16bit signed integer value to write to underlying stream</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteInt16(ByVal Value As Short, ByVal EndianType As Endian)
        Dim Buffer As Byte() = BitConverter.GetBytes(CShort(Value))
        Write(Buffer, 2, EndianType)
    End Sub

    ''' <summary>
    ''' Writes a 16bit unsigned integer to underlying stream and advances stream position by 2
    ''' </summary>
    ''' <param name="Value">A 16bit unsigned integer value to write to underlying stream</param>
    Sub WriteUInt16(ByVal Value As UShort)
        WriteUInt16(Value, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a 16bit unsigned integer to underlying stream and advances stream position by 2
    ''' </summary>
    ''' <param name="Value">A 16bit unsigned integer value to write to underlying stream</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteUInt16(ByVal Value As UShort, ByVal EndianType As Endian)
        Dim Buffer As Byte() = BitConverter.GetBytes(CUShort(Value))
        Write(Buffer, 2, EndianType)
    End Sub

    ''' <summary>
    ''' Writes a 24bit signed integer to underlying stream and advances stream position by 3
    ''' </summary>
    ''' <param name="Value">A 24bit signed integer value to write to underlying stream</param>
    Sub WriteInt24(ByVal Value As Integer)
        WriteInt24(Value, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a 24bit signed integer to underlying stream and advances stream position by 3
    ''' </summary>
    ''' <param name="Value">A 24bit signed integer value to write to underlying stream</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteInt24(ByVal Value As Integer, ByVal EndianType As Endian)
        Write(Int24ToBytes(Value, IIf(EndianType = Endian.Big, True, False)), 3, Endian.Little)
    End Sub

    ''' <summary>
    ''' Writes a 24bit unsigned integer to underlying stream and advances stream position by 3
    ''' </summary>
    ''' <param name="Value">A 24bit unsigned integer value to write to underlying stream</param>
    Sub WriteUInt24(ByVal Value As UInteger)
        WriteUInt24(Value, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a 24bit unsigned integer to underlying stream and advances stream position by 3
    ''' </summary>
    ''' <param name="Value">A 24bit unsigned integer value to write to underlying stream</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteUInt24(ByVal Value As UInteger, ByVal EndianType As Endian)
        Write(UInt24ToBytes(Value, IIf(EndianType = Endian.Big, True, False)), 3, Endian.Little)
    End Sub

    ''' <summary>
    ''' Writes a 32bit signed integer to underlying stream and advances stream position by 4
    ''' </summary>
    ''' <param name="Value">A 32bit signed integer value to write to underlying stream</param>
    Sub WriteInt32(ByVal Value As Int32)
        WriteInt32(Value, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a 32bit signed integer to underlying stream and advances stream position by 4
    ''' </summary>
    ''' <param name="Value">A 32bit signed integer value to write to underlying stream</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteInt32(ByVal Value As Int32, ByVal EndianType As Endian)
        Dim Buffer As Byte() = BitConverter.GetBytes(CInt(Value))
        Write(Buffer, 4, EndianType)
    End Sub

    ''' <summary>
    ''' Writes a 32bit unsigned integer to underlying stream and advances stream position by 4
    ''' </summary>
    ''' <param name="Value">A 32bit unsigned integer value to write to underlying stream</param>
    Sub WriteUInt32(ByVal Value As UInt32)
        WriteUInt32(Value, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a 32bit unsigned integer to underlying stream and advances stream position by 4
    ''' </summary>
    ''' <param name="Value">A 32bit unsigned integer value to write to underlying stream</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteUInt32(ByVal Value As UInt32, ByVal EndianType As Endian)
        Dim Buffer As Byte() = BitConverter.GetBytes(CUInt(Value))
        Write(Buffer, 4, EndianType)
    End Sub

    ''' <summary>
    ''' Writes a 40bit signed integer to underlying stream and advances stream position by 5
    ''' </summary>
    ''' <param name="Value">A 40bit signed integer value to write to underlying stream</param>
    Sub WriteInt40(ByVal Value As Long)
        WriteInt40(Value, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a 40bit signed integer to underlying stream and advances stream position by 5
    ''' </summary>
    ''' <param name="Value">A 40bit signed integer value to write to underlying stream</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteInt40(ByVal Value As Long, ByVal EndianType As Endian)
        Write(Int40ToBytes(CLng(Value)), 5, EndianType)
    End Sub

    ''' <summary>
    ''' Writes a 40bit unsigned integer to underlying stream and advances stream position by 5
    ''' </summary>
    ''' <param name="Value">A 40bit unsigned integer value to write to underlying stream</param>
    Sub WriteUInt40(ByVal Value As ULong)
        WriteUInt40(Value, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a 40bit unsigned integer to underlying stream and advances stream position by 5
    ''' </summary>
    ''' <param name="Value">A 40bit unsigned integer value to write to underlying stream</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteUInt40(ByVal Value As ULong, ByVal EndianType As Endian)
        Write(UInt40ToBytes(CULng(Value)), 5, EndianType)
    End Sub

    ''' <summary>
    ''' Writes a 48bit signed integer to underlying stream and advances stream position by 6
    ''' </summary>
    ''' <param name="Value">A 48bit signed integer value to write to underlying stream</param>
    Sub WriteInt48(ByVal Value As Long)
        WriteInt48(Value, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a 48bit signed integer to underlying stream and advances stream position by 6
    ''' </summary>
    ''' <param name="Value">A 48bit signed integer value to write to underlying stream</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteInt48(ByVal Value As Long, ByVal EndianType As Endian)
        Write(Int48ToBytes(CLng(Value)), 6, EndianType)
    End Sub

    ''' <summary>
    ''' Writes a 48bit unsigned integer to underlying stream and advances stream position by 6
    ''' </summary>
    ''' <param name="Value">A 48bit unsigned integer value to write to underlying stream</param>
    Sub WriteUInt48(ByVal Value As ULong)
        WriteUInt48(Value, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a 48bit unsigned integer to underlying stream and advances stream position by 6
    ''' </summary>
    ''' <param name="Value">A 48bit unsigned integer value to write to underlying stream</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteUInt48(ByVal Value As ULong, ByVal EndianType As Endian)
        Write(UInt48ToBytes(CULng(Value)), 6, EndianType)
    End Sub

    ''' <summary>
    ''' Writes a 56bit signed integer to underlying stream and advances stream position by 7
    ''' </summary>
    ''' <param name="Value">A 56bit signed integer value to write to underlying stream</param>
    Sub WriteInt56(ByVal Value As Long)
        WriteInt56(Value, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a 56bit signed integer to underlying stream and advances stream position by 7
    ''' </summary>
    ''' <param name="Value">A 56bit signed integer value to write to underlying stream</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteInt56(ByVal Value As Long, ByVal EndianType As Endian)
        Write(Int56ToBytes(CLng(Value)), 7, EndianType)
    End Sub

    ''' <summary>
    ''' Writes a 56bit unsigned integer to underlying stream and advances stream position by 7
    ''' </summary>
    ''' <param name="Value">A 56bit unsigned integer value to write to underlying stream</param>
    Sub WriteUInt56(ByVal Value As ULong)
        WriteUInt56(Value, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a 56bit unsigned integer to underlying stream and advances stream position by 7
    ''' </summary>
    ''' <param name="Value">A 56bit unsigned integer value to write to underlying stream</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteUInt56(ByVal Value As ULong, ByVal EndianType As Endian)
        Write(UInt56ToBytes(CULng(Value)), 7, EndianType)
    End Sub

    ''' <summary>
    ''' Writes a 64bit signed integer to underlying stream and advances stream position by 8
    ''' </summary>
    ''' <param name="Value">A 64bit signed integer value to write to underlying stream</param>
    Sub WriteInt64(ByVal Value As Int64)
        WriteInt64(Value, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a 64bit signed integer to underlying stream and advances stream position by 8
    ''' </summary>
    ''' <param name="Value">A 64bit signed integer value to write to underlying stream</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteInt64(ByVal Value As Int64, ByVal EndianType As Endian)
        Dim Buffer As Byte() = BitConverter.GetBytes(CLng(Value))
        Write(Buffer, 8, EndianType)
    End Sub

    ''' <summary>
    ''' Writes a 64bit unsigned integer to underlying stream and advances stream position by 8
    ''' </summary>
    ''' <param name="Value">A 64bit unsigned integer value to write to underlying stream</param>
    Sub WriteUInt64(ByVal Value As UInt64)
        WriteUInt64(Value, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a 64bit unsigned integer to underlying stream and advances stream position by 8
    ''' </summary>
    ''' <param name="Value">A 64bit unsigned integer value to write to underlying stream</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteUInt64(ByVal Value As UInt64, ByVal EndianType As Endian)
        Dim Buffer As Byte() = BitConverter.GetBytes(CULng(Value))
        Write(Buffer, 8, EndianType)
    End Sub

    ''' <summary>
    ''' Writes a 32bit float to underlying stream and advances stream position by 4
    ''' </summary>
    ''' <param name="Value">A 32bit float value to write to underlying stream</param>
    Sub WriteSingle(ByVal Value As Single)
        WriteSingle(Value, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a 32bit float to underlying stream and advances stream position by 4
    ''' </summary>
    ''' <param name="Value">A 32bit float value to write to underlying stream</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteSingle(ByVal Value As Single, ByVal EndianType As Endian)
        Dim Buffer As Byte() = BitConverter.GetBytes(CSng(Value))
        Me.Write(Buffer, 4, EndianType)
    End Sub

    ''' <summary>
    ''' Writes a 64bit double to underlying stream and advances stream position by 8
    ''' </summary>
    ''' <param name="Value">A 64bit double float value to write to underlying stream</param>
    Sub WriteDouble(ByVal Value As Double)
        WriteDouble(Value, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a 64bit double to underlying stream and advances stream position by 8
    ''' </summary>
    ''' <param name="Value">A 64bit double value to write to underlying stream</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteDouble(ByVal Value As Double, ByVal EndianType As Endian)
        Dim Buffer As Byte() = BitConverter.GetBytes(CDbl(Value))
        Write(Buffer, 8, EndianType)
    End Sub

    ''' <summary>
    ''' Writes a single UTF8 encoded character to underlying stream and advances stream position by 1
    ''' </summary>
    ''' <param name="Value">A single character to write to underlying stream</param>
    Sub WriteChar(ByVal Value As Char)
        CheckPaused()
        Last = OpenStream.Position
        Writer.Write(CChar(Value))
    End Sub

    ''' <summary>
    ''' Writes a sequence UTF8 encoded characters to underlying stream and advances stream position by count of charaters
    ''' </summary>
    ''' <param name="Value">An array of characters to write to underlying stream</param>
    Sub WriteChars(ByVal Value As Char())
        For Each Letter As Char In Value
            WriteChar(Letter)
        Next
    End Sub

    ''' <summary>
    ''' Writes a string containing binary values (bits) to underlying stream and advances stream position by ((count / 8) + (8 Mod count)) rounded up to nearest multiple of 8
    ''' </summary>
    ''' <param name="Value">A binary (bit) string</param>
    Sub WriteBinary(ByVal Value As String)
        WriteBinary(Value, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a string containing binary values (bits) to underlying stream and advances stream position by ((count / 8) + (8 Mod count)) rounded up to nearest multiple of 8
    ''' </summary>
    ''' <param name="Value">A binary (bit) string</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteBinary(ByVal Value As String, ByVal EndianType As Endian)
        Write(BitStringToBytes(Value), EndianType)
    End Sub

    ''' <summary>
    ''' Writes a string containing ASCII encoded charaters to underlying stream and advances stream position by count of characters
    ''' </summary>
    ''' <param name="Value">A string containing ASCII encoded characters</param>
    Sub WriteASCII(ByVal Value As String)
        WriteASCII(Value, Value.Length)
    End Sub

    ''' <summary>
    ''' Writes a string containing ASCII encoded charaters to underlying stream for specified length and advances stream position by Length
    ''' </summary>
    ''' <param name="Value">A string containing ASCII encoded characters</param>
    ''' <param name="Length">Maximum number of characters to write</param>
    Sub WriteASCII(ByVal Value As String, ByVal Length As Integer)
        Dim Buffer As Byte() = ASCII.GetBytes(Value)
        Write(Buffer, Length, Endian.Little)
    End Sub

    ''' <summary>
    ''' Writes a string containing Unicode encoded charaters to underlying stream in currently set endianness and advances stream position by (count of characters * 2)
    ''' </summary>
    ''' <param name="Value">A string containing Unicode encoded characters</param>
    Sub WriteUnicode(ByVal Value As String)
        WriteUnicode(Value, Value.Length)
    End Sub

    ''' <summary>
    ''' Writes a string containing Unicode encoded charaters to underlying stream in currently set endianness and advances stream position by (Length * 2)
    ''' </summary>
    ''' <param name="Value">A string containing Unicode encoded characters</param>
    ''' <param name="Length">Maximum number of characters to write</param>
    Sub WriteUnicode(ByVal Value As String, ByVal Length As Integer)
        WriteUnicode(Value, Length, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a string containing Unicode encoded charaters to underlying stream in specified endianness and advances stream position by (Length * 2)
    ''' </summary>
    ''' <param name="Value">A string containing Unicode encoded characters</param>
    ''' <param name="Length">Maximum number of characters to write</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteUnicode(ByVal Value As String, ByVal Length As Integer, ByVal EndianType As Endian)
        If EndianType = Endian.Big Then
            Write(BigEndianUnicode.GetBytes(Value), Length, Endian.Little)
            Return
        End If
        Write(Unicode.GetBytes(Value), Length, Endian.Little)
    End Sub

    ''' <summary>
    ''' Writes a string containing Hexadecimal values to underlying stream in specified endianness and advances stream position by count of digits
    ''' </summary>
    ''' <param name="Value">A string containing Hexadecimal values</param>
    Sub WriteHex(ByVal Value As String)
        WriteHex(Value, Value.Length)
    End Sub

    ''' <summary>
    ''' Writes a string containing Hexadecimal values to underlying stream in specified endianness and advances stream position by Length
    ''' </summary>
    ''' <param name="Value">A string containing Hexadecimal values</param>
    ''' <param name="Length">Maximum number of bytes to write</param>
    Sub WriteHex(ByVal Value As String, ByVal Length As Integer)
        WriteHex(Value, Length, CurrentEndian)
    End Sub

    ''' <summary>
    ''' Writes a string containing Hexadecimal values to underlying stream in specified endianness and advances stream position by Length
    ''' </summary>
    ''' <param name="Value">A string containing Hexadecimal values</param>
    ''' <param name="Length">Maximum number of bytes to write</param>
    ''' <param name="EndianType">Endianness in which to write data</param>
    Sub WriteHex(ByVal Value As String, ByVal Length As Integer, ByVal EndianType As Endian)
        Write(HexToBytes(Value), (Length / 2), EndianType)
    End Sub

    ''' <summary>
    ''' Gets or sets the current FileAccess flag of the stream (FileStream only)
    ''' </summary>
    ''' <value>FileAccess enum value to attempt to change the underlying stream to</value>
    ''' <returns>Returns current FileAccess enum value the underlying stream is set to</returns>
    Property FileAccess() As FileAccess
        Get
            CheckPaused()
            If Type <> StreamType.FileStream Then Throw New Exception("Current stream type is not FileStream")
            Return Access
        End Get
        Set(ByVal value As FileAccess)
            If value = Access Then Return
            CheckPaused()
            If Type <> StreamType.FileStream Then Throw New Exception("Current stream type is not FileStream")
            Dim x As Long = Position
            OpenStream.Flush()
            OpenStream.Close()
            Access = value
            OpenStream = New FileStream(OpenFile, FileMode.Open, Access, Share)
            Position = x
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the current FileShare flag of the stream (FileStream only)
    ''' </summary>
    ''' <value>FileShare enum value to attempt to change the underlying stream to</value>
    ''' <returns>Returns current FileShare enum value the underlying stream is set to</returns>
    Property FileShare() As FileShare
        Get
            CheckPaused()
            If Type <> StreamType.FileStream Then Throw New Exception("Current stream type is not FileStream")
            Return Share
        End Get
        Set(ByVal value As FileShare)
            If value = Share Then Return
            CheckPaused()
            If Type <> StreamType.FileStream Then Throw New Exception("Current stream type is not FileStream")
            Dim x As Long = Position
            OpenStream.Flush()
            OpenStream.Close()
            Share = value
            OpenStream = New FileStream(OpenFile, FileMode.Open, Access, Share)
            Position = x
        End Set
    End Property

    ''' <summary>
    ''' Swtiches the current endianness to type not currently in use
    ''' </summary>
    Sub SwapEndian()
        If CurrentEndian = Endian.Little Then
            CurrentEndian = Endian.Big
        Else
            CurrentEndian = Endian.Little
        End If
    End Sub

    ''' <summary>
    ''' Gets the opposite endianness not currently in use
    ''' </summary>
    ''' <returns>Returns Endian enum value indicating the endianness not currently in use</returns>
    ReadOnly Property OppositeEndian() As Endian
        Get
            If CurrentEndian = Endian.Little Then Return Endian.Big
            Return Endian.Little
        End Get
    End Property

    Private Sub CheckPaused()
        If Not IsOpen Then Throw New Exception("Stream access is currently paused")
    End Sub

    ''' <summary>
    ''' Whether or not access to the underlying stream can be suspended
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returns true or false whether access to underlying stream can be paused</returns>
    ReadOnly Property Pausable() As Boolean
        Get
            Return CanPause
        End Get
    End Property

    ''' <summary>
    ''' Whether the instance is currently accessing the underlying stream
    ''' </summary>
    ''' <returns>True or false whether the instance is currently accessing the underlying stream</returns>
    ReadOnly Property IsOpen() As Boolean
        Get
            If Writer Is Nothing OrElse OpenStream Is Nothing OrElse IsPaused Then Return False
            Return True
        End Get
    End Property

    ''' <summary>
    ''' Gets whether access to the underlying stream is suspended
    ''' </summary>
    ''' <returns>Returns whether access to the underlying stream is paused</returns>
    ReadOnly Property IsPaused() As Boolean
        Get
            Return StreamPaused
        End Get
    End Property

    ''' <summary>
    ''' Suspends access to current underlying stream (FileStream only)
    ''' </summary>
    Sub Pause()
        If Not CanPause Then Throw New Exception("Stream type cannot be paused or resumed. Can only pause or resume access to files")
        If StreamPaused Then Throw New Exception("Stream access is already paused")
        Flush()
        Writer.Close() : Writer = Nothing
        OpenStream.Close() : OpenStream = Nothing
        StreamPaused = True
    End Sub

    ''' <summary>
    ''' Resumes access to underlying stream if previously paused
    ''' </summary>
    ''' <param name="Position">Offset to restore stream position to. Optional.</param>
    ''' <param name="EndianType">Byte order in which to read/write data. Optional.</param>
    ''' <param name="FileAccess">A System.IO.FileAccess contstant that determines how the file can be access by the object. Optional.</param>
    ''' <param name="FileShare">A System.IO.FileAccess contstant that determines how the file will be shared by processes. Optional.</param>
    Sub [Resume](Optional ByVal Position As Long = 0, Optional ByVal EndianType As Endian = Endian.Little, Optional ByVal FileAccess As FileAccess = FileAccess.Write, Optional ByVal FileShare As FileShare = FileShare.ReadWrite)
        If Not CanPause Then Throw New Exception("Stream type cannot be paused or resumed. Can only pause or resume access to files")
        If Not StreamPaused Then Throw New Exception("Stream access is not paused")
        If Not System.IO.File.Exists(OpenFile) Then Throw New FileNotFoundException(New String(OpenFile & " was not found or could not be accessed"))
        Access = FileAccess
        Share = FileShare
        OpenStream = New FileStream(OpenFile, FileMode.Open, FileAccess, FileShare)
        Writer = New BinaryWriter(OpenStream)
        Me.Position = Position
        CurrentEndian = EndianType
        StreamPaused = False
    End Sub

    ''' <summary>
    ''' Ensures all pending writes are written to underlying device and clears any buffered data
    ''' </summary>
    Sub Flush()
        CheckPaused()
        OpenStream.Flush()
    End Sub

    ''' <summary>
    ''' Closes the current Writer and optionally the underlying stream. Flushing will occur beforehand
    ''' </summary>
    ''' <param name="CloseUnderlying">Whether to close underlying stream</param>
    ''' <param name="Dispose">Whether to dispose of the class base after closing</param>
    Sub Close(Optional ByVal CloseUnderlying As Boolean = True, Optional ByVal Dispose As Boolean = False)
        If IsOpen Then
            Writer.Flush() : Writer.Close() : Writer = Nothing
            If CloseUnderlying Then
                OpenStream.Close()
                OpenStream = Nothing
            End If
        End If
        OpenFile = ""
        If Dispose Then Me.Dispose()
    End Sub

    ''' <summary>
    ''' Creates a shallow copy of the current class base
    ''' </summary>
    ''' <returns>Returns a shallow copy reference of the current class base</returns>
    Overloads Function Clone() As Object Implements System.ICloneable.Clone
        Return DirectCast(Me.MemberwiseClone, Writer)
    End Function

    ''' <summary>
    ''' Disposes of the class base and forces immediate garbage collection
    ''' </summary>
    Overloads Sub Dispose() Implements System.IDisposable.Dispose
        MyBase.Finalize()
        GC.Collect(GC.GetGeneration(Me), GCCollectionMode.Forced)
    End Sub

    ''' <summary>
    ''' Determines whether the specified Writer instance is equal to the current instance
    ''' </summary>
    ''' <param name="obj">An existing instance of the Writer class to compare to the current instance</param>
    Overloads Function Equals(ByVal obj As Writer) As Boolean Implements System.IEquatable(Of Writer).Equals
        Return MyBase.Equals(obj)
    End Function

End Class

''' <summary>
''' A conversion class holding methods to convert various data types
''' </summary>
Public Class Conversions

    Shared Function BytesToASCII(ByVal Bytes As Byte()) As String
        Return ASCII.GetString(Bytes)
    End Function

    Shared Function ASCIIToBytes(ByVal Value As String) As Byte()
        If Not IsValidASCII(Value) Then Throw New Exception("Invalid ASCII input")
        Return ASCII.GetBytes(Value)
    End Function

    Shared Function BytesToUnicode(ByVal Bytes As Byte(), Optional ByVal ReturnBigEndian As Boolean = False) As String
        If ReturnBigEndian Then Return BigEndianUnicode.GetString(Bytes)
        Return Unicode.GetString(Bytes)
    End Function

    Shared Function UnicodeToBytes(ByVal Value As String, Optional ByVal ReturnBigEndian As Boolean = False) As Byte()
        If Not IsValidUnicode(Value) Then Throw New Exception("Invalid unicode input")
        If ReturnBigEndian Then Return BigEndianUnicode.GetBytes(Value)
        Return Unicode.GetBytes(Value)
    End Function

    Shared Function BytesToBase64(ByVal Bytes As Byte()) As String
        Return System.Convert.ToBase64String(Bytes)
    End Function

    Shared Function Base64ToBytes(ByVal ASCII As String) As Byte()
        Return System.Convert.FromBase64String(ASCII)
    End Function

    Shared Function Base64ToASCII(ByVal Base64 As String) As String
        Return BytesToASCII(Base64ToBytes(Base64))
    End Function

    Shared Function ASCIIToBase64(ByVal ASCII As String) As String
        Return BytesToBase64(ASCIIToBytes(ASCII))
    End Function

    Shared Function HexToBytes(ByVal Value As String) As Byte()
        If Value Is Nothing OrElse Value.Length = 0 OrElse Not IsValidHex(Value) Then Throw New Exception("Invalid hexadecimal input")
        Dim Buffer As Byte() = New Byte(Value.Length / 2 - 1) {}
        For i As Integer = 0 To (Value.Length / 2) - 1
            Buffer(i) = Val("&h" & Value.Substring((i * 2), 2))
        Next
        Return Buffer
    End Function

    Shared Function BytesToHex(ByVal Bytes As Byte()) As String
        Return ObjectToHex(Bytes)
    End Function

    Shared Function ObjectToHex(ByVal Value As Object) As String
        If Value.GetType.ToString = "System.Byte[]" Then Return BitConverter.ToString(Value).Replace("-", "")
        Return Hex(Value)
    End Function

    Shared Function BytesToInt16(ByVal Bytes As Byte(), Optional ByVal ReturnBigEndian As Boolean = False) As Short
        If ReturnBigEndian Then Bytes = SwapSex(Bytes)
        Return BitConverter.ToInt16(Bytes, 0)
    End Function

    Shared Function Int16ToBytes(ByVal Value As Short, Optional ByVal ReturnBigEndian As Boolean = False) As Byte()
        Dim Buffer As Byte() = BitConverter.GetBytes(CShort(Value))
        If ReturnBigEndian Then Buffer = SwapSex(Buffer)
        Return Buffer
    End Function

    Shared Function BytesToUInt16(ByVal Bytes As Byte(), Optional ByVal ReturnBigEndian As Boolean = False) As UShort
        If ReturnBigEndian Then Bytes = SwapSex(Bytes)
        Return BitConverter.ToUInt16(Bytes, 0)
    End Function

    Shared Function UInt16ToBytes(ByVal Value As UShort, Optional ByVal ReturnBigEndian As Boolean = False) As Byte()
        Dim Buffer As Byte() = BitConverter.GetBytes(CUShort(Value))
        If ReturnBigEndian Then Buffer = SwapSex(Buffer)
        Return Buffer
    End Function

    Shared Function BytesToUInt24(ByVal Buffer As Byte(), Optional ByVal ReturnBigEndian As Boolean = False) As UInteger
        If Buffer Is Nothing OrElse Buffer.Length <> 3 Then Throw New NullReferenceException("Input byte array is null or does not contain enough bytes to convert")
        Dim Value As UInteger = CUInt(Bytes2Int(Buffer, ReturnBigEndian))
        If Value > PowMax(24, False) OrElse Value < PowMin(24, False) Then Throw New OverflowException(New String("Value is to low or to high to be represented as an unsigned 24 bit integer. MaxValue = " & PowMax(24, False) & "MinValue = " & PowMin(24, False)))
        Return Value
    End Function

    Shared Function BytesToInt24(ByVal Buffer As Byte(), Optional ByVal ReturnBigEndian As Boolean = False) As Integer
        If Buffer Is Nothing OrElse Buffer.Length <> 3 Then Throw New NullReferenceException("Input byte array is null or does not contain enough bytes to convert")
        Dim Value As Integer = CInt(Bytes2Int(Buffer, ReturnBigEndian))
        If Value > PowMax(24) OrElse Value < PowMin(24) Then Throw New OverflowException(New String("Value is to low or to high to be represented as a signed 24 bit integer. MaxValue = " & PowMax(24) & "MinValue = " & PowMin(24)))
        Return Value
    End Function

    Shared Function Int24ToBytes(ByVal Value As Integer, Optional ByVal ReturnBigEndian As Boolean = False) As Byte()
        If Value > PowMax(24) OrElse Value < PowMin(24) Then Throw New OverflowException(New String("Value is to low or to high to be represented as a signed 24 bit integer. MaxValue = " & PowMax(24) & "MinValue = " & PowMin(24)))
        Dim Buffer As Byte() = BitConverter.GetBytes(CInt(Value))
        Buffer = New Byte() {Buffer(2), Buffer(1), Buffer(0)}
        If ReturnBigEndian Then Array.Reverse(Buffer)
        Return Buffer
    End Function

    Shared Function UInt24ToBytes(ByVal Value As UInteger, Optional ByVal ReturnBigEndian As Boolean = False) As Byte()
        If Value > PowMax(24, False) OrElse Value < PowMin(24, False) Then Throw New OverflowException(New String("Value is to low or to high to be represented as an unsigned 24 bit integer. MaxValue = " & PowMax(24, False) & "MinValue = " & PowMin(24, False)))
        Dim Buffer As Byte() = BitConverter.GetBytes(CUInt(Value))
        Buffer = New Byte() {Buffer(2), Buffer(1), Buffer(0)}
        If ReturnBigEndian Then Array.Reverse(Buffer)
        Return Buffer
    End Function

    Shared Function BytesToInt32(ByVal Bytes As Byte(), Optional ByVal ReturnBigEndian As Boolean = False) As Integer
        If ReturnBigEndian Then Array.Reverse(Bytes)
        Return BitConverter.ToInt32(Bytes, 0)
    End Function

    Shared Function Int32ToBytes(ByVal Value As Integer, Optional ByVal ReturnBigEndian As Boolean = False) As Byte()
        Dim Buffer As Byte() = BitConverter.GetBytes(CInt(Value))
        If ReturnBigEndian Then Array.Reverse(Buffer)
        Return Buffer
    End Function

    Shared Function BytesToUInt32(ByVal Bytes As Byte(), Optional ByVal ReturnBigEndian As Boolean = False) As UInteger
        If ReturnBigEndian Then Bytes = SwapSex(Bytes)
        Return BitConverter.ToUInt32(Bytes, 0)
    End Function

    Shared Function UInt32ToBytes(ByVal Value As UInteger, Optional ByVal ReturnBigEndian As Boolean = False) As Byte()
        Dim Buffer As Byte() = BitConverter.GetBytes(CUInt(Value))
        If ReturnBigEndian Then Buffer = SwapSex(Buffer)
        Return Buffer
    End Function

    Shared Function BytesToUInt40(ByVal Buffer As Byte(), Optional ByVal ReturnBigEndian As Boolean = False) As ULong
        If Buffer Is Nothing OrElse Buffer.Length <> 5 Then Throw New NullReferenceException("Input byte array is null or does not contain enough bytes to convert")
        Dim Value As ULong = CULng(Bytes2Int(Buffer, ReturnBigEndian))
        If Value > PowMax(40, False) OrElse Value < PowMin(40, False) Then Throw New OverflowException(New String("Value is to low or to high to be represented as an unsigned 40 bit integer. MaxValue = " & PowMax(40, False) & "MinValue = " & PowMin(40, False)))
        Return Value
    End Function

    Shared Function BytesToInt40(ByVal Buffer As Byte(), Optional ByVal ReturnBigEndian As Boolean = False) As Long
        If Buffer Is Nothing OrElse Buffer.Length <> 5 Then Throw New NullReferenceException("Input byte array is null or does not contain enough bytes to convert")
        Dim Value As Long = CLng(Bytes2Int(Buffer, ReturnBigEndian))
        If Value > PowMax(40) OrElse Value < PowMin(40) Then Throw New OverflowException(New String("Value is to low or to high to be represented as a signed 40 bit integer. MaxValue = " & PowMax(40) & "MinValue = " & PowMin(40)))
        Return Value
    End Function

    Shared Function Int40ToBytes(ByVal Value As Long, Optional ByVal ReturnBigEndian As Boolean = False) As Byte()
        If Value > PowMax(40) OrElse Value < PowMin(40) Then Throw New OverflowException(New String("Value is to low or to high to be represented as a signed 40 bit integer. MaxValue = " & PowMax(40) & "MinValue = " & PowMin(40)))
        Dim Buffer As Byte() = BitConverter.GetBytes(CLng(Value))
        Buffer = New Byte() {Buffer(4), Buffer(3), Buffer(2), Buffer(1), Buffer(0)}
        If ReturnBigEndian Then Array.Reverse(Buffer)
        Return Buffer
    End Function

    Shared Function UInt40ToBytes(ByVal Value As ULong, Optional ByVal ReturnBigEndian As Boolean = False) As Byte()
        If Value > PowMax(40, False) OrElse Value < PowMin(40, False) Then Throw New OverflowException(New String("Value is to low or to high to be represented as an unsigned 40 bit integer. MaxValue = " & PowMax(40, False) & "MinValue = " & PowMin(40, False)))
        Dim Buffer As Byte() = BitConverter.GetBytes(CULng(Value))
        Buffer = New Byte() {Buffer(4), Buffer(3), Buffer(2), Buffer(1), Buffer(0)}
        If ReturnBigEndian Then Array.Reverse(Buffer)
        Return Buffer
    End Function

    Shared Function BytesToUInt48(ByVal Buffer As Byte(), Optional ByVal ReturnBigEndian As Boolean = False) As ULong
        If Buffer Is Nothing OrElse Buffer.Length <> 6 Then Throw New NullReferenceException("Input byte array is null or does not contain enough bytes to convert")
        Dim Value As ULong = CULng(Bytes2Int(Buffer, ReturnBigEndian))
        If Value > PowMax(48, False) OrElse Value < PowMin(48, False) Then Throw New OverflowException(New String("Value is to low or to high to be represented as an unsigned 48 bit integer. MaxValue = " & PowMax(48, False) & "MinValue = " & PowMin(48, False)))
        Return Value
    End Function

    Shared Function BytesToInt48(ByVal Buffer As Byte(), Optional ByVal ReturnBigEndian As Boolean = False) As Long
        If Buffer Is Nothing OrElse Buffer.Length <> 6 Then Throw New NullReferenceException("Input byte array is null or does not contain enough bytes to convert")
        Dim Value As Long = CLng(Bytes2Int(Buffer, ReturnBigEndian))
        If Value > PowMax(48) OrElse Value < PowMin(48) Then Throw New OverflowException(New String("Value is to low or to high to be represented as a signed 48 bit integer. MaxValue = " & PowMax(48) & "MinValue = " & PowMin(48)))
        Return Value
    End Function

    Shared Function Int48ToBytes(ByVal Value As Long, Optional ByVal ReturnBigEndian As Boolean = False) As Byte()
        If Value > PowMax(48) OrElse Value < PowMin(48) Then Throw New OverflowException(New String("Value is to low or to high to be represented as a signed 48 bit integer. MaxValue = " & PowMax(48) & "MinValue = " & PowMin(48)))
        Dim Buffer As Byte() = BitConverter.GetBytes(CLng(Value))
        Buffer = New Byte() {Buffer(5), Buffer(4), Buffer(3), Buffer(2), Buffer(1), Buffer(0)}
        If ReturnBigEndian Then Array.Reverse(Buffer)
        Return Buffer
    End Function

    Shared Function UInt48ToBytes(ByVal Value As ULong, Optional ByVal ReturnBigEndian As Boolean = False) As Byte()
        If Value > PowMax(48, False) OrElse Value < PowMin(48, False) Then Throw New OverflowException(New String("Value is to low or to high to be represented as an unsigned 48 bit integer. MaxValue = " & PowMax(48, False) & "MinValue = " & PowMin(48, False)))
        Dim Buffer As Byte() = BitConverter.GetBytes(CULng(Value))
        Buffer = New Byte() {Buffer(5), Buffer(4), Buffer(3), Buffer(2), Buffer(1), Buffer(0)}
        If ReturnBigEndian Then Array.Reverse(Buffer)
        Return Buffer
    End Function

    Shared Function BytesToUInt56(ByVal Buffer As Byte(), Optional ByVal ReturnBigEndian As Boolean = False) As ULong
        If Buffer Is Nothing OrElse Buffer.Length <> 7 Then Throw New NullReferenceException("Input byte array is null or does not contain enough bytes to convert")
        Dim Value As ULong = CULng(Bytes2Int(Buffer, ReturnBigEndian))
        If Value > PowMax(56, False) OrElse Value < PowMin(56, False) Then Throw New OverflowException(New String("Value is to low or to high to be represented as an unsigned 56 bit integer. MaxValue = " & PowMax(56, False) & "MinValue = " & PowMin(56, False)))
        Return Value
    End Function

    Shared Function BytesToInt56(ByVal Buffer As Byte(), Optional ByVal ReturnBigEndian As Boolean = False) As Long
        If Buffer Is Nothing OrElse Buffer.Length <> 7 Then Throw New NullReferenceException("Input byte array is null or does not contain enough bytes to convert")
        Dim Value As Long = CLng(Bytes2Int(Buffer, ReturnBigEndian))
        If Value > PowMax(56) OrElse Value < PowMin(56) Then Throw New OverflowException(New String("Value is to low or to high to be represented as a signed 56 bit integer. MaxValue = " & PowMax(56) & "MinValue = " & PowMin(56)))
        Return Value
    End Function

    Shared Function Int56ToBytes(ByVal Value As Long, Optional ByVal ReturnBigEndian As Boolean = False) As Byte()
        If Value > PowMax(56) OrElse Value < PowMin(56) Then Throw New OverflowException(New String("Value is to low or to high to be represented as a signed 56 bit integer. MaxValue = " & PowMax(56) & "MinValue = " & PowMin(56)))
        Dim Buffer As Byte() = BitConverter.GetBytes(CLng(Value))
        Buffer = New Byte() {Buffer(6), Buffer(5), Buffer(4), Buffer(3), Buffer(2), Buffer(1), Buffer(0)}
        If ReturnBigEndian Then Array.Reverse(Buffer)
        Return Buffer
    End Function

    Shared Function UInt56ToBytes(ByVal Value As ULong, Optional ByVal ReturnBigEndian As Boolean = False) As Byte()
        If Value > PowMax(56, False) OrElse Value < PowMin(56, False) Then Throw New OverflowException(New String("Value is to low or to high to be represented as an unsigned 56 bit integer. MaxValue = " & PowMax(56, False) & "MinValue = " & PowMin(56, False)))
        Dim Buffer As Byte() = BitConverter.GetBytes(CULng(Value))
        Buffer = New Byte() {Buffer(6), Buffer(5), Buffer(4), Buffer(3), Buffer(2), Buffer(1), Buffer(0)}
        If ReturnBigEndian Then Array.Reverse(Buffer)
        Return Buffer
    End Function

    Shared Function BytesToInt64(ByVal Bytes As Byte(), Optional ByVal ReturnBigEndian As Boolean = False) As Long
        If ReturnBigEndian Then Bytes = SwapSex(Bytes)
        Return BitConverter.ToInt64(Bytes, 0)
    End Function

    Shared Function Int64ToBytes(ByVal Value As Long, Optional ByVal ReturnBigEndian As Boolean = False) As Byte()
        Dim Buffer As Byte() = BitConverter.GetBytes(CLng(Value))
        If ReturnBigEndian Then Buffer = SwapSex(Buffer)
        Return Buffer
    End Function

    Shared Function BytesToUInt64(ByVal Bytes As Byte(), Optional ByVal ReturnBigEndian As Boolean = False) As ULong
        If ReturnBigEndian Then Bytes = SwapSex(Bytes)
        Return BitConverter.ToUInt64(Bytes, 0)
    End Function

    Shared Function UInt64ToBytes(ByVal Value As ULong, Optional ByVal ReturnBigEndian As Boolean = False) As Byte()
        Dim Buffer As Byte() = BitConverter.GetBytes(CULng(Value))
        If ReturnBigEndian Then Buffer = SwapSex(Buffer)
        Return Buffer
    End Function

    Shared Function BytesToSingle(ByVal Bytes As Byte(), Optional ByVal ReturnBigEndian As Boolean = False) As Single
        If ReturnBigEndian Then Bytes = SwapSex(Bytes)
        Return BitConverter.ToSingle(Bytes, 0)
    End Function

    Shared Function SingleToBytes(ByVal Value As Single, Optional ByVal ReturnBigEndian As Boolean = False) As Byte()
        Dim Buffer As Byte() = BitConverter.GetBytes(CSng(Value))
        If ReturnBigEndian Then Buffer = SwapSex(Buffer)
        Return Buffer
    End Function

    Shared Function BytesToDouble(ByVal Bytes As Byte(), Optional ByVal ReturnBigEndian As Boolean = False) As Double
        If ReturnBigEndian Then Bytes = SwapSex(Bytes)
        Return BitConverter.ToDouble(Bytes, 0)
    End Function

    Shared Function DoubleToBytes(ByVal Value As Double, Optional ByVal ReturnBigEndian As Boolean = False) As Byte()
        Dim Buffer As Byte() = BitConverter.GetBytes(CDbl(Value))
        If ReturnBigEndian Then Buffer = SwapSex(Buffer)
        Return Buffer
    End Function

    Private Shared Function Bytes2Int(ByVal Buffer As Byte(), Optional ByVal Big As Boolean = False) As Object
        If Big Then Array.Reverse(Buffer)
        Dim x As Object = 0
        For i As Int32 = 0 To Buffer.Length - 1
            x += (Buffer(i) * CLng(Math.Pow(256, i)))
        Next
        Return x
    End Function

    Shared Function BytesToImage(ByVal Bytes As Byte()) As Image
        Return Image.FromStream(New MemoryStream(Bytes))
    End Function

    Shared Function ImageToBytes(ByVal Picture As Image) As Byte()
        Dim Stream As New MemoryStream, Buffer(0) As Byte
        Picture.Save(Stream, Picture.RawFormat)
        Array.Resize(Of Byte)(Buffer, Stream.Length)
        StreamToBuffer(Stream, Buffer)
        Stream = Nothing
        Return Buffer
    End Function

    Shared Function Int32ToBitString(ByVal Value As Integer) As String
        Dim Binary As String = Nothing
        Do
            Binary += CStr(Value Mod 2)
            Value = (Value \ 2)
        Loop Until Value < 1
        Return Binary
    End Function

    Shared Function BitStringToInt32(ByVal Value As String) As Integer
        If Value Is Nothing OrElse Value.Length = 0 OrElse Not IsValidBitString(Value) Then Throw New Exception("Invalid bitstring")
        If Not Value.Length Mod 8 = 0 Then Throw New Exception("Bitstring must be divisible by 8")
        Dim Num As Long = 0
        For i As Integer = 0 To Len(Value) - 1
            Num += Val(Mid(Value, (Len(Value) - i) + 1, 1)) * 2 ^ (i - 1)
        Next
        Return Num
    End Function

    Shared Function BytesToBitString(ByVal Bytes As Byte()) As String
        Dim Binary As String = Nothing
        For i As Integer = 0 To Bytes.Length - 1
            Binary = Binary & System.Convert.ToString(Bytes(i), 2).PadLeft(8, "0")
        Next
        Return Binary
    End Function

    Shared Function BitStringToBytes(ByVal Value As String) As Byte()
        If Value Is Nothing OrElse Value.Length = 0 OrElse Not IsValidBitString(Value) Then Throw New Exception("Invalid bitstring")
        If Not Value.Length Mod 8 = 0 Then Throw New Exception("Bitstring must be divisible by 8")
        Dim Buffer(Value.Length / 8) As Byte
        For i As Integer = 0 To Value.Length / 8 - 1
            Buffer(i) = System.Convert.ToByte(Value.Substring(i * 8, 8), 2)
        Next
        Return Buffer
    End Function

    Shared Function BytesToBits(ByVal Bytes As Byte()) As BitArray
        If Bytes Is Nothing OrElse Bytes.Length = 0 Then Throw New Exception("Byte array is null")
        Return New BitArray(Bytes)
    End Function

    Shared Function BitsToBytes(ByVal Value As BitArray) As Byte()
        If Value Is Nothing OrElse Value.Count = 0 Then Throw New Exception("Bit array is null")
        Dim Buffer() As Byte = New Byte() {}
        Dim OddBits As Int32 = Value.Count Mod 8
        For i As Int32 = 0 To Value.Count \ 8 - 1
            Array.Resize(Buffer, Buffer.Length + 1)
            Value.CopyTo(Buffer, (i * 8))
        Next
        If OddBits <> 0 Then
            Array.Resize(Buffer, (Buffer.Length + 1))
            Value.CopyTo(Buffer, (Buffer.Length - 1))
        End If
        Return Buffer
    End Function

End Class

''' <summary>
''' A class of assorted functions for doing a variety things
''' </summary>
Public Class Functions

    Shared Function RemoveByteAt(ByVal Bytes As Byte(), ByVal Index As Integer) As Byte()
        Return HexToBytes(ObjectToHex(Bytes).Remove(Index, 2))
    End Function

    Shared Function RemoveByte(ByVal Bytes As Byte(), ByVal ByteToRemove As Byte) As Byte()
        Return HexToBytes(ObjectToHex(Bytes).Replace(ByteToRemove, ""))
    End Function

    Shared Sub FillBuffer(<[In](), Out()> ByRef Buffer As Byte(), Optional ByVal FillWith As Byte = 0)
        FillBuffer(Buffer, 0, FillWith)
    End Sub

    Shared Sub FillBuffer(<[In](), Out()> ByRef Buffer As Byte(), ByVal Index As ULong, Optional ByVal FillWith As Byte = 0)
        FillBuffer(Buffer, Index, Buffer.Length, FillWith)
    End Sub

    Shared Sub FillBuffer(<[In](), Out()> ByRef Buffer As Byte(), ByVal Index As ULong, ByVal Length As Long, Optional ByVal FillWith As Byte = 0)
        For i As Long = Index To Length - 1
            Buffer(i) = FillWith
        Next
    End Sub

    Shared Function DeleteBytes(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As Byte()
        Array.Copy(Buffer, (Index + Length), Buffer, Index, (Buffer.Length - (Index + Length)))
        Array.Resize(Of Byte)(Buffer, (Buffer.Length - Length))
        Return Buffer
    End Function

    Shared Function InsertBytes(ByRef Source As Byte(), ByVal SourceIndex As UInt32, ByVal Destination As Byte(), ByVal DestinationIndex As UInt32, ByVal Length As UInt32) As Byte()
        Array.Resize(Of Byte)(Destination, (Destination.Length + Length))
        Array.Copy(Destination, DestinationIndex, Destination, (DestinationIndex + Length), ((Destination.Length - DestinationIndex) - Length))
        Array.Copy(Source, SourceIndex, Destination, DestinationIndex, Length)
        Return Destination
    End Function

    Shared Function CompareBytes(ByRef Arg0 As Byte(), ByRef Arg1 As Byte()) As Boolean
        If Arg0 Is Nothing OrElse Arg1 Is Nothing Then Return False
        Return CompareBytes(Arg0, Arg1, Arg0.Length)
    End Function

    Shared Function CompareBytes(ByRef Arg0 As Byte(), ByRef Arg1 As Byte(), ByVal Length As Int32) As Boolean
        If Arg0 Is Nothing OrElse Arg1 Is Nothing Then Return False
        If Arg0.Length <> Arg1.Length Then Return False
        For i As Int32 = 0 To Length - 1
            If Arg0(i) <> Arg1(i) Then Return False
        Next
        Return True
    End Function

    Shared Function CompareStrings(ByRef Str0 As String, ByRef Str1 As String, Optional ByVal ComparisonType As StringComparison = 4) As Boolean
        If [String].Compare(Str0, Str1, ComparisonType) <> 0 Then Return False
        Return True
    End Function

    Shared Sub CombineArrays(ByRef TargetArray As Array, ByRef SourceArray As Array)
        CombineArrays(TargetArray, New Array() {SourceArray})
    End Sub

    Shared Sub CombineArrays(ByRef TargetArray As Array, ByVal ParamArray SourceArrays As Array())
        If SourceArrays Is Nothing OrElse SourceArrays.Length = 0 Then Return
        If TargetArray Is Nothing Then TargetArray = New Object()
        For i As Int32 = 0 To SourceArrays.Length - 1
            If SourceArrays(i) Is Nothing OrElse SourceArrays(i).Length = 0 Then GoTo 0
            Array.Resize(Of Object)(TargetArray, (TargetArray.Length + SourceArrays(i).Length))
            Array.Copy(SourceArrays(i), 0, TargetArray, (TargetArray.Length - SourceArrays(i).Length), SourceArrays(i).Length)
0:      Next
    End Sub

    Shared Function SwapSex(ByRef Buffer As Byte()) As Byte()
        Return ReverseArray(Buffer)
    End Function

    Shared Function ReverseArray(ByRef Buffer As Array) As Array
        Array.Reverse(Buffer)
        Return Buffer
    End Function

    Shared Sub StreamToBuffer(ByRef Stream As Stream, ByRef OutBuffer As Byte())
        If Stream Is Nothing OrElse Stream.Length = 0 OrElse Not Stream.CanRead Then Return
        If Not AvailableMemory() > Stream.Length Then Throw New OutOfMemoryException("Not enough memory available to copy the stream to an array")
        Array.Resize(Of Byte)(OutBuffer, Stream.Length)
        Dim Reader As New BinaryReader(Stream), LastPosition As Long = Stream.Position
        Reader.BaseStream.Position = 0
        For i As Long = 0 To Reader.BaseStream.Length - 1
            OutBuffer(i) = Reader.ReadByte
        Next
        Reader = Nothing
        Stream.Position = LastPosition
    End Sub

    Shared Function GetImageThumbnail(ByVal Picture As Image, ByVal Width As Integer, ByVal Height As Integer) As Image
        Return Picture.GetThumbnailImage(Width, Height, Image.GetThumbnailImageAbort.Combine, System.IntPtr.Zero)
    End Function

    Shared Function LenB(ByVal ObjStr As String) As Integer
        If Len(ObjStr) = 0 Then Return 0
        Return System.Text.Encoding.Unicode.GetByteCount(ObjStr)
    End Function

    Shared Function FileInfo(ByVal FilePath As String) As FileInfo
        Return New FileInfo(FilePath)
    End Function

    Shared Function FileLen(ByVal FilePath As String) As Integer
        Return FileSystem.FileLen(FilePath)
    End Function

    Shared Function IsValidHex(ByVal Hex As String) As Boolean
        Return New Regex("^[A-Fa-f0-9]*$", RegexOptions.IgnoreCase).IsMatch(Hex)
    End Function

    Shared Function IsValidUnicode(ByVal Unicode As String) As Boolean
        Return New Regex("^(\u0009|[\u0020-\u007E]|\u0085|[\u00A0-\uD7FF]|[\uE000-\uFFFD])+$", RegexOptions.IgnoreCase).IsMatch(Unicode)
    End Function

    Shared Function IsValidNumeric(ByVal Numeric As Long) As Boolean
        Return New Regex("^[0-9]+\d", RegexOptions.IgnoreCase).IsMatch(Numeric.ToString)
    End Function

    Shared Function IsValidASCII(ByVal [String] As String) As Boolean
        Return New Regex("^([\x00-\xff]*)$", RegexOptions.IgnoreCase).IsMatch([String])
    End Function

    Shared Function IsValidBitString(ByVal BitString As String) As Boolean
        Return New Regex("^[0-1]+\d").IsMatch(BitString)
    End Function

    Shared Function IsValidXboxString(ByVal Value As String) As Boolean
        If Value Is Nothing OrElse Value.Length = 0 Then Return False
        Value = Value.Replace(vbNullChar, "")
        Dim AllowedChars As String = "!#$%&'()-.@[]^_`{}~ abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        For Each x As Char In Value
            For i As Int32 = 0 To AllowedChars.Length - 1
                If Not AllowedChars.Contains(x) Then Return False
            Next
        Next
        Return True
    End Function

    Shared Function RoundBytes(ByVal ByteCount As Double) As String
        Return RoundBytes(ByteCount, 2)
    End Function

    Shared Function RoundBytes(ByVal ByteCount As Double, ByVal DecimalPlaces As Int32) As String
        If ByteCount < 1024 Then Return ByteCount & " bytes"
        ByteCount = Math.Round(CDec(ByteCount / 1024), DecimalPlaces)
        If ByteCount < 1024 Then Return ByteCount & " KB"
        ByteCount = Math.Round(CDec(ByteCount / 1024), DecimalPlaces)
        If ByteCount < 1024 Then Return ByteCount & " MB"
        ByteCount = Math.Round(CDec(ByteCount / 1024), DecimalPlaces)
        If ByteCount < 1024 Then Return ByteCount & " GB"
        ByteCount = Math.Round(CDec(ByteCount / 1024), DecimalPlaces)
        If ByteCount < 1024 Then Return ByteCount & " TB"
        ByteCount = Math.Round(CDec(ByteCount / 1024), DecimalPlaces)
        If ByteCount < 1024 Then Return ByteCount & " PB"
        ByteCount = Math.Round(CDec(ByteCount / 1024), DecimalPlaces)
        If ByteCount < 1024 Then Return ByteCount & " EB"
        ByteCount = Math.Round(CDec(ByteCount / 1024), DecimalPlaces)
        If ByteCount < 1024 Then Return ByteCount & " ZB"
        ByteCount = Math.Round(CDec(ByteCount / 1024), DecimalPlaces)
        If ByteCount < 1024 Then Return ByteCount & " YB"
        Throw New Exception("OMFGWTF!")
    End Function

    Shared Function GetRatio(ByVal Arg0 As Long, ByVal Arg1 As Long) As String
        Return Format((Arg0 / Arg1), "0.00")
    End Function

    Shared Function GetPercentage(ByVal SmallerNumber As Long, ByVal LargerNumber As Long, Optional ByVal DecimalPlaces As Byte = 0) As String
        Return New String(Math.Round(CDec((SmallerNumber / LargerNumber) * 100), DecimalPlaces) & "%")
    End Function

    Shared Function GetSmallerNumber(ByVal Arg0 As Long, ByVal Arg1 As Long) As Long
        If Arg0 > Arg1 Then Return Arg1
        Return Arg0
    End Function

    Shared Function GetLargerNumber(ByVal Arg0 As Long, ByVal Arg1 As Long) As Long
        If Arg0 > Arg1 Then Return Arg0
        Return Arg1
    End Function

    Shared Function ConvertImageFormat(ByVal Image As Image, ByVal Format As Imaging.ImageFormat) As Image
        Dim Stream As Stream = New MemoryStream
        Image.Save(Stream, Format)
        Image = Drawing.Image.FromStream(Stream)
        Stream.Close()
        Return Image
    End Function

    Shared Function GetFileListing(ByVal Directory As String, Optional ByVal ReturnSubDirectoriesAlso As Boolean = True) As String()
        Dim Listing As New List(Of String)
        If ReturnSubDirectoriesAlso Then
            For Each Dir As String In System.IO.Directory.GetDirectories(Directory)
                Listing.Add(Dir)
            Next
        End If
        For Each File As String In System.IO.Directory.GetFiles(Directory)
            Listing.Add(File)
        Next
        Return Listing.ToArray
    End Function

    Shared Function GetDirectoryListing(ByVal Path As String, Optional ByVal SearchOption As SearchOption = SearchOption.AllDirectories) As String()
        Dim Listing As New List(Of String)
        For Each Dir As String In Directory.GetDirectories(Path, "*.*", SearchOption)
            Listing.Add(Dir)
        Next
        Return Listing.ToArray
    End Function

    Shared Function AvailableMemory() As ULong
        Return CULng(New Devices.ComputerInfo().AvailablePhysicalMemory + New Devices.ComputerInfo().AvailableVirtualMemory)
    End Function

    ''' <summary>
    ''' Determines the minimum integer value that can be represented by the count of signed or unsigned bits
    ''' </summary>
    ''' <param name="Bits">Number of bits</param>
    ''' <param name="Signed">Whether to return the minimum of a signed or unsigned integer</param>
    ''' <returns>Returns the minimum integer value by count of signed or unsigned bits</returns>
    Shared Function PowMin(ByVal Bits As Int32, Optional ByVal Signed As Boolean = True) As Object
        If Not Signed Then Return 0
        Return CLng(Math.Round(-Math.Pow(2, Bits - 1), 1))
    End Function

    ''' <summary>
    ''' Determines the maximum integer value that can be represented by the count of signed or unsigned bits
    ''' </summary>
    ''' <param name="Bits">Number of bits</param>
    ''' <param name="Signed">Whether to return the maximum of a signed or unsigned integer</param>
    ''' <returns>Returns the maximum integer value by count of signed or unsigned bits</returns>
    Shared Function PowMax(ByVal Bits As Int32, Optional ByVal Signed As Boolean = True) As Object
        Dim x As Object = Math.Round((+Math.Pow(2, IIf(Signed, Bits - 1, Bits)) - 1), 1)
        If Signed Then Return CLng(x)
        Return CULng(x)
    End Function

End Class

''' <summary>
''' A class for quickly displaying various dialog prompts
''' </summary>
Public Class Dialogs

    Shared Function OpenDialog(Optional ByVal Title As String = "", Optional ByVal Filter As String = "All Files (*.*)|*.*", Optional ByVal InitialDirectory As String = "") As String
        Dim Open As New Windows.Forms.OpenFileDialog()
        Open.Title = Title
        Open.Filter = Filter
        Open.Multiselect = False
        Open.InitialDirectory = InitialDirectory
        If Open.ShowDialog = Windows.Forms.DialogResult.OK Then Return Open.FileName
        Return Nothing
    End Function

    Shared Function OpenMultiDialog(Optional ByVal Title As String = "", Optional ByVal Filter As String = "All Files (*.*)|*.*", Optional ByVal InitialDirectory As String = "") As String()
        Dim Open As New Windows.Forms.OpenFileDialog()
        Open.Title = Title
        Open.Filter = Filter
        Open.Multiselect = True
        Open.InitialDirectory = InitialDirectory
        If Open.ShowDialog = Windows.Forms.DialogResult.OK Then Return Open.FileNames
        Return Nothing
    End Function

    Shared Function SaveDialog(Optional ByVal Title As String = "", Optional ByVal Filter As String = "", Optional ByVal InitialDirectory As String = "", Optional ByVal ShowOverwritePrompt As Boolean = True, Optional ByVal AutoAddExtention As String = Nothing) As String
        Dim Save As New Windows.Forms.SaveFileDialog()
        Save.Title = Title
        Save.Filter = Filter
        Save.InitialDirectory = InitialDirectory
        Save.OverwritePrompt = ShowOverwritePrompt
        If Not AutoAddExtention = Nothing Then Save.AddExtension = True
        Save.DefaultExt = AutoAddExtention
        If Save.ShowDialog = Windows.Forms.DialogResult.OK Then Return Save.FileName
        Return Nothing
    End Function

    Shared Function BrowseDialog(Optional ByVal Description As String = "", Optional ByVal ShowNewFolderButton As Boolean = True, Optional ByVal RootFolder As Environment.SpecialFolder = Environment.SpecialFolder.Desktop)
        Dim Folder As New Windows.Forms.FolderBrowserDialog
        Folder.Description = Description : Folder.ShowNewFolderButton = ShowNewFolderButton : Folder.RootFolder = RootFolder
        If Folder.ShowDialog = Windows.Forms.DialogResult.OK Then Return Folder.SelectedPath
        Return Nothing
    End Function

End Class

Namespace Algorithms

    Public Class Adler32
        Private a As UInt32 = 1, b As UInt32

        Sub New()
            MyBase.New()
        End Sub

        ReadOnly Property GetHashBytes() As Byte()
            Get
                Return UInt32ToBytes(GetHashUInt32, True)
            End Get
        End Property

        ReadOnly Property GetHashUInt32() As UInt32
            Get
                Return CUInt((b << 16) Or a)
            End Get
        End Property

        Sub TransformBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32)
            For i As UInt32 = Index To Length - 1
                a = CUInt(((a + Buffer(i)) Mod &HFFF1))
                b = CUInt(((b + a) Mod &HFFF1))
            Next
        End Sub

        Sub TransformBlock(ByVal Buffer As Byte(), ByVal Length As UInt32)
            TransformBlock(Buffer, 0, Length)
        End Sub

        Sub TransformBlock(ByVal Buffer As Byte())
            TransformBlock(Buffer, 0, Buffer.Length)
        End Sub

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As UInt32
            TransformBlock(Buffer, Index, Length)
            Return GetHashUInt32
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Length As UInt32) As UInt32
            Return TransformFinalBlock(Buffer, 0, Length)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte()) As UInt32
            Return TransformFinalBlock(Buffer, 0, Buffer.Length)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As Byte()
            TransformBlock(Buffer, Index, Length)
            Return GetHashBytes
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Length As UInt32) As Byte()
            Return Compute(Buffer, 0, Length)
        End Function

        Function Compute(ByVal Buffer As Byte()) As Byte()
            Return Compute(Buffer, 0, Buffer.Length)
        End Function

        Sub Clear()
            a = 1
            b = 0
        End Sub

        Sub Dispose()
            GC.SuppressFinalize(Me)
        End Sub

    End Class

    Public Class CRC32
        Private Table() As UInt32 = Nothing
        Private Poly As UInt32 = &HEDB88320UI
        Private InitVal As UInt32 = &HFFFFFFFFUI
        Private Hash As UInt32 = 0

        Sub New()
            MyBase.New()
            Hash = InitVal
            GenerateTable()
        End Sub

        Sub New(ByVal Polynomial As UInt32, ByVal InitialValue As UInt32)
            MyBase.New()
            Me.Polynomial = Polynomial
            Me.InitialValue = InitialValue
            GenerateTable()
        End Sub

        Property Polynomial() As UInt32
            Get
                Return Poly
            End Get
            Set(ByVal value As UInt32)
                If value > UInt32.MaxValue OrElse value < UInt32.MinValue Then Throw New Exception("Value cannot be represented as UInt32")
                Poly = value
            End Set
        End Property

        Property InitialValue() As UInt32
            Get
                Return InitVal
            End Get
            Set(ByVal value As UInt32)
                If value > UInt32.MaxValue OrElse value < UInt32.MinValue Then Throw New Exception("Value cannot be represented as UInt32")
                InitVal = value
                Hash = InitVal
            End Set
        End Property

        ReadOnly Property GetHashBytes() As Byte()
            Get
                Return UInt32ToBytes(GetHashUInt32, True)
            End Get
        End Property

        ReadOnly Property GetHashUInt32() As UInt32
            Get
                Return (Not Hash)
            End Get
        End Property

        Private Sub GenerateTable()
            If Table Is Nothing OrElse Table.Length <> 256 Then Table = New UInt32(256 - 1) {}
            Dim Value As UInt32 = 0
            For u As UInt32 = 0 To Table.Length - 1
                Value = u
                For b As SByte = 8 To 1 Step -1
                    If (Value And 1) = 1 Then
                        Value = CUInt(((Value >> 1) Xor Polynomial))
                        GoTo [Next]
                    End If
                    Value >>= 1
[Next]:         Next
                Table(u) = Value
            Next
        End Sub

        Sub TransformBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32)
            If Table Is Nothing OrElse Table.Length <> 256 Then GenerateTable()
            For u As UInt32 = Index To Length - 1
                Hash = CUInt((Hash >> 8) Xor Table(CByte(((Hash) And &HFF) Xor Buffer(u))))
            Next
        End Sub

        Sub TransformBlock(ByVal Buffer As Byte(), ByVal Length As UInt32)
            TransformBlock(Buffer, 0, Length)
        End Sub

        Sub TransformBlock(ByVal Buffer As Byte())
            TransformBlock(Buffer, 0, Buffer.Length)
        End Sub

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As UInt32
            TransformBlock(Buffer, Index, Length)
            Return GetHashUInt32
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Length As UInt32) As UInt32
            Return TransformFinalBlock(Buffer, 0, Length)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte()) As UInt32
            Return TransformFinalBlock(Buffer, 0, Buffer.Length)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As Byte()
            TransformBlock(Buffer, Index, Length)
            Return GetHashBytes()
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Length As UInt32) As Byte()
            Return Compute(Buffer, 0, Length)
        End Function

        Function Compute(ByVal Buffer As Byte()) As Byte()
            Return Compute(Buffer, 0, Buffer.Length)
        End Function

        Sub Clear()
            Hash = 0
        End Sub

        Sub Dispose()
            GC.SuppressFinalize(Me)
        End Sub

    End Class

    Public Class BZIP2
        Private Table() As UInt32 = Nothing
        Private Poly As UInt32 = &H4C11DB7UI
        Private InitVal As UInt32 = &HFFFFFFFFUI
        Private Hash As UInt32 = 0

        Sub New()
            MyBase.New()
            Hash = InitVal
            GenerateTable()
        End Sub

        Sub New(ByVal Polynomial As UInt32, ByVal InitialValue As UInt32)
            MyBase.New()
            Me.Polynomial = Polynomial
            Me.InitialValue = InitialValue
            GenerateTable()
        End Sub

        Property Polynomial() As UInt32
            Get
                Return Poly
            End Get
            Set(ByVal value As UInt32)
                If value > UInt32.MaxValue OrElse value < UInt32.MinValue Then Throw New Exception("Value cannot be represented as UInt32")
                Poly = value
            End Set
        End Property

        Property InitialValue() As UInt32
            Get
                Return InitVal
            End Get
            Set(ByVal value As UInt32)
                If value > UInt32.MaxValue OrElse value < UInt32.MinValue Then Throw New Exception("Value cannot be represented as UInt32")
                InitVal = value
                Hash = InitVal
            End Set
        End Property

        ReadOnly Property GetHashBytes() As Byte()
            Get
                Return UInt32ToBytes(GetHashUInt32, True)
            End Get
        End Property

        ReadOnly Property GetHashUInt32() As UInt32
            Get
                Return (Not Hash)
            End Get
        End Property

        Private Sub GenerateTable()
            If Table Is Nothing OrElse Table.Length <> 256 Then Table = New UInt32(256 - 1) {}
            Dim Value As UInt32 = 0
            For u As UInt32 = 0 To Table.Length - 1
                Value = CUInt(u << &H18)
                For b As SByte = 0 To 8 - 1
                    If (Value And &H80000000UI) Then
                        Value = CUInt(((Value << 1) Xor Polynomial))
                        GoTo [Next]
                    End If
                    Value <<= 1
[Next]:         Next
                Table(u) = Value
            Next
        End Sub

        Sub TransformBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32)
            If Table Is Nothing OrElse Table.Length <> 256 Then GenerateTable()
            For u As UInt32 = Index To Length - 1
                Hash = CUInt((Hash << 8) Xor Table(CByte(((Hash >> &H18) And &HFF) Xor Buffer(u))))
            Next
        End Sub

        Sub TransformBlock(ByVal Buffer As Byte(), ByVal Length As UInt32)
            TransformBlock(Buffer, 0, Length)
        End Sub

        Sub TransformBlock(ByVal Buffer As Byte())
            TransformBlock(Buffer, 0, Buffer.Length)
        End Sub

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As UInt32
            TransformBlock(Buffer, Index, Length)
            Return GetHashUInt32
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Length As UInt32) As UInt32
            Return TransformFinalBlock(Buffer, 0, Length)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte()) As UInt32
            Return TransformFinalBlock(Buffer, 0, Buffer.Length)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As Byte()
            TransformBlock(Buffer, Index, Length)
            Return GetHashBytes()
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Length As UInt32) As Byte()
            Return Compute(Buffer, 0, Length)
        End Function

        Function Compute(ByVal Buffer As Byte()) As Byte()
            Return Compute(Buffer, 0, Buffer.Length)
        End Function

        Sub Clear()
            Hash = InitVal
        End Sub

        Sub Dispose()
            GC.SuppressFinalize(Me)
        End Sub

    End Class

    Public Class XOR8

        Sub New()
            MyBase.New()
        End Sub

        Function Compute(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As Byte
            Dim Result As UInt64 = 0
            For u As UInt32 = Index To Length - 1
                Result = ((Result + Buffer(u)) ^ Byte.MaxValue)
            Next
            Return System.Convert.ToByte(Result)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Length As UInt32) As Byte
            Return Compute(Buffer, 0, Length)
        End Function

        Function Compute(ByVal Buffer As Byte()) As Byte
            Return Compute(Buffer, 0, Buffer.Length)
        End Function

        Sub Dispose()
            GC.SuppressFinalize(Me)
        End Sub

    End Class

    Public Class XOR16

        Sub New()
            MyBase.New()
        End Sub

        Function Compute(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As UShort
            Dim Result As UInt64 = 0
            For u As UInt32 = Index To Length - 1
                Result = ((Result + Buffer(u)) ^ UInt16.MaxValue)
            Next
            Return System.Convert.ToUInt16(Result)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Length As UInt32) As UShort
            Return Compute(Buffer, 0, Length)
        End Function

        Function Compute(ByVal Buffer As Byte()) As UShort
            Return Compute(Buffer, 0, Buffer.Length)
        End Function

        Sub Dispose()
            GC.SuppressFinalize(Me)
        End Sub

    End Class

    Public Class XOR24
        Private MaxValue As UInt32 = &HFFFFFFUI

        Sub New()
            MyBase.New()
        End Sub

        Function Compute(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As UInteger
            Dim Result As UInt64 = 0
            For u As UInt32 = Index To Length - 1
                Result = ((Result + Buffer(u)) ^ MaxValue)
            Next
            Return System.Convert.ToUInt32(Result)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Length As UInt32) As UInteger
            Return Compute(Buffer, 0, Length)
        End Function

        Function Compute(ByVal Buffer As Byte()) As UInteger
            Return Compute(Buffer, 0, Buffer.Length)
        End Function

        Sub Dispose()
            GC.SuppressFinalize(Me)
        End Sub

    End Class

    Public Class XOR32

        Sub New()
            MyBase.New()
        End Sub

        Function Compute(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As UInteger
            Dim Result As UInt64 = 0
            For u As UInt32 = Index To Length - 1
                Result = ((Result + Buffer(u)) ^ UInt32.MaxValue)
            Next
            Return System.Convert.ToUInt32(Result)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Length As UInt32) As UInteger
            Return Compute(Buffer, 0, Length)
        End Function

        Function Compute(ByVal Buffer As Byte()) As UInteger
            Return Compute(Buffer, 0, Buffer.Length)
        End Function

        Sub Dispose()
            GC.SuppressFinalize(Me)
        End Sub

    End Class

    Public Class XOR64

        Sub New()
            MyBase.New()
        End Sub

        Function Compute(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As ULong
            Dim Result As UInt64 = 0
            For u As UInt32 = Index To Length - 1
                Result = ((Result + Buffer(u)) ^ UInt64.MaxValue)
            Next
            Return System.Convert.ToUInt64(Result)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Length As UInt32) As ULong
            Return Compute(Buffer, 0, Length)
        End Function

        Function Compute(ByVal Buffer As Byte()) As ULong
            Return Compute(Buffer, 0, Buffer.Length)
        End Function

        Sub Dispose()
            GC.SuppressFinalize(Me)
        End Sub

    End Class

    Public Class Checksum8

        Sub New()
            MyBase.New()
        End Sub

        Function Compute(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As Byte
            Dim Result As UInt64 = 0
            For u As UInt32 = Index To Length - 1
                Result = ((Result + Buffer(u)) And Byte.MaxValue)
            Next
            Return System.Convert.ToByte(Result)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Length As UInt32) As Byte
            Return Compute(Buffer, 0, Length)
        End Function

        Function Compute(ByVal Buffer As Byte()) As Byte
            Return Compute(Buffer, 0, Buffer.Length)
        End Function

        Sub Dispose()
            GC.SuppressFinalize(Me)
        End Sub

    End Class

    Public Class Checksum16

        Sub New()
            MyBase.New()
        End Sub

        Function Compute(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As UShort
            Dim Result As UInt64 = 0
            For u As UInt32 = Index To Length - 1
                Result = ((Result + Buffer(u)) And UInt16.MaxValue)
            Next
            Return System.Convert.ToUInt16(Result)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Length As UInt32) As UShort
            Return Compute(Buffer, 0, Length)
        End Function

        Function Compute(ByVal Buffer As Byte()) As UShort
            Return Compute(Buffer, 0, Buffer.Length)
        End Function

        Sub Dispose()
            GC.SuppressFinalize(Me)
        End Sub

    End Class

    Public Class Checksum24
        Private MaxValue As UInt32 = &HFFFFFFUI

        Sub New()
            MyBase.New()
        End Sub

        Function Compute(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As UInteger
            Dim Result As UInt64 = 0
            For u As UInt32 = Index To Length - 1
                Result = ((Result + Buffer(u)) And MaxValue)
            Next
            Return System.Convert.ToUInt32(Result)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Length As UInt32) As UInteger
            Return Compute(Buffer, 0, Length)
        End Function

        Function Compute(ByVal Buffer As Byte()) As UInteger
            Return Compute(Buffer, 0, Buffer.Length)
        End Function

        Sub Dispose()
            GC.SuppressFinalize(Me)
        End Sub

    End Class

    Public Class Checksum32

        Sub New()
            MyBase.New()
        End Sub

        Function Compute(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As UInteger
            Dim Result As UInt64 = 0
            For u As UInt32 = Index To Length - 1
                Result = ((Result + Buffer(u)) And UInt32.MaxValue)
            Next
            Return System.Convert.ToUInt32(Result)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Length As UInt32) As UInteger
            Return Compute(Buffer, 0, Length)
        End Function

        Function Compute(ByVal Buffer As Byte()) As UInteger
            Return Compute(Buffer, 0, Buffer.Length)
        End Function

        Sub Dispose()
            GC.SuppressFinalize(Me)
        End Sub

    End Class

    Public Class Checksum64

        Sub New()
            MyBase.New()
        End Sub

        Function Compute(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As ULong
            Dim Result As UInt64 = 0
            For u As UInt32 = Index To Length - 1
                Result = ((Result + Buffer(u)) And UInt64.MaxValue)
            Next
            Return System.Convert.ToUInt64(Result)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Length As UInt32) As ULong
            Return Compute(Buffer, 0, Length)
        End Function

        Function Compute(ByVal Buffer As Byte()) As ULong
            Return Compute(Buffer, 0, Buffer.Length)
        End Function

        Sub Dispose()
            GC.SuppressFinalize(Me)
        End Sub

    End Class

    Public Class MD5
        Private MD5 As MD5CryptoServiceProvider
        Private Hash As Byte()

        Sub New()
            MyBase.New()
            MD5 = New MD5CryptoServiceProvider
            Hash = New Byte(16 - 1) {}
        End Sub

        ReadOnly Property GetHashBytes() As Byte()
            Get
                Return Hash
            End Get
        End Property

        Sub TransformBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32)
            MD5.TransformBlock(Buffer, Index, Length, Hash, 0)
        End Sub

        Sub TransformBlock(ByVal Buffer As Byte(), ByVal Length As UInt32)
            TransformBlock(Buffer, 0, Length)
        End Sub

        Sub TransformBlock(ByVal Buffer As Byte())
            TransformBlock(Buffer, 0, Buffer.Length)
        End Sub

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As Byte()
            Return MD5.TransformFinalBlock(Buffer, Index, Length)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Length As UInt32) As Byte()
            Return TransformFinalBlock(Buffer, 0, Length)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte()) As Byte()
            Return TransformFinalBlock(Buffer, 0, Buffer.Length)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As Byte()
            Return MD5.ComputeHash(Buffer, Index, Length)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Length As UInt32) As Byte()
            Return Compute(Buffer, 0, Length)
        End Function

        Function Compute(ByVal Buffer As Byte()) As Byte()
            Return Compute(Buffer, 0, Buffer.Length)
        End Function

        Sub Clear()
            MD5 = New MD5CryptoServiceProvider
            Hash = New Byte(16 - 1) {}
        End Sub

        Sub Dispose()
            GC.SuppressFinalize(Me)
        End Sub

    End Class

    Public Class SHA1
        Private SHA As SHA1CryptoServiceProvider
        Private Hash As Byte()

        Sub New()
            MyBase.New()
            SHA = New SHA1CryptoServiceProvider
            Hash = New Byte(20 - 1) {}
        End Sub

        ReadOnly Property GetHashBytes() As Byte()
            Get
                Return Hash
            End Get
        End Property

        Sub TransformBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32)
            SHA.TransformBlock(Buffer, Index, Length, Hash, 0)
        End Sub

        Sub TransformBlock(ByVal Buffer As Byte(), ByVal Length As UInt32)
            TransformBlock(Buffer, 0, Length)
        End Sub

        Sub TransformBlock(ByVal Buffer As Byte())
            TransformBlock(Buffer, 0, Buffer.Length)
        End Sub

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As Byte()
            Return SHA.TransformFinalBlock(Buffer, Index, Length)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Length As UInt32) As Byte()
            Return TransformFinalBlock(Buffer, 0, Length)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte()) As Byte()
            Return TransformFinalBlock(Buffer, 0, Buffer.Length)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As Byte()
            Return SHA.ComputeHash(Buffer, Index, Length)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Length As UInt32) As Byte()
            Return Compute(Buffer, 0, Length)
        End Function

        Function Compute(ByVal Buffer As Byte()) As Byte()
            Return Compute(Buffer, 0, Buffer.Length)
        End Function

        Sub Clear()
            SHA = New SHA1CryptoServiceProvider
            Hash = New Byte(20 - 1) {}
        End Sub

        Sub Dispose()
            GC.SuppressFinalize(Me)
        End Sub

    End Class

    Public Class SHA2
        Private SHA As SHA256CryptoServiceProvider
        Private Hash As Byte()

        Sub New()
            MyBase.New()
            SHA = New SHA256CryptoServiceProvider
            Hash = New Byte(32 - 1) {}
        End Sub

        ReadOnly Property GetHashBytes() As Byte()
            Get
                Return Hash
            End Get
        End Property

        Sub TransformBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32)
            SHA.TransformBlock(Buffer, Index, Length, Hash, 0)
        End Sub

        Sub TransformBlock(ByVal Buffer As Byte(), ByVal Length As UInt32)
            TransformBlock(Buffer, 0, Length)
        End Sub

        Sub TransformBlock(ByVal Buffer As Byte())
            TransformBlock(Buffer, 0, Buffer.Length)
        End Sub

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As Byte()
            Return SHA.TransformFinalBlock(Buffer, Index, Length)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Length As UInt32) As Byte()
            Return TransformFinalBlock(Buffer, 0, Length)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte()) As Byte()
            Return TransformFinalBlock(Buffer, 0, Buffer.Length)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As Byte()
            Return SHA.ComputeHash(Buffer, Index, Length)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Length As UInt32) As Byte()
            Return Compute(Buffer, 0, Length)
        End Function

        Function Compute(ByVal Buffer As Byte()) As Byte()
            Return Compute(Buffer, 0, Buffer.Length)
        End Function

        Sub Clear()
            SHA = New SHA256CryptoServiceProvider
            Hash = New Byte(32 - 1) {}
        End Sub

        Sub Dispose()
            GC.SuppressFinalize(Me)
        End Sub

    End Class

    Public Class SHA3
        Private SHA As SHA384CryptoServiceProvider
        Private Hash As Byte()

        Sub New()
            MyBase.New()
            SHA = New SHA384CryptoServiceProvider
            Hash = New Byte(48 - 1) {}
        End Sub

        ReadOnly Property GetHashBytes() As Byte()
            Get
                Return Hash
            End Get
        End Property

        Sub TransformBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32)
            SHA.TransformBlock(Buffer, Index, Length, Hash, 0)
        End Sub

        Sub TransformBlock(ByVal Buffer As Byte(), ByVal Length As UInt32)
            TransformBlock(Buffer, 0, Length)
        End Sub

        Sub TransformBlock(ByVal Buffer As Byte())
            TransformBlock(Buffer, 0, Buffer.Length)
        End Sub

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As Byte()
            Return SHA.TransformFinalBlock(Buffer, Index, Length)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Length As UInt32) As Byte()
            Return TransformFinalBlock(Buffer, 0, Length)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte()) As Byte()
            Return TransformFinalBlock(Buffer, 0, Buffer.Length)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As Byte()
            Return SHA.ComputeHash(Buffer, Index, Length)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Length As UInt32) As Byte()
            Return Compute(Buffer, 0, Length)
        End Function

        Function Compute(ByVal Buffer As Byte()) As Byte()
            Return Compute(Buffer, 0, Buffer.Length)
        End Function

        Sub Clear()
            SHA = New SHA384CryptoServiceProvider
            Hash = New Byte(48 - 1) {}
        End Sub

        Sub Dispose()
            GC.SuppressFinalize(Me)
        End Sub

    End Class

    Public Class SHA5
        Private SHA As SHA512CryptoServiceProvider
        Private Hash As Byte()

        Sub New()
            MyBase.New()
            SHA = New SHA512CryptoServiceProvider
            Hash = New Byte(64 - 1) {}
        End Sub

        ReadOnly Property GetHashBytes() As Byte()
            Get
                Return Hash
            End Get
        End Property

        Sub TransformBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32)
            SHA.TransformBlock(Buffer, Index, Length, Hash, 0)
        End Sub

        Sub TransformBlock(ByVal Buffer As Byte(), ByVal Length As UInt32)
            TransformBlock(Buffer, 0, Length)
        End Sub

        Sub TransformBlock(ByVal Buffer As Byte())
            TransformBlock(Buffer, 0, Buffer.Length)
        End Sub

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As Byte()
            Return SHA.TransformFinalBlock(Buffer, Index, Length)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Length As UInt32) As Byte()
            Return TransformFinalBlock(Buffer, 0, Length)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte()) As Byte()
            Return TransformFinalBlock(Buffer, 0, Buffer.Length)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As Byte()
            Return SHA.ComputeHash(Buffer, Index, Length)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Length As UInt32) As Byte()
            Return Compute(Buffer, 0, Length)
        End Function

        Function Compute(ByVal Buffer As Byte()) As Byte()
            Return Compute(Buffer, 0, Buffer.Length)
        End Function

        Sub Clear()
            SHA = New SHA512CryptoServiceProvider
            Hash = New Byte(64 - 1) {}
        End Sub

        Sub Dispose()
            GC.SuppressFinalize(Me)
        End Sub

    End Class

    Public Class RIPEMD160
        Private RIPE As System.Security.Cryptography.RIPEMD160Managed
        Private Hash As Byte()

        Sub New()
            MyBase.New()
            RIPE = New System.Security.Cryptography.RIPEMD160Managed
            Hash = New Byte(20 - 1) {}
        End Sub

        ReadOnly Property GetHashBytes() As Byte()
            Get
                Return Hash
            End Get
        End Property

        Sub TransformBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32)
            RIPE.TransformBlock(Buffer, Index, Length, Hash, 0)
        End Sub

        Sub TransformBlock(ByVal Buffer As Byte(), ByVal Length As UInt32)
            TransformBlock(Buffer, 0, Length)
        End Sub

        Sub TransformBlock(ByVal Buffer As Byte())
            TransformBlock(Buffer, 0, Buffer.Length)
        End Sub

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As Byte()
            Return RIPE.TransformFinalBlock(Buffer, Index, Length)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Length As UInt32) As Byte()
            Return TransformFinalBlock(Buffer, 0, Length)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte()) As Byte()
            Return TransformFinalBlock(Buffer, 0, Buffer.Length)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As Byte()
            Return RIPE.ComputeHash(Buffer, Index, Length)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Length As UInt32) As Byte()
            Return Compute(Buffer, 0, Length)
        End Function

        Function Compute(ByVal Buffer As Byte()) As Byte()
            Return Compute(Buffer, 0, Buffer.Length)
        End Function

        Sub Clear()
            RIPE = New System.Security.Cryptography.RIPEMD160Managed
            Hash = New Byte(20 - 1) {}
        End Sub

        Sub Dispose()
            GC.SuppressFinalize(Me)
        End Sub

    End Class

    Public Class MACTripleDES
        Private MAC As System.Security.Cryptography.MACTripleDES
        Private Hash As Byte()

        Sub New()
            MyBase.New()
            MAC = New System.Security.Cryptography.MACTripleDES()
            Hash = New Byte(8 - 1) {}
        End Sub

        Sub New(ByVal Key As Byte())
            Me.New()
            Me.Key = Key
        End Sub

        Private Function ValidKeySize(ByVal Key As Byte()) As Boolean
            If Key Is Nothing Then Return False
            Dim Len As UInt32 = Key.Length
            If Len <> 8 OrElse Len <> 16 OrElse Len <> 24 Then Return False
            Return True
        End Function

        Property Key() As Byte()
            Get
                Return MAC.Key
            End Get
            Set(ByVal value As Byte())
                If Not ValidKeySize(value) Then Throw New CryptographicException("Key must be 8, 16 or 24 bytes in length")
                MAC.Key = value
            End Set
        End Property

        ReadOnly Property GetHashBytes() As Byte()
            Get
                Return Hash
            End Get
        End Property

        Sub TransformBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32)
            MAC.TransformBlock(Buffer, Index, Length, Hash, 0)
        End Sub

        Sub TransformBlock(ByVal Buffer As Byte(), ByVal Length As UInt32)
            TransformBlock(Buffer, 0, Length)
        End Sub

        Sub TransformBlock(ByVal Buffer As Byte())
            TransformBlock(Buffer, 0, Buffer.Length)
        End Sub

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As Byte()
            Return MAC.TransformFinalBlock(Buffer, Index, Length)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Length As UInt32) As Byte()
            Return TransformFinalBlock(Buffer, 0, Length)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte()) As Byte()
            Return TransformFinalBlock(Buffer, 0, Buffer.Length)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As Byte()
            Return MAC.ComputeHash(Buffer, Index, Length)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Length As UInt32) As Byte()
            Return Compute(Buffer, 0, Length)
        End Function

        Function Compute(ByVal Buffer As Byte()) As Byte()
            Return Compute(Buffer, 0, Buffer.Length)
        End Function

        Sub Clear()
            MAC = New System.Security.Cryptography.MACTripleDES(Key)
            Hash = New Byte(8 - 1) {}
        End Sub

        Sub Dispose()
            GC.SuppressFinalize(Me)
        End Sub

    End Class

    Public Class HMACMD5
        Private HMAC As System.Security.Cryptography.HMACMD5
        Private Hash As Byte()

        Sub New()
            MyBase.New()
            HMAC = New System.Security.Cryptography.HMACMD5
            Hash = New Byte(16 - 1) {}
        End Sub

        Sub New(ByVal Key As Byte())
            Me.New()
            Me.Key = Key
        End Sub

        ReadOnly Property MinimumKeySize() As Int32
            Get
                Return 16
            End Get
        End Property

        Property Key() As Byte()
            Get
                Return HMAC.Key
            End Get
            Set(ByVal value As Byte())
                If value Is Nothing OrElse value.Length < MinimumKeySize Then Throw New CryptographicException("Key must not be null and 16 bytes in length or greater")
                HMAC.Key = value
            End Set
        End Property

        ReadOnly Property GetHashBytes() As Byte()
            Get
                Return Hash
            End Get
        End Property

        Sub TransformBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32)
            HMAC.TransformBlock(Buffer, Index, Length, Hash, 0)
        End Sub

        Sub TransformBlock(ByVal Buffer As Byte(), ByVal Length As UInt32)
            TransformBlock(Buffer, 0, Length)
        End Sub

        Sub TransformBlock(ByVal Buffer As Byte())
            TransformBlock(Buffer, 0, Buffer.Length)
        End Sub

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As Byte()
            Return HMAC.TransformFinalBlock(Buffer, Index, Length)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Length As UInt32) As Byte()
            Return TransformFinalBlock(Buffer, 0, Length)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte()) As Byte()
            Return TransformFinalBlock(Buffer, 0, Buffer.Length)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As Byte()
            Return HMAC.ComputeHash(Buffer, Index, Length)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Length As UInt32) As Byte()
            Return Compute(Buffer, 0, Length)
        End Function

        Function Compute(ByVal Buffer As Byte()) As Byte()
            Return Compute(Buffer, Buffer.Length)
        End Function

        Sub Clear()
            HMAC = New System.Security.Cryptography.HMACMD5(Key)
            Hash = New Byte(16 - 1) {}
        End Sub

        Sub Dispose()
            GC.SuppressFinalize(Me)
        End Sub

    End Class

    Public Class HMACSHA1
        Private HMAC As System.Security.Cryptography.HMACSHA1
        Private Hash As Byte()

        Sub New()
            MyBase.New()
            HMAC = New System.Security.Cryptography.HMACSHA1
            Hash = New Byte(20 - 1) {}
        End Sub

        Sub New(ByVal Key As Byte())
            Me.New()
            Me.Key = Key
        End Sub

        ReadOnly Property MinimumKeySize() As Int32
            Get
                Return 20
            End Get
        End Property

        Property Key() As Byte()
            Get
                Return HMAC.Key
            End Get
            Set(ByVal value As Byte())
                If value Is Nothing OrElse value.Length < MinimumKeySize Then Throw New CryptographicException("Key must not be null and 20 bytes in length or greater")
                HMAC.Key = value
            End Set
        End Property

        ReadOnly Property GetHashBytes() As Byte()
            Get
                Return Hash
            End Get
        End Property

        Sub TransformBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32)
            HMAC.TransformBlock(Buffer, Index, Length, Hash, 0)
        End Sub

        Sub TransformBlock(ByVal Buffer As Byte(), ByVal Length As UInt32)
            TransformBlock(Buffer, 0, Length)
        End Sub

        Sub TransformBlock(ByVal Buffer As Byte())
            TransformBlock(Buffer, 0, Buffer.Length)
        End Sub

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As Byte()
            Return HMAC.TransformFinalBlock(Buffer, Index, Length)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Length As UInt32) As Byte()
            Return TransformFinalBlock(Buffer, 0, Length)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte()) As Byte()
            Return TransformFinalBlock(Buffer, 0, Buffer.Length)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As Byte()
            Return HMAC.ComputeHash(Buffer, Index, Length)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Length As UInt32) As Byte()
            Return Compute(Buffer, 0, Length)
        End Function

        Function Compute(ByVal Buffer As Byte()) As Byte()
            Return Compute(Buffer, Buffer.Length)
        End Function

        Sub Clear()
            HMAC = New System.Security.Cryptography.HMACSHA1(Key)
            Hash = New Byte(20 - 1) {}
        End Sub

        Sub Dispose()
            GC.SuppressFinalize(Me)
        End Sub

    End Class

    Public Class HMACSHA2
        Private HMAC As System.Security.Cryptography.HMACSHA256
        Private Hash As Byte()

        Sub New()
            MyBase.New()
            HMAC = New System.Security.Cryptography.HMACSHA256
            Hash = New Byte(32 - 1) {}
        End Sub

        Sub New(ByVal Key As Byte())
            Me.New()
            Me.Key = Key
        End Sub

        ReadOnly Property MinimumKeySize() As Int32
            Get
                Return 32
            End Get
        End Property

        Property Key() As Byte()
            Get
                Return HMAC.Key
            End Get
            Set(ByVal value As Byte())
                If value Is Nothing OrElse value.Length < MinimumKeySize Then Throw New CryptographicException("Key must not be null and 32 bytes in length or greater")
                HMAC.Key = value
            End Set
        End Property

        ReadOnly Property GetHashBytes() As Byte()
            Get
                Return Hash
            End Get
        End Property

        Sub TransformBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32)
            HMAC.TransformBlock(Buffer, Index, Length, Hash, 0)
        End Sub

        Sub TransformBlock(ByVal Buffer As Byte(), ByVal Length As UInt32)
            TransformBlock(Buffer, 0, Length)
        End Sub

        Sub TransformBlock(ByVal Buffer As Byte())
            TransformBlock(Buffer, 0, Buffer.Length)
        End Sub

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As Byte()
            Return HMAC.TransformFinalBlock(Buffer, Index, Length)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Length As UInt32) As Byte()
            Return TransformFinalBlock(Buffer, 0, Length)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte()) As Byte()
            Return TransformFinalBlock(Buffer, 0, Buffer.Length)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As Byte()
            Return HMAC.ComputeHash(Buffer, Index, Length)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Length As UInt32) As Byte()
            Return Compute(Buffer, 0, Length)
        End Function

        Function Compute(ByVal Buffer As Byte()) As Byte()
            Return Compute(Buffer, Buffer.Length)
        End Function

        Sub Clear()
            HMAC = New System.Security.Cryptography.HMACSHA256(Key)
            Hash = New Byte(32 - 1) {}
        End Sub

        Sub Dispose()
            GC.SuppressFinalize(Me)
        End Sub

    End Class

    Public Class HMACSHA3
        Private HMAC As System.Security.Cryptography.HMACSHA384
        Private Hash As Byte()

        Sub New()
            MyBase.New()
            HMAC = New System.Security.Cryptography.HMACSHA384
            Hash = New Byte(48 - 1) {}
        End Sub

        Sub New(ByVal Key As Byte())
            Me.New()
            Me.Key = Key
        End Sub

        ReadOnly Property MinimumKeySize() As Int32
            Get
                Return 48
            End Get
        End Property

        Property Key() As Byte()
            Get
                Return HMAC.Key
            End Get
            Set(ByVal value As Byte())
                If value Is Nothing OrElse value.Length < MinimumKeySize Then Throw New CryptographicException("Key must not be null and 48 bytes in length or greater")
                HMAC.Key = value
            End Set
        End Property

        ReadOnly Property GetHashBytes() As Byte()
            Get
                Return Hash
            End Get
        End Property

        Sub TransformBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32)
            HMAC.TransformBlock(Buffer, Index, Length, Hash, 0)
        End Sub

        Sub TransformBlock(ByVal Buffer As Byte(), ByVal Length As UInt32)
            TransformBlock(Buffer, 0, Length)
        End Sub

        Sub TransformBlock(ByVal Buffer As Byte())
            TransformBlock(Buffer, 0, Buffer.Length)
        End Sub

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As Byte()
            Return HMAC.TransformFinalBlock(Buffer, Index, Length)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Length As UInt32) As Byte()
            Return TransformFinalBlock(Buffer, 0, Length)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte()) As Byte()
            Return TransformFinalBlock(Buffer, 0, Buffer.Length)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As Byte()
            Return HMAC.ComputeHash(Buffer, Index, Length)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Length As UInt32) As Byte()
            Return Compute(Buffer, 0, Length)
        End Function

        Function Compute(ByVal Buffer As Byte()) As Byte()
            Return Compute(Buffer, Buffer.Length)
        End Function

        Sub Clear()
            HMAC = New System.Security.Cryptography.HMACSHA384(Key)
            Hash = New Byte(48 - 1) {}
        End Sub

        Sub Dispose()
            GC.SuppressFinalize(Me)
        End Sub

    End Class

    Public Class HMACSHA5
        Private HMAC As System.Security.Cryptography.HMACSHA512
        Private Hash As Byte()

        Sub New()
            MyBase.New()
            HMAC = New System.Security.Cryptography.HMACSHA512
            Hash = New Byte(64 - 1) {}
        End Sub

        Sub New(ByVal Key As Byte())
            Me.New()
            Me.Key = Key
        End Sub

        ReadOnly Property MinimumKeySize() As Int32
            Get
                Return 64
            End Get
        End Property

        Property Key() As Byte()
            Get
                Return HMAC.Key
            End Get
            Set(ByVal value As Byte())
                If value Is Nothing OrElse value.Length < MinimumKeySize Then Throw New CryptographicException("Key must not be null and 64 bytes in length or greater")
                HMAC.Key = value
            End Set
        End Property

        ReadOnly Property GetHashBytes() As Byte()
            Get
                Return Hash
            End Get
        End Property

        Sub TransformBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32)
            HMAC.TransformBlock(Buffer, Index, Length, Hash, 0)
        End Sub

        Sub TransformBlock(ByVal Buffer As Byte(), ByVal Length As UInt32)
            TransformBlock(Buffer, 0, Length)
        End Sub

        Sub TransformBlock(ByVal Buffer As Byte())
            TransformBlock(Buffer, 0, Buffer.Length)
        End Sub

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As Byte()
            Return HMAC.TransformFinalBlock(Buffer, Index, Length)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Length As UInt32) As Byte()
            Return TransformFinalBlock(Buffer, 0, Length)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte()) As Byte()
            Return TransformFinalBlock(Buffer, 0, Buffer.Length)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As Byte()
            Return HMAC.ComputeHash(Buffer, Index, Length)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Length As UInt32) As Byte()
            Return Compute(Buffer, 0, Length)
        End Function

        Function Compute(ByVal Buffer As Byte()) As Byte()
            Return Compute(Buffer, Buffer.Length)
        End Function

        Sub Clear()
            HMAC = New System.Security.Cryptography.HMACSHA512(Key)
            Hash = New Byte(64 - 1) {}
        End Sub

        Sub Dispose()
            GC.SuppressFinalize(Me)
        End Sub

    End Class

    Public Class HMACRIPEMD160
        Private HMAC As System.Security.Cryptography.HMACRIPEMD160
        Private Hash As Byte()

        Sub New()
            MyBase.New()
            HMAC = New System.Security.Cryptography.HMACRIPEMD160
            Hash = New Byte(20 - 1) {}
        End Sub

        Sub New(ByVal Key As Byte())
            Me.New()
            Me.Key = Key
        End Sub

        ReadOnly Property MinimumKeySize() As Int32
            Get
                Return 20
            End Get
        End Property

        Property Key() As Byte()
            Get
                Return HMAC.Key
            End Get
            Set(ByVal value As Byte())
                If value Is Nothing OrElse value.Length < MinimumKeySize Then Throw New CryptographicException("Key must not be null and 20 bytes in length or greater")
                HMAC.Key = value
            End Set
        End Property

        ReadOnly Property GetHashBytes() As Byte()
            Get
                Return Hash
            End Get
        End Property

        Sub TransformBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32)
            HMAC.TransformBlock(Buffer, Index, Length, Hash, 0)
        End Sub

        Sub TransformBlock(ByVal Buffer As Byte(), ByVal Length As UInt32)
            TransformBlock(Buffer, 0, Length)
        End Sub

        Sub TransformBlock(ByVal Buffer As Byte())
            TransformBlock(Buffer, 0, Buffer.Length)
        End Sub

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As Byte()
            Return HMAC.TransformFinalBlock(Buffer, Index, Length)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Length As UInt32) As Byte()
            Return TransformFinalBlock(Buffer, 0, Length)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte()) As Byte()
            Return TransformFinalBlock(Buffer, 0, Buffer.Length)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As Byte()
            Return HMAC.ComputeHash(Buffer, Index, Length)
        End Function

        Function Compute(ByVal Buffer As Byte(), ByVal Length As UInt32) As Byte()
            Return Compute(Buffer, 0, Length)
        End Function

        Function Compute(ByVal Buffer As Byte()) As Byte()
            Return Compute(Buffer, Buffer.Length)
        End Function

        Sub Clear()
            HMAC = New System.Security.Cryptography.HMACRIPEMD160(Key)
            Hash = New Byte(20 - 1) {}
        End Sub

        Sub Dispose()
            GC.SuppressFinalize(Me)
        End Sub
    End Class

End Namespace

Namespace Encryption

    Public Class AES
        Private Provider As AesCryptoServiceProvider
        Private Encryptor As ICryptoTransform
        Private Decryptor As ICryptoTransform
        Private Block As Byte()

        Sub New()
            MyBase.New()
            Provider = New AesCryptoServiceProvider
            Provider.GenerateKey()
            Provider.GenerateIV()
            Encryptor = Provider.CreateEncryptor(Provider.Key, Provider.IV)
            Decryptor = Provider.CreateDecryptor(Provider.Key, Provider.IV)
        End Sub

        Sub New(ByVal Key As Byte(), ByVal IV As Byte())
            MyBase.New()
            Provider = New AesCryptoServiceProvider
            Provider.Key = Key
            Provider.Key = IV
            Encryptor = Provider.CreateEncryptor(Provider.Key, Provider.IV)
            Decryptor = Provider.CreateDecryptor(Provider.Key, Provider.IV)
        End Sub

        Function ValidKeySize(ByVal Size As Integer) As Boolean
            Return Provider.ValidKeySize(Size)
        End Function

        Property Key() As Byte()
            Get
                Return Provider.Key
            End Get
            Set(ByVal value As Byte())
                Provider.Key = value
            End Set
        End Property

        Property InitializationVector() As Byte()
            Get
                Return Provider.IV
            End Get
            Set(ByVal value As Byte())
                Provider.IV = value
            End Set
        End Property

        Property KeySize() As Int32
            Get
                Return Provider.KeySize
            End Get
            Set(ByVal value As Int32)
                Provider.KeySize = value
            End Set
        End Property

        ReadOnly Property LegalBlockSizes() As System.Security.Cryptography.KeySizes()
            Get
                Return Provider.LegalBlockSizes
            End Get
        End Property

        ReadOnly Property LegalKeySizes() As System.Security.Cryptography.KeySizes()
            Get
                Return Provider.LegalKeySizes
            End Get
        End Property

        Property CipherMode() As System.Security.Cryptography.CipherMode
            Get
                Return Provider.Mode
            End Get
            Set(ByVal value As System.Security.Cryptography.CipherMode)
                Provider.Mode = value
            End Set
        End Property

        Function TransformBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32, ByVal Mode As Mode) As Byte()
            Block = New Byte(((Length - Index) - 1)) {}
            If Mode = Mode.Decrypt Then
                Decryptor.TransformBlock(Buffer, Index, Length, Block, 0)
            Else
                Encryptor.TransformBlock(Buffer, Index, Length, Block, 0)
            End If
            Return Block
        End Function

        Function TransformBlock(ByVal Buffer As Byte(), ByVal Length As UInt32, ByVal Mode As Mode) As Byte()
            Return TransformBlock(Buffer, 0, Length, Mode)
        End Function

        Function TransformBlock(ByVal Buffer As Byte(), ByVal Mode As Mode) As Byte()
            Return TransformBlock(Buffer, 0, Buffer.Length, Mode)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32, ByVal Mode As Mode) As Byte()
            If Mode = Mode.Decrypt Then
                Return Decryptor.TransformFinalBlock(Buffer, Index, Length)
            Else
                Return Encryptor.TransformFinalBlock(Buffer, Index, Length)
            End If
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Length As UInt32, ByVal Mode As Mode) As Byte()
            Return TransformFinalBlock(Buffer, 0, Length, Mode)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Mode As Mode) As Byte()
            Return TransformFinalBlock(Buffer, 0, Buffer.Length, Mode)
        End Function

        Sub Dispose()
            Encryptor.Dispose()
            Decryptor.Dispose()
            GC.SuppressFinalize(Me)
        End Sub

    End Class

    Public Class DES
        Private Provider As DESCryptoServiceProvider
        Private Encryptor As ICryptoTransform
        Private Decryptor As ICryptoTransform
        Private Block As Byte()

        Sub New()
            MyBase.New()
            Provider = New DESCryptoServiceProvider
            Provider.GenerateKey()
            Provider.GenerateIV()
            Encryptor = Provider.CreateEncryptor(Provider.Key, Provider.IV)
            Decryptor = Provider.CreateDecryptor(Provider.Key, Provider.IV)
        End Sub

        Sub New(ByVal Key As Byte(), ByVal IV As Byte())
            MyBase.New()
            Provider = New DESCryptoServiceProvider
            Provider.Key = Key
            Provider.Key = IV
            Encryptor = Provider.CreateEncryptor(Provider.Key, Provider.IV)
            Decryptor = Provider.CreateDecryptor(Provider.Key, Provider.IV)
        End Sub

        Function ValidKeySize(ByVal Size As Integer) As Boolean
            Return Provider.ValidKeySize(Size)
        End Function

        Property Key() As Byte()
            Get
                Return Provider.Key
            End Get
            Set(ByVal value As Byte())
                Provider.Key = value
            End Set
        End Property

        Property InitializationVector() As Byte()
            Get
                Return Provider.IV
            End Get
            Set(ByVal value As Byte())
                Provider.IV = value
            End Set
        End Property

        Property KeySize() As Int32
            Get
                Return Provider.KeySize
            End Get
            Set(ByVal value As Int32)
                Provider.KeySize = value
            End Set
        End Property

        ReadOnly Property LegalBlockSizes() As System.Security.Cryptography.KeySizes()
            Get
                Return Provider.LegalBlockSizes
            End Get
        End Property

        ReadOnly Property LegalKeySizes() As System.Security.Cryptography.KeySizes()
            Get
                Return Provider.LegalKeySizes
            End Get
        End Property

        Property CipherMode() As System.Security.Cryptography.CipherMode
            Get
                Return Provider.Mode
            End Get
            Set(ByVal value As System.Security.Cryptography.CipherMode)
                Provider.Mode = value
            End Set
        End Property

        Function TransformBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32, ByVal Mode As Mode) As Byte()
            Block = New Byte(((Length - Index) - 1)) {}
            If Mode = Mode.Decrypt Then
                Decryptor.TransformBlock(Buffer, Index, Length, Block, 0)
            Else
                Encryptor.TransformBlock(Buffer, Index, Length, Block, 0)
            End If
            Return Block
        End Function

        Function TransformBlock(ByVal Buffer As Byte(), ByVal Length As UInt32, ByVal Mode As Mode) As Byte()
            Return TransformBlock(Buffer, 0, Length, Mode)
        End Function

        Function TransformBlock(ByVal Buffer As Byte(), ByVal Mode As Mode) As Byte()
            Return TransformBlock(Buffer, 0, Buffer.Length, Mode)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32, ByVal Mode As Mode) As Byte()
            If Mode = Mode.Decrypt Then
                Return Decryptor.TransformFinalBlock(Buffer, Index, Length)
            Else
                Return Encryptor.TransformFinalBlock(Buffer, Index, Length)
            End If
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Length As UInt32, ByVal Mode As Mode) As Byte()
            Return TransformFinalBlock(Buffer, 0, Length, Mode)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Mode As Mode) As Byte()
            Return TransformFinalBlock(Buffer, 0, Buffer.Length, Mode)
        End Function

        Sub Dispose()
            Encryptor.Dispose()
            Decryptor.Dispose()
            GC.SuppressFinalize(Me)
        End Sub

    End Class

    Public Class TripleDES
        Private Provider As TripleDESCryptoServiceProvider
        Private Encryptor As ICryptoTransform
        Private Decryptor As ICryptoTransform
        Private Block As Byte()

        Sub New()
            MyBase.New()
            Provider = New TripleDESCryptoServiceProvider
            Provider.GenerateKey()
            Provider.GenerateIV()
            Encryptor = Provider.CreateEncryptor(Provider.Key, Provider.IV)
            Decryptor = Provider.CreateDecryptor(Provider.Key, Provider.IV)
        End Sub

        Sub New(ByVal Key As Byte(), ByVal IV As Byte())
            MyBase.New()
            Provider = New TripleDESCryptoServiceProvider
            Provider.Key = Key
            Provider.Key = IV
            Encryptor = Provider.CreateEncryptor(Provider.Key, Provider.IV)
            Decryptor = Provider.CreateDecryptor(Provider.Key, Provider.IV)
        End Sub

        Function ValidKeySize(ByVal Size As Integer) As Boolean
            Return Provider.ValidKeySize(Size)
        End Function

        Property Key() As Byte()
            Get
                Return Provider.Key
            End Get
            Set(ByVal value As Byte())
                Provider.Key = value
            End Set
        End Property

        Property InitializationVector() As Byte()
            Get
                Return Provider.IV
            End Get
            Set(ByVal value As Byte())
                Provider.IV = value
            End Set
        End Property

        Property KeySize() As Int32
            Get
                Return Provider.KeySize
            End Get
            Set(ByVal value As Int32)
                Provider.KeySize = value
            End Set
        End Property

        ReadOnly Property LegalBlockSizes() As System.Security.Cryptography.KeySizes()
            Get
                Return Provider.LegalBlockSizes
            End Get
        End Property

        ReadOnly Property LegalKeySizes() As System.Security.Cryptography.KeySizes()
            Get
                Return Provider.LegalKeySizes
            End Get
        End Property

        Property CipherMode() As System.Security.Cryptography.CipherMode
            Get
                Return Provider.Mode
            End Get
            Set(ByVal value As System.Security.Cryptography.CipherMode)
                Provider.Mode = value
            End Set
        End Property

        Function TransformBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32, ByVal Mode As Mode) As Byte()
            Block = New Byte(((Length - Index) - 1)) {}
            If Mode = Mode.Decrypt Then
                Decryptor.TransformBlock(Buffer, Index, Length, Block, 0)
            Else
                Encryptor.TransformBlock(Buffer, Index, Length, Block, 0)
            End If
            Return Block
        End Function

        Function TransformBlock(ByVal Buffer As Byte(), ByVal Length As UInt32, ByVal Mode As Mode) As Byte()
            Return TransformBlock(Buffer, 0, Length, Mode)
        End Function

        Function TransformBlock(ByVal Buffer As Byte(), ByVal Mode As Mode) As Byte()
            Return TransformBlock(Buffer, 0, Buffer.Length, Mode)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32, ByVal Mode As Mode) As Byte()
            If Mode = Mode.Decrypt Then
                Return Decryptor.TransformFinalBlock(Buffer, Index, Length)
            Else
                Return Encryptor.TransformFinalBlock(Buffer, Index, Length)
            End If
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Length As UInt32, ByVal Mode As Mode) As Byte()
            Return TransformFinalBlock(Buffer, 0, Length, Mode)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Mode As Mode) As Byte()
            Return TransformFinalBlock(Buffer, 0, Buffer.Length, Mode)
        End Function

        Sub Dispose()
            Encryptor.Dispose()
            Decryptor.Dispose()
            GC.SuppressFinalize(Me)
        End Sub

    End Class

    Public Class RijndaelManaged
        Private Provider As System.Security.Cryptography.RijndaelManaged
        Private Encryptor As ICryptoTransform
        Private Decryptor As ICryptoTransform
        Private Block As Byte()

        Sub New()
            MyBase.New()
            Provider = New System.Security.Cryptography.RijndaelManaged
            Provider.GenerateKey()
            Provider.GenerateIV()
            Encryptor = Provider.CreateEncryptor(Provider.Key, Provider.IV)
            Decryptor = Provider.CreateDecryptor(Provider.Key, Provider.IV)
        End Sub

        Sub New(ByVal Key As Byte(), ByVal IV As Byte())
            MyBase.New()
            Provider = New System.Security.Cryptography.RijndaelManaged
            Provider.Key = Key
            Provider.Key = IV
            Encryptor = Provider.CreateEncryptor(Provider.Key, Provider.IV)
            Decryptor = Provider.CreateDecryptor(Provider.Key, Provider.IV)
        End Sub

        Function ValidKeySize(ByVal Size As Integer) As Boolean
            Return Provider.ValidKeySize(Size)
        End Function

        Property Key() As Byte()
            Get
                Return Provider.Key
            End Get
            Set(ByVal value As Byte())
                Provider.Key = value
            End Set
        End Property

        Property InitializationVector() As Byte()
            Get
                Return Provider.IV
            End Get
            Set(ByVal value As Byte())
                Provider.IV = value
            End Set
        End Property

        Property KeySize() As Int32
            Get
                Return Provider.KeySize
            End Get
            Set(ByVal value As Int32)
                Provider.KeySize = value
            End Set
        End Property

        ReadOnly Property LegalBlockSizes() As System.Security.Cryptography.KeySizes()
            Get
                Return Provider.LegalBlockSizes
            End Get
        End Property

        ReadOnly Property LegalKeySizes() As System.Security.Cryptography.KeySizes()
            Get
                Return Provider.LegalKeySizes
            End Get
        End Property

        Property CipherMode() As System.Security.Cryptography.CipherMode
            Get
                Return Provider.Mode
            End Get
            Set(ByVal value As System.Security.Cryptography.CipherMode)
                Provider.Mode = value
            End Set
        End Property

        Function TransformBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32, ByVal Mode As Mode) As Byte()
            Block = New Byte(((Length - Index) - 1)) {}
            If Mode = Mode.Decrypt Then
                Decryptor.TransformBlock(Buffer, Index, Length, Block, 0)
            Else
                Encryptor.TransformBlock(Buffer, Index, Length, Block, 0)
            End If
            Return Block
        End Function

        Function TransformBlock(ByVal Buffer As Byte(), ByVal Length As UInt32, ByVal Mode As Mode) As Byte()
            Return TransformBlock(Buffer, 0, Length, Mode)
        End Function

        Function TransformBlock(ByVal Buffer As Byte(), ByVal Mode As Mode) As Byte()
            Return TransformBlock(Buffer, 0, Buffer.Length, Mode)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32, ByVal Mode As Mode) As Byte()
            If Mode = Mode.Decrypt Then
                Return Decryptor.TransformFinalBlock(Buffer, Index, Length)
            Else
                Return Encryptor.TransformFinalBlock(Buffer, Index, Length)
            End If
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Length As UInt32, ByVal Mode As Mode) As Byte()
            Return TransformFinalBlock(Buffer, 0, Length, Mode)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Mode As Mode) As Byte()
            Return TransformFinalBlock(Buffer, 0, Buffer.Length, Mode)
        End Function

        Sub Dispose()
            Encryptor.Dispose()
            Decryptor.Dispose()
            GC.SuppressFinalize(Me)
        End Sub

    End Class

    Public Class RC2
        Private Provider As RC2CryptoServiceProvider
        Private Encryptor As ICryptoTransform
        Private Decryptor As ICryptoTransform
        Private Block As Byte()

        Sub New()
            MyBase.New()
            Provider = New RC2CryptoServiceProvider
            Provider.GenerateKey()
            Provider.GenerateIV()
            Encryptor = Provider.CreateEncryptor(Provider.Key, Provider.IV)
            Decryptor = Provider.CreateDecryptor(Provider.Key, Provider.IV)
        End Sub

        Sub New(ByVal Key As Byte(), ByVal IV As Byte())
            MyBase.New()
            Provider = New RC2CryptoServiceProvider
            Provider.Key = Key
            Provider.Key = IV
            Encryptor = Provider.CreateEncryptor(Provider.Key, Provider.IV)
            Decryptor = Provider.CreateDecryptor(Provider.Key, Provider.IV)
        End Sub

        Function ValidKeySize(ByVal Size As Integer) As Boolean
            Return Provider.ValidKeySize(Size)
        End Function

        Property Key() As Byte()
            Get
                Return Provider.Key
            End Get
            Set(ByVal value As Byte())
                Provider.Key = value
            End Set
        End Property

        Property InitializationVector() As Byte()
            Get
                Return Provider.IV
            End Get
            Set(ByVal value As Byte())
                Provider.IV = value
            End Set
        End Property

        Property KeySize() As Int32
            Get
                Return Provider.KeySize
            End Get
            Set(ByVal value As Int32)
                Provider.KeySize = value
            End Set
        End Property

        ReadOnly Property LegalBlockSizes() As System.Security.Cryptography.KeySizes()
            Get
                Return Provider.LegalBlockSizes
            End Get
        End Property

        ReadOnly Property LegalKeySizes() As System.Security.Cryptography.KeySizes()
            Get
                Return Provider.LegalKeySizes
            End Get
        End Property

        Property CipherMode() As System.Security.Cryptography.CipherMode
            Get
                Return Provider.Mode
            End Get
            Set(ByVal value As System.Security.Cryptography.CipherMode)
                Provider.Mode = value
            End Set
        End Property

        Function TransformBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32, ByVal Mode As Mode) As Byte()
            Block = New Byte(((Length - Index) - 1)) {}
            If Mode = Mode.Decrypt Then
                Decryptor.TransformBlock(Buffer, Index, Length, Block, 0)
            Else
                Encryptor.TransformBlock(Buffer, Index, Length, Block, 0)
            End If
            Return Block
        End Function

        Function TransformBlock(ByVal Buffer As Byte(), ByVal Length As UInt32, ByVal Mode As Mode) As Byte()
            Return TransformBlock(Buffer, 0, Length, Mode)
        End Function

        Function TransformBlock(ByVal Buffer As Byte(), ByVal Mode As Mode) As Byte()
            Return TransformBlock(Buffer, 0, Buffer.Length, Mode)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32, ByVal Mode As Mode) As Byte()
            If Mode = Mode.Decrypt Then
                Return Decryptor.TransformFinalBlock(Buffer, Index, Length)
            Else
                Return Encryptor.TransformFinalBlock(Buffer, Index, Length)
            End If
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Length As UInt32, ByVal Mode As Mode) As Byte()
            Return TransformFinalBlock(Buffer, 0, Length, Mode)
        End Function

        Function TransformFinalBlock(ByVal Buffer As Byte(), ByVal Mode As Mode) As Byte()
            Return TransformFinalBlock(Buffer, 0, Buffer.Length, Mode)
        End Function

        Sub Dispose()
            Encryptor.Dispose()
            Decryptor.Dispose()
            GC.SuppressFinalize(Me)
        End Sub

    End Class

    Public Class RC4
        Private CryptoKey As Byte() = Nothing

        Sub New(ByVal Key As Byte())
            MyBase.New()
            Me.Key = Key
        End Sub

        Property Key() As Byte()
            Get
                Return Me.CryptoKey
            End Get
            Set(ByVal value As Byte())
                If value Is Nothing OrElse value.Length = 0 Then Throw New CryptographicException("Key must not be null")
                If value.Length < 1 OrElse value.Length > 256 Then Throw New CryptographicException("A valid key must be between 1 to 256 bytes in length")
                CryptoKey = value
            End Set
        End Property

        Private Function Core(ByVal Buffer As Byte(), ByVal Index As Int32, ByVal Length As Int32) As Byte()
            If Key Is Nothing OrElse Key.Length = 0 Then Throw New CryptographicException("Key must not be null")
            If Buffer Is Nothing OrElse Buffer.Length = 0 Then Throw New CryptographicException("Input data must not be null")
            Dim a(256 - 1), b(256 - 1) As Byte, c As Byte, d As Byte
            For i As Int32 = 0 To 256 - 1
                a(i) = i
                b(i) = CryptoKey(i Mod CryptoKey.Length)
            Next
            Dim x As Int32 = 0
            For i As Int32 = 0 To 256 - 1
                x = ((x + a(i) + b(i)) Mod 256)
                c = a(i)
                a(i) = a(x)
                a(x) = c
            Next
            Dim y As Int32 = x
            For i As Int32 = 0 To Buffer.Length - 1
                y = ((y + 1) Mod 256)
                x = ((x + a(y)) Mod 256)
                c = a(y)
                a(y) = a(x)
                a(x) = c
                d = ((a(y) + a(x)) Mod 256)
                Buffer(i) = (Buffer(i) Xor a(d))
            Next
            Return Buffer
        End Function

        Function Encrypt(ByVal Buffer As Byte(), ByVal Index As Int32, ByVal Length As Int32) As Byte()
            Return Core(Buffer, Index, Length)
        End Function

        Function Encrypt(ByVal Buffer As Byte(), ByVal Length As Int32) As Byte()
            Return Encrypt(Buffer, 0, Length)
        End Function

        Function Encrypt(ByVal Buffer As Byte()) As Byte()
            Return Encrypt(Buffer, Buffer.Length)
        End Function

        Function Decrypt(ByVal Buffer As Byte(), ByVal Index As Int32, ByVal Length As Int32) As Byte()
            Return Core(Buffer, Index, Length)
        End Function

        Function Decrypt(ByVal Buffer As Byte(), ByVal Length As Int32) As Byte()
            Return Decrypt(Buffer, 0, Length)
        End Function

        Function Decrypt(ByVal Buffer As Byte()) As Byte()
            Return Decrypt(Buffer, Buffer.Length)
        End Function

        Sub Dispose()
            GC.SuppressFinalize(Me)
        End Sub

    End Class

    Public Class RSA
        Private RSA As RSACryptoServiceProvider

        Sub New()
            MyBase.New()
            RSA = New RSACryptoServiceProvider
            RSA.ImportParameters(New RSAParameters)
        End Sub

        Sub New(ByVal Parameters As RSAParameters)
            MyBase.New()
            RSA = New RSACryptoServiceProvider
            RSA.ImportParameters(Parameters)
        End Sub

        Sub New(ByVal CspBlob As Byte())
            MyBase.New()
            RSA = New RSACryptoServiceProvider
            RSA.ImportCspBlob(CspBlob)
        End Sub

        Property Parameters() As RSAParameters
            Get
                Return RSA.ExportParameters(True)
            End Get
            Set(ByVal value As RSAParameters)
                RSA.ImportParameters(value)
            End Set
        End Property

        Property CspBlob() As Byte()
            Get
                Return RSA.ExportCspBlob(True)
            End Get
            Set(ByVal value As Byte())
                RSA.ImportCspBlob(value)
            End Set
        End Property

        ReadOnly Property LegalKeySizes() As System.Security.Cryptography.KeySizes()
            Get
                Return RSA.LegalKeySizes
            End Get
        End Property

        ReadOnly Property PublicKeyOnly() As Boolean
            Get
                Return RSA.PublicOnly
            End Get
        End Property

        ReadOnly Property KeyExchangeAlgorithm() As String
            Get
                Return RSA.KeyExchangeAlgorithm
            End Get
        End Property

        ReadOnly Property KeySize() As Int32
            Get
                Return RSA.KeySize
            End Get
        End Property

        ReadOnly Property SignatureAlgorithm() As String
            Get
                Return RSA.SignatureAlgorithm
            End Get
        End Property

        ReadOnly Property CspKeyContainerInfo() As System.Security.Cryptography.CspKeyContainerInfo
            Get
                Return RSA.CspKeyContainerInfo
            End Get
        End Property

        Function Encrypt(ByVal Buffer As Byte(), ByVal OAEP_Padding As Boolean) As Byte()
            Return RSA.Encrypt(Buffer, OAEP_Padding)
        End Function

        Function Encrypt(ByVal Buffer As Byte()) As Byte()
            Return Encrypt(Buffer, True)
        End Function

        Function Decrypt(ByVal Buffer As Byte(), ByVal OAEP_Padding As Boolean) As Byte()
            Return RSA.Decrypt(Buffer, OAEP_Padding)
        End Function

        Function Decrypt(ByVal Buffer As Byte()) As Byte()
            Return Decrypt(Buffer, True)
        End Function
    End Class

    Public Enum Mode
        Encrypt
        Decrypt
    End Enum

End Namespace

Namespace PublicKeyCryptography

    Public Class PKCS1
        Private Provider As RSACryptoServiceProvider
        Private Formatter As RSAPKCS1SignatureFormatter
        Private Deformatter As RSAPKCS1SignatureDeformatter

        Sub New(Optional ByVal Algorithm As String = "sha-1")
            MyBase.New()
            Provider = New RSACryptoServiceProvider()
            Provider.ImportParameters(New RSAParameters)
            Formatter = New RSAPKCS1SignatureFormatter(Provider)
            Formatter.SetHashAlgorithm(Algorithm)
            Deformatter = New RSAPKCS1SignatureDeformatter(Provider)
            Deformatter.SetHashAlgorithm(Algorithm)
        End Sub

        Sub New(ByVal Parameters As RSAParameters, Optional ByVal Algorithm As String = "sha-1")
            MyBase.New()
            Provider = New RSACryptoServiceProvider()
            Provider.ImportParameters(Parameters)
            Formatter = New RSAPKCS1SignatureFormatter(Provider)
            Formatter.SetHashAlgorithm(Algorithm)
            Deformatter = New RSAPKCS1SignatureDeformatter(Provider)
            Deformatter.SetHashAlgorithm(Algorithm)
        End Sub

        Sub New(ByVal CspBlob As Byte(), Optional ByVal Algorithm As String = "sha-1")
            MyBase.New()
            Provider = New RSACryptoServiceProvider()
            Provider.ImportCspBlob(CspBlob)
            Formatter = New RSAPKCS1SignatureFormatter(Provider)
            Formatter.SetHashAlgorithm(Algorithm)
            Deformatter = New RSAPKCS1SignatureDeformatter(Provider)
            Deformatter.SetHashAlgorithm(Algorithm)
        End Sub

        Property Parameters() As RSAParameters
            Get
                Return Provider.ExportParameters(True)
            End Get
            Set(ByVal value As RSAParameters)
                Provider.ImportParameters(value)
            End Set
        End Property

        Property CspBlob() As Byte()
            Get
                Return Provider.ExportCspBlob(True)
            End Get
            Set(ByVal value As Byte())
                Provider.ImportCspBlob(value)
            End Set
        End Property

        ReadOnly Property KeyExchangeAlgorithm() As String
            Get
                Return Provider.KeyExchangeAlgorithm
            End Get
        End Property

        ReadOnly Property PublicKeyOnly() As Boolean
            Get
                Return Provider.PublicOnly
            End Get
        End Property

        ReadOnly Property SignatureAlgorithm() As String
            Get
                Return Provider.SignatureAlgorithm
            End Get
        End Property

        ReadOnly Property CspKeyContainerInfo() As System.Security.Cryptography.CspKeyContainerInfo
            Get
                Return Provider.CspKeyContainerInfo
            End Get
        End Property

        ReadOnly Property LegalKeySizes() As System.Security.Cryptography.KeySizes()
            Get
                Return Provider.LegalKeySizes
            End Get
        End Property

        Function SignData(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32, Optional ByVal Algorithm As String = "sha-1") As Byte()
            Return Provider.SignData(Buffer, Index, Length, Algorithm)
        End Function

        Function SignData(ByVal Buffer As Byte(), ByVal Length As UInt32, Optional ByVal Algorithm As String = "sha-1") As Byte()
            Return SignData(Buffer, 0, Length, Algorithm)
        End Function

        Function SignData(ByVal Buffer As Byte(), Optional ByVal Algorithm As String = "sha-1") As Byte()
            Return SignData(Buffer, 0, Buffer.Length, Algorithm)
        End Function

        Function VerifyData(ByVal Buffer As Byte(), ByVal Signature As Byte(), Optional ByVal Algorithm As String = "sha-1") As Boolean
            Return Provider.VerifyData(Buffer, Algorithm, Signature)
        End Function

        Function SignHash(ByVal Hash As Byte(), ByVal Index As UInt32, ByVal Length As UInt32, Optional ByVal Algorithm As String = "sha-1") As Byte()
            Return Provider.SignHash(Hash, Algorithm)
        End Function

        Function VerifyHash(ByVal Hash As Byte(), ByVal Signature As Byte(), Optional ByVal Algorithm As String = "sha-1") As Boolean
            Return Provider.VerifyHash(Hash, Algorithm, Signature)
        End Function

        Function ValidateSignature(ByVal Hash As Byte(), ByVal Signature As Byte(), Optional ByVal Algorithm As String = "sha-1") As Boolean
            Deformatter.SetHashAlgorithm(Algorithm)
            Return Deformatter.VerifySignature(Hash, Signature)
        End Function

        Function GenerateSignature(ByVal Hash As Byte(), Optional ByVal Algorithm As String = "sha-1") As Byte()
            Formatter.SetHashAlgorithm(Algorithm)
            Return Formatter.CreateSignature(Hash)
        End Function

        Sub Dispose()
            GC.SuppressFinalize(Me)
        End Sub

    End Class

    Public Class DSA
        Private Provider As DSACryptoServiceProvider
        Private Formatter As DSASignatureFormatter
        Private Deformatter As DSASignatureDeformatter

        Sub New(Optional ByVal Algorithm As String = "sha-1")
            MyBase.New()
            Provider = New DSACryptoServiceProvider()
            Provider.ImportParameters(New DSAParameters)
            Formatter = New DSASignatureFormatter(Provider)
            Formatter.SetHashAlgorithm(Algorithm)
            Deformatter = New DSASignatureDeformatter(Provider)
            Deformatter.SetHashAlgorithm(Algorithm)
        End Sub

        Sub New(ByVal Parameters As DSAParameters, Optional ByVal Algorithm As String = "sha-1")
            MyBase.New()
            Provider = New DSACryptoServiceProvider()
            Provider.ImportParameters(Parameters)
            Formatter = New DSASignatureFormatter(Provider)
            Formatter.SetHashAlgorithm(Algorithm)
            Deformatter = New DSASignatureDeformatter(Provider)
            Deformatter.SetHashAlgorithm(Algorithm)
        End Sub

        Sub New(ByVal CspBlob As Byte(), Optional ByVal Algorithm As String = "sha-1")
            MyBase.New()
            Provider = New DSACryptoServiceProvider()
            Provider.ImportCspBlob(CspBlob)
            Formatter = New DSASignatureFormatter(Provider)
            Formatter.SetHashAlgorithm(Algorithm)
            Deformatter = New DSASignatureDeformatter(Provider)
            Deformatter.SetHashAlgorithm(Algorithm)
        End Sub

        Property Parameters() As DSAParameters
            Get
                Return Provider.ExportParameters(True)
            End Get
            Set(ByVal value As DSAParameters)
                Provider.ImportParameters(value)
            End Set
        End Property

        Property CspBlob() As Byte()
            Get
                Return Provider.ExportCspBlob(True)
            End Get
            Set(ByVal value As Byte())
                Provider.ImportCspBlob(value)
            End Set
        End Property

        ReadOnly Property KeyExchangeAlgorithm() As String
            Get
                Return Provider.KeyExchangeAlgorithm
            End Get
        End Property

        ReadOnly Property PublicKeyOnly() As Boolean
            Get
                Return Provider.PublicOnly
            End Get
        End Property

        ReadOnly Property SignatureAlgorithm() As String
            Get
                Return Provider.SignatureAlgorithm
            End Get
        End Property

        ReadOnly Property CspKeyContainerInfo() As System.Security.Cryptography.CspKeyContainerInfo
            Get
                Return Provider.CspKeyContainerInfo
            End Get
        End Property

        ReadOnly Property LegalKeySizes() As System.Security.Cryptography.KeySizes()
            Get
                Return Provider.LegalKeySizes
            End Get
        End Property

        Function SignData(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32) As Byte()
            Return Provider.SignData(Buffer, Index, Length)
        End Function

        Function SignData(ByVal Buffer As Byte(), ByVal Length As UInt32) As Byte()
            Return SignData(Buffer, 0, Length)
        End Function

        Function SignData(ByVal Buffer As Byte()) As Byte()
            Return SignData(Buffer, 0, Buffer.Length)
        End Function

        Function VerifyData(ByVal Buffer As Byte(), ByVal Signature As Byte()) As Boolean
            Return Provider.VerifyData(Buffer, Signature)
        End Function

        Function SignHash(ByVal Hash As Byte(), ByVal Index As UInt32, ByVal Length As UInt32, Optional ByVal Algorithm As String = "sha-1") As Byte()
            Return Provider.SignHash(Hash, Algorithm)
        End Function

        Function VerifyHash(ByVal Hash As Byte(), ByVal Signature As Byte(), Optional ByVal Algorithm As String = "sha-1") As Boolean
            Return Provider.VerifyHash(Hash, Algorithm, Signature)
        End Function

        Function ValidateSignature(ByVal Hash As Byte(), ByVal Signature As Byte(), Optional ByVal Algorithm As String = "sha-1") As Boolean
            Deformatter.SetHashAlgorithm(Algorithm)
            Return Deformatter.VerifySignature(Hash, Signature)
        End Function

        Function GenerateSignature(ByVal Hash As Byte(), Optional ByVal Algorithm As String = "sha-1") As Byte()
            Formatter.SetHashAlgorithm(Algorithm)
            Return Formatter.CreateSignature(Hash)
        End Function

        Sub Dispose()
            GC.SuppressFinalize(Me)
        End Sub

    End Class

End Namespace

Namespace Compression

    Public Class Deflate
        Private Stream As System.IO.Compression.DeflateStream

        Sub New()
            MyBase.New()
        End Sub

        Function TransformBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32, ByVal Mode As Mode) As Byte()
            If Buffer Is Nothing Then Return Nothing
            Dim [In] As Stream = New MemoryStream(Buffer)
            Dim [Out] As Stream = New MemoryStream
            If Mode = Mode.Compress Then
                Stream = New System.IO.Compression.DeflateStream([Out], 1, True)
            Else
                Stream = New System.IO.Compression.DeflateStream([Out], 0, True)
            End If
            For u As UInt32 = Index To Length - 1
                [Out].WriteByte([In].ReadByte)
                [Out].Flush()
            Next
            [In].Close()
            [In].Dispose()
            Buffer = New Byte([Out].Length - 1) {}
            [Out].Read(Buffer, 0, Buffer.Length)
            [Out].Close()
            Stream.Close()
            Stream.Dispose()
            Return Buffer
        End Function

        Function TransformBlock(ByVal Buffer As Byte(), ByVal Length As UInt32, ByVal Mode As Mode) As Byte()
            Return TransformBlock(Buffer, 0, Length, Mode)
        End Function

        Function TransformBlock(ByVal Buffer As Byte(), ByVal Mode As Mode) As Byte()
            Return TransformBlock(Buffer, 0, Buffer.Length, Mode)
        End Function

        Sub Dispose()
            GC.SuppressFinalize(Me)
        End Sub

    End Class

    Public Class GZip
        Private Stream As System.IO.Compression.GZipStream

        Sub New()
            MyBase.New()
        End Sub

        Function TransformBlock(ByVal Buffer As Byte(), ByVal Index As UInt32, ByVal Length As UInt32, ByVal Mode As Mode) As Byte()
            If Buffer Is Nothing Then Return Nothing
            Dim [In] As Stream = New MemoryStream(Buffer)
            Dim [Out] As Stream = New MemoryStream
            If Mode = Mode.Compress Then
                Stream = New System.IO.Compression.GZipStream([Out], 1, True)
            Else
                Stream = New System.IO.Compression.GZipStream([Out], 0, True)
            End If
            For u As UInt32 = Index To Length - 1
                [Out].WriteByte([In].ReadByte)
                [Out].Flush()
            Next
            [In].Close()
            [In].Dispose()
            Buffer = New Byte([Out].Length - 1) {}
            [Out].Read(Buffer, 0, Buffer.Length)
            [Out].Close()
            Stream.Close()
            Stream.Dispose()
            Return Buffer
        End Function

        Function TransformBlock(ByVal Buffer As Byte(), ByVal Length As UInt32, ByVal Mode As Mode) As Byte()
            Return TransformBlock(Buffer, 0, Length, Mode)
        End Function

        Function TransformBlock(ByVal Buffer As Byte(), ByVal Mode As Mode) As Byte()
            Return TransformBlock(Buffer, 0, Buffer.Length, Mode)
        End Function

        Sub Dispose()
            GC.SuppressFinalize(Me)
        End Sub

    End Class

    Public Enum Mode
        Compress
        Decompress
    End Enum

End Namespace

''' <summary>
''' Holds, displays, and fetches information about PackageIO
''' </summary>
Public Class About
    Shared ReadOnly Product As String = "PackageIO"
    Shared ReadOnly Author As String = "feudalnate"
    Shared ReadOnly Title As String = "Endian IO Class"
    Shared ReadOnly Description As String = "An open source multi-purpose Endian IO class"
    Shared ReadOnly Version As String = "0.1.9.4"
    Shared ReadOnly Website As String = "http://www.360haven.com"
    Shared ReadOnly Thanks As String = "Special thanks to Mojobojo and DJ Shepherd"
    Shared ReadOnly PrintInfo As String = New String(Product & Chr(32) & Chr(45) & Chr(32) & Version & vbLf & Description & vbLf & vbLf & "Author: " & Author & vbLf & vbLf & Thanks & vbLf & vbLf & vbLf & Website.ToString)

    ''' <summary>
    ''' Contacts server to check if an update for PackageIO is availible
    ''' </summary>
    ''' <returns>Returns true or false if a newer version of PackageIO is availible</returns>
    Shared Function CheckUpdate() As Boolean
        Dim Network As New Devices.Network()
        Try
            If Not Network.IsAvailable Then Throw New Exception("No network connection availible")
            Return (Version <> New StreamReader(WebRequest.Create("http://360haven.com/feudalnate/pio.php?fetch=version").GetResponse().GetResponseStream).ReadToEnd().Replace(vbNullChar, ""))
        Catch x As Exception
            Throw New Exception("There was an error while checking for an update:" & vbLf & vbLf & x.Message)
        Finally
            Network = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Contacts server to check if an update for PackageIO is availible and downloads if true
    ''' </summary>
    ''' <param name="DestinationDirectory">Path to an existing directory to store downloaded data. Exception is thrown if directory does not exist</param>
    ''' <param name="DestinationFileName">Name of the file to create in specified directory and write downloaded data</param>
    ''' <returns>Returns true or false if a newer version of PackageIO is availible, if update is availible then return value also represents if the update was downloaded successfully</returns>
    Shared Function CheckUpdate(ByVal DestinationDirectory As String, Optional ByVal DestinationFileName As String = "PackageIO.dll") As Boolean
        Dim Network As New Devices.Network()
        Try
            If CheckUpdate Then
                If Not Directory.Exists(DestinationDirectory) Then Throw New DirectoryNotFoundException("Destination directory was not found or could not be accessed")
                Dim x As String = Path.Combine(DestinationDirectory.Replace(vbNullChar, ""), DestinationFileName.Replace(vbNullChar, ""))
                If File.Exists(x) Then
                    File.Delete(x)
                    Threading.Thread.Sleep(50)
                End If
                File.Create(x).Close()
                Network.DownloadFile(New StreamReader(WebRequest.Create("http://360haven.com/feudalnate/pio.php?fetch=link").GetResponse().GetResponseStream).ReadToEnd().Replace(vbNullChar, ""), x)
                Return True
            End If
            Return False
        Catch x As Exception
            Throw New Exception("There was an error while checking for an update:" & vbLf & vbLf & x.Message)
        Finally
            Network = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Displays a message box containing information about PackageIO
    ''' </summary>
    Shared Sub ShowMsgBox()
        MsgBox(PrintInfo, MsgBoxStyle.Information, "About PackageIO")
    End Sub

    ''' <summary>
    ''' Opens the PackageIO homepage in the default browser
    ''' </summary>
    Shared Sub LaunchWebsite()
        Process.Start(Website)
    End Sub

End Class

''' <summary>
''' Endianness enumerator. Byte ordering in which to read or write data
''' </summary>
Public Enum Endian
    ''' <summary>
    ''' Big Endian byte order. Most significant byte first
    ''' </summary>
    Big
    ''' <summary>
    ''' Little Endian byte order. Least significant byte first
    ''' </summary>
    Little
End Enum

''' <summary>
''' Integer bit width enumerator. Specifies integer types supported by various classes
''' </summary>
Public Enum IntegerType
    [Int8]
    [UInt8]
    [Int16]
    [UInt16]
    [Int24]
    [UInt24]
    [Int32]
    [UInt32]
    [Int40]
    [UInt40]
    [Int48]
    [UInt48]
    [Int56]
    [UInt56]
    [Int64]
    [UInt64]
    [Single]
    [Double]
End Enum

''' <summary>
''' String type enumerator. Specifies string types supported by various classes
''' </summary>
Public Enum StringType
    ASCII
    Unicode
    BigEndianUnicode
    Hexadecimal
End Enum

''' <summary>
''' Represents the type of stream used in initial declaration of the reader, writer and IO classes
''' </summary>
Public Enum StreamType
    GenericStream
    FileStream
    MemoryStream
End Enum
